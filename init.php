<?php
/* @var $GLOBALS['db'] ADODB_mysqli */
/**
 * =============================================================================
 * This file will setup our defs, and configure php
 * 
 * @author SteamFriends Development Team
 * @version 1.0.0
 * @copyright SourceBans (C)2007 SteamFriends.com.  All rights reserved.
 * @package SourceBans
 * @link http://www.sourcebans.net
 * 
 * @version $Id: init.php 271 2009-06-24 18:48:42Z peace-maker $
 * =============================================================================
 */
// ---------------------------------------------------
//  Directories
// ---------------------------------------------------
define('ROOT', dirname(__FILE__) . DIRECTORY_SEPARATOR);
define('TEMPLATES_PATH', ROOT . 'pages' . DIRECTORY_SEPARATOR);
define('INCLUDES_PATH', ROOT . 'includes' . DIRECTORY_SEPARATOR);
define('SB_MAP_LOCATION', ROOT . 'images'.DIRECTORY_SEPARATOR.'maps' . DIRECTORY_SEPARATOR);
define('SB_ICONS', ROOT . 'images' . DIRECTORY_SEPARATOR . 'games' . DIRECTORY_SEPARATOR);
define('SB_DEMOS', ROOT . 'demos' . DIRECTORY_SEPARATOR);
define('SB_MODULES', ROOT . 'modules' . DIRECTORY_SEPARATOR);

define('SB_THEMES', ROOT . 'themes' . DIRECTORY_SEPARATOR);
define('SB_THEMES_COMPILE', ROOT . 'cache' . DIRECTORY_SEPARATOR . 'htmlCache');
define('SB_ADODB_CACHE', ROOT . 'cache' . DIRECTORY_SEPARATOR . 'dbCache');

define('SB_LANGUAGES_PATH', ROOT . 'languages' . DIRECTORY_SEPARATOR);

if (!@is_writable(SB_THEMES_COMPILE)) {
    die("Theme Error: <b>" . SB_THEMES_COMPILE . "</b> MUST be writable.");
}

if (!@is_writable(SB_ADODB_CACHE)) {
    die("Cache Error: <b>" . SB_ADODB_CACHE . "</b> MUST be writable.");
}

define('IN_SB', true);

spl_autoload_register(function ($classname) {
    if(class_exists($classname)) {
        return $classname;
    }
    $file = INCLUDES_PATH . $classname . '.php';
    if(!is_file($file)) {
        exit("File \"{$file}\" not found");
    }
    include $file;
});

// ---------------------------------------------------
//  Fix some $_SERVER vars
// ---------------------------------------------------
// Fix for IIS, which doesn't set REQUEST_URI
if (!isset($_SERVER['REQUEST_URI']) || trim($_SERVER['REQUEST_URI']) == '') {
    $_SERVER['REQUEST_URI'] = $_SERVER['SCRIPT_NAME'];
    if (isset($_SERVER['QUERY_STRING']) && !empty($_SERVER['QUERY_STRING'])) {
        $_SERVER['REQUEST_URI'] .= '?' . $_SERVER['QUERY_STRING'];
    }
}
// Fix for Dreamhost and other PHP as CGI hosts
if (strstr($_SERVER['SCRIPT_NAME'], 'php.cgi')) {
    unset($_SERVER['PATH_INFO']);
}
if (trim($_SERVER['PHP_SELF']) == '') {
    $_SERVER['PHP_SELF'] = preg_replace("/(\?.*)?$/", '', $_SERVER["REQUEST_URI"]);
}

// ---------------------------------------------------
//  Are we installed?
// ---------------------------------------------------
if (!file_exists(ROOT . 'config.php')) {
    // No were not
    if ($_SERVER['HTTP_HOST'] != "localhost") {
        echo "config.cfg not found. Are SourceBans installed?";
        die();
    }
} elseif(filesize(ROOT . 'config.php') < 3) {
    // SB is not installed
    if(is_dir(ROOT . 'install')) {
        header("Location: install/index.php");
    } else {
        echo 'Error!!! SourceBans is not installed and install directory not found';
    }
    exit;
}

include ROOT . 'config.php';

// ---------------------------------------------------
//  Initial setup
// ---------------------------------------------------
#define('SB_SVN', true);
if (!defined('SB_VERSION')) {
    define('SB_VERSION', '1.4.11');
    define('SB_REV', '$Rev: 462 $');
}
define('LOGIN_COOKIE_LIFETIME', (60 * 60 * 24 * 7) * 2);
define('COOKIE_PATH', '/');
define('COOKIE_DOMAIN', '');
define('COOKIE_SECURE', false);
define('SB_SALT', 'SourceBans');

// ---------------------------------------------------
//  Setup our DB
// ---------------------------------------------------
include_once(INCLUDES_PATH . "adodb".DIRECTORY_SEPARATOR."adodb.inc.php");
include_once(INCLUDES_PATH . "adodb".DIRECTORY_SEPARATOR."adodb-errorhandler.inc.php");
include_once(INCLUDES_PATH . "adodb".DIRECTORY_SEPARATOR."adodb-errorhandler.inc.php");
$ADODB_CACHE_DIR = SB_ADODB_CACHE;
$GLOBALS['db'] = ADONewConnection("mysqli://" . DB_USER . ':' . DB_PASS . '@' . DB_HOST . ':' . DB_PORT . '/' . DB_NAME);

if (!is_object($GLOBALS['db'])) {
    die('Error create ADODB object. Maybe wrong DB user data?');
}

$debug = $GLOBALS['db']->CacheExecute(86400, "SELECT `value` FROM `" . DB_PREFIX . "_settings` WHERE `setting` = 'config.debug';");
if ($debug->fields['value'] == "1") {
    defined("DEVELOPER_MODE") or define("DEVELOPER_MODE", true);
}

if (!defined("DEVELOPER_MODE") && !defined("IS_UPDATE") && file_exists(ROOT . "install")) {
    if ($_SERVER['HTTP_HOST'] != "localhost") {
        echo "Please delete the install directory before you use SourceBans";
        die();
    }
}

if (!defined("DEVELOPER_MODE") && !defined("IS_UPDATE") && file_exists(ROOT . "updater")) {
    if ($_SERVER['HTTP_HOST'] != "localhost") {
        echo "Please delete the updater directory before using SourceBans";
        die();
    }
}

// ---------------------------------------------------
//  Setup our custom error handler
// ---------------------------------------------------
set_error_handler('sbError');

function sbError($errno, $errstr, $errfile, $errline) {
    switch ($errno) {
        case E_USER_ERROR:
            $msg = "[{$errno}] {$errstr}<br />\n";
            $msg .= "Fatal error on line {$errline} in file {$errfile}";
            new CSystemLog("e", "PHP Error", $msg);
            exit(1);
            break;

        case E_USER_WARNING:
            $msg = "[{$errno}] {$errstr}<br />\n";
            $msg .= "Error on line {$errline} in file {$errfile}";
            new CSystemLog("w", "PHP Warning", $msg);
            break;

        case E_USER_NOTICE:
            $msg = "[{$errno}] {$errstr}<br />\n";
            $msg .= "Notice on line {$errline} in file {$errfile}";
            new CSystemLog("m", "PHP Notice", $msg);
            break;

        default:
            return false;
    }

    /* Don't execute PHP internal error handler */
    return true;
}

// ---------------------------------------------------
//  Some defs
// ---------------------------------------------------
define('STEAM_FORMAT', "/^((STEAM_[0-9]:[0-9]:[0-9]+)|(\[[A-Z]:[0-9]:[0-9]+\]))$/");

// Web admin-flags
define('ADMIN_LIST_ADMINS', (1 << 0));
define('ADMIN_ADD_ADMINS', (1 << 1));
define('ADMIN_EDIT_ADMINS', (1 << 2));
define('ADMIN_DELETE_ADMINS', (1 << 3));

define('ADMIN_LIST_SERVERS', (1 << 4));
define('ADMIN_ADD_SERVER', (1 << 5));
define('ADMIN_EDIT_SERVERS', (1 << 6));
define('ADMIN_DELETE_SERVERS', (1 << 7));

define('ADMIN_ADD_BAN', (1 << 8));
define('ADMIN_EDIT_OWN_BANS', (1 << 10));
define('ADMIN_EDIT_GROUP_BANS', (1 << 11));
define('ADMIN_EDIT_ALL_BANS', (1 << 12));
define('ADMIN_BAN_PROTESTS', (1 << 13));
define('ADMIN_BAN_SUBMISSIONS', (1 << 14));
define('ADMIN_DELETE_BAN', (1 << 25));
define('ADMIN_UNBAN', (1 << 26));
define('ADMIN_BAN_IMPORT', (1 << 27));
define('ADMIN_UNBAN_OWN_BANS', (1 << 30));
define('ADMIN_UNBAN_GROUP_BANS', (1 << 31));

define('ADMIN_LIST_GROUPS', (1 << 15));
define('ADMIN_ADD_GROUP', (1 << 16));
define('ADMIN_EDIT_GROUPS', (1 << 17));
define('ADMIN_DELETE_GROUPS', (1 << 18));

define('ADMIN_WEB_SETTINGS', (1 << 19));

define('ADMIN_LIST_MODS', (1 << 20));
define('ADMIN_ADD_MODS', (1 << 21));
define('ADMIN_EDIT_MODS', (1 << 22));
define('ADMIN_DELETE_MODS', (1 << 23));

define('ADMIN_NOTIFY_SUB', (1 << 28));
define('ADMIN_NOTIFY_PROTEST', (1 << 29));

define('ADMIN_OWNER', (1 << 24));

// Server admin-flags
define('SM_RESERVED_SLOT', "a");
define('SM_GENERIC', "b");
define('SM_KICK', "c");
define('SM_BAN', "d");
define('SM_UNBAN', "e");
define('SM_SLAY', "f");
define('SM_MAP', "g");
define('SM_CVAR', "h");
define('SM_CONFIG', "i");
define('SM_CHAT', "j");
define('SM_VOTE', "k");
define('SM_PASSWORD', "l");
define('SM_RCON', "m");
define('SM_CHEATS', "n");
define('SM_ROOT', "z");

define('SM_CUSTOM1', "o");
define('SM_CUSTOM2', "p");
define('SM_CUSTOM3', "q");
define('SM_CUSTOM4', "r");
define('SM_CUSTOM5', "s");
define('SM_CUSTOM6', "t");


define('ALL_WEB', ADMIN_LIST_ADMINS
    | ADMIN_ADD_ADMINS
    | ADMIN_EDIT_ADMINS
    | ADMIN_DELETE_ADMINS
    | ADMIN_LIST_SERVERS
    | ADMIN_ADD_SERVER
    | ADMIN_EDIT_SERVERS
    | ADMIN_DELETE_SERVERS
    | ADMIN_ADD_BAN
    | ADMIN_EDIT_OWN_BANS
    | ADMIN_EDIT_GROUP_BANS
    | ADMIN_EDIT_ALL_BANS
    | ADMIN_BAN_PROTESTS
    | ADMIN_BAN_SUBMISSIONS
    | ADMIN_LIST_GROUPS
    | ADMIN_ADD_GROUP
    | ADMIN_EDIT_GROUPS
    | ADMIN_DELETE_GROUPS
    | ADMIN_WEB_SETTINGS
    | ADMIN_LIST_MODS
    | ADMIN_ADD_MODS
    | ADMIN_EDIT_MODS
    | ADMIN_DELETE_MODS
    | ADMIN_OWNER
    | ADMIN_DELETE_BAN
    | ADMIN_UNBAN
    | ADMIN_BAN_IMPORT
    | ADMIN_UNBAN_OWN_BANS
    | ADMIN_UNBAN_GROUP_BANS
    | ADMIN_NOTIFY_SUB
    | ADMIN_NOTIFY_PROTEST
);

define('ALL_SERVER', SM_RESERVED_SLOT 
    . SM_GENERIC 
    . SM_KICK 
    . SM_BAN 
    . SM_UNBAN 
    . SM_SLAY 
    . SM_MAP 
    . SM_CVAR 
    . SM_CONFIG 
    . SM_VOTE 
    . SM_PASSWORD
    . SM_RCON
    . SM_CHEATS
    . SM_CUSTOM1
    . SM_CUSTOM2
    . SM_CUSTOM3
    . SM_CUSTOM4
    . SM_CUSTOM5
    . SM_CUSTOM6
    . SM_ROOT
);

$GLOBALS['db']->Execute("SET NAMES utf8;");

$res = $GLOBALS['db']->CacheExecute(86400, "SELECT * FROM " . DB_PREFIX . "_settings GROUP BY `setting`, `value`");
$GLOBALS['config'] = array();
while (!$res->EOF) {
    $setting = array($res->fields['setting'] => $res->fields['value']);
    $GLOBALS['config'] = array_merge_recursive($GLOBALS['config'], $setting);
    $res->MoveNext();
}

define('SB_BANS_PER_PAGE', $GLOBALS['config']['banlist.bansperpage']);
define('MIN_PASS_LENGTH', $GLOBALS['config']['config.password.minlength']);
$dateformat = !empty($GLOBALS['config']['config.dateformat']) ? $GLOBALS['config']['config.dateformat'] : "m-d-y H:i";
define('DATE_FORMAT', $dateformat);

$offset = (empty($GLOBALS['config']['config.timezone']) ? 0 : $GLOBALS['config']['config.timezone']) * 3600;
$timeZone = "GMT";
$abbrarray = timezone_abbreviations_list();
foreach ($abbrarray as $abbr) {
    foreach ($abbr as $city) {
        if ($city['offset'] == $offset && $city['dst'] == $GLOBALS['config']['config.summertime']) {
            $timeZone = $city['timezone_id'];
            break 2;
        }
    }
}

date_default_timezone_set($timeZone);

include_once(INCLUDES_PATH . "user-functions.php");
include_once(INCLUDES_PATH . "system-functions.php");

require(INCLUDES_PATH . 'smarty'.DIRECTORY_SEPARATOR.'Smarty.class.php');

global $theme, $userbank, $language;

$language = new CLanguage;
$lang = $language->getLanguage();

$theme_name = isset($GLOBALS['config']['config.theme']) ? $GLOBALS['config']['config.theme'] : 'default';
if (defined("IS_UPDATE")) {
    $theme_name = "default";
}
define('SB_THEME', $theme_name);

if (!@file_exists(SB_THEMES . $theme_name . DIRECTORY_SEPARATOR . "theme.conf.php")) {
    die("Theme Error: <b>" . $theme_name . "</b> is not a valid theme. Must have a valid <b>theme.conf.php</b> file.");
}

$theme = new Smarty();
$theme->error_reporting = 0;
$theme->debugging = false;
$theme->use_sub_dirs = false;
$theme->compile_id = $lang . '_' . $theme_name;
$theme->caching = false;
$theme->template_dir = SB_THEMES . $theme_name;
$theme->compile_dir = SB_THEMES_COMPILE;

if ((isset($_GET['debug']) && $_GET['debug'] == 1) || defined("DEVELOPER_MODE")) {
    $theme->force_compile = true;
}
// ---------------------------------------------------
// Setup our user manager
// ---------------------------------------------------
$userbank = new CUserManager(isset($_COOKIE['aid']) ? $_COOKIE['aid'] : '', isset($_COOKIE['password']) ? $_COOKIE['password'] : '');
