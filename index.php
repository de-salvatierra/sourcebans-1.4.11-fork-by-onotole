<?php 
/**
 * =============================================================================
 * Main loader file
 * 
 * @author SteamFriends Development Team
 * @version 1.0.0
 * @copyright SourceBans (C)2007 SteamFriends.com.  All rights reserved.
 * @package SourceBans
 * @link http://www.sourcebans.net
 * 
 * @version $Id: index.php 24 2007-11-06 18:17:05Z olly $
 * =============================================================================
 */

include 'init.php';
include_once(INCLUDES_PATH . "sb-callback.php");

include(INCLUDES_PATH . "page-builder.php");
