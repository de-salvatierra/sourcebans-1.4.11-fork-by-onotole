<?php
return array(
    'Table installation' => 'Создание таблиц',
    'Error' => 'Ошибка',
    'There was an error creating the table structure. Please read the message above to help debug the problem.' => 'Возникла ошибка при создании структуры таблиц. Пожалуйста, прочитайте сообщение выше, чтобы выявить проблему.',
    'Success' => 'Успешно',
    'The tables were created successfully' => 'Таблицы успешно созданы',
    'Ok' => 'Продолжить',
    'There were some errors in your setup that prevent SourceBans from being installed. <br />Please refer to the documentation to find possible fixes for these problems.' => '',
    'Errors' => 'Ошибки',
);
