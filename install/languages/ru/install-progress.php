<?php
return array(
    'Installation Progress' => 'Прогресс установки',
    'Step 1: License Agreement' => 'Шаг 1: Лицензия',
    'Step 2: Database Information' => 'Шаг 2: База данных',
    'Step 3: System Requirements' => 'Шаг 3: Система',
    'Step 4: Table Creation' => 'Шаг 4: Создание таблиц',
    'Step 5: Initial Setup' => 'Шаг 5: Начало установки',
    
    'Step 6 - AMXBans Import' => 'Шаг 6 - Импорт из AmxBans',
    'Step 5 - Setup' => 'Шаг 5 - Установка',
    'Step 4 - Table Creation' => 'Шаг 4 - ',
    'Step 3 - System Requirements Check' => 'Шаг 3 - Проверка системных требований',
    'Step 2 - Database Details' => 'Шаг 2 - Данные подключения к серверу баз данных',
    'Step 1 - License agreement' => 'Шаг 1 - Лицензионное соглашение',
);
