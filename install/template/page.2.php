<?php
if(!defined("IN_SB")){
    echo "You should not be here. Only follow links!";
    die();
}
if(isset($_POST['postd'])) {
    if(
        empty($_POST['server'])
            ||
        empty($_POST['port'])
            ||
        empty($_POST['username'])
            ||
        empty($_POST['database'])
            ||
        empty($_POST['prefix'])
    ) {
        echo "<script>ShowBox('".t('page2', 'Error')."', '".t('page2', 'There is some missing data. All fields are required.')."', 'red', '', true);</script>";
    }
    else
    {
        require(ROOT . "../includes/adodb/adodb.inc.php");
        include_once(ROOT . "../includes/adodb/adodb-errorhandler.inc.php");
        $server = "mysqli://" . $_POST['username'] . ":" . $_POST['password'] . "@" . $_POST['server'] . ":" . $_POST['port'] . "/" . $_POST['database'];
        $db = ADONewConnection($server);
        if(!$db) {
            echo "<script>ShowBox('".t('page2', 'Error')."', '".t('page2', 'There was an error connecting to your database. <br />Recheck the details to make sure they are correct')."', 'red', '', true);</script>";
        } else if(strlen($_POST['prefix']) > 9) {
            echo "<script>ShowBox('".t('page2', 'Error')."', '".t('page2', 'The prefix cannot be longer than 9 characters.<br />Correct this and submit again.')."', 'red', '', true);</script>";
        } else {
            ?>
            <form action="index.php?step=3" method="post" name="send" id="send">
                <input type="hidden" name="username" value="<?php echo $_POST['username']?>">
                <input type="hidden" name="password" value="<?php echo $_POST['password']?>">
                <input type="hidden" name="server" value="<?php echo $_POST['server']?>">
                <input type="hidden" name="database" value="<?php echo $_POST['database']?>">
                <input type="hidden" name="port" value="<?php echo $_POST['port']?>">
                <input type="hidden" name="prefix" value="<?php echo $_POST['prefix']?>">
            </form>
            <script>
                $('send').submit();
            </script> <?php
        }
    }
}
?>
<form action="" method="post" name="submit" id="submit">
<div id="install-progress">
<b><u><?php echo t('install-progress', 'Installation Progress')?></u></b><br />
<strike><?php echo t('install-progress', 'Step 1: License Agreement')?></strike><br />
<b><?php echo t('install-progress', 'Step 2: Database Information')?></b><br />
<?php echo t('install-progress', 'Step 3: System Requirements')?><br />
<?php echo t('install-progress', 'Step 4: Table Creation')?><br />
<?php echo t('install-progress', 'Step 5: Initial Setup')?><br />
</div>
<?php echo t('page2', 'Hover your mouse over the \'?\' buttons to see an explanation of the field.')?>
<br /><br />
<div id="submit-main-full"><h3><?php echo t('page2', 'MySQL Information')?></h3>
<table width="90%" style="border-collapse:collapse;" id="group.details" cellpadding="3">
  <tr>
    <td valign="top" width="35%">
      <div class="rowdesc">
        <?php echo HelpIcon(t('page2', 'server'), t('page2', 'Type the ip, or hostname to your MySQL server'));?>
        <?php echo t('page2', 'server')?>
      </div>
    </td>
    <td>
        <div align="left">
            <input type="text" TABINDEX=1 class="inputbox" id="server" name="server" value="<?php echo isset($_POST['server'])?$_POST['server']:'localhost';?>" />
        </div>
        <div id="server.msg" style="color:#CC0000;"></div>
    </td>
  </tr>
  <tr>
    <td valign="top" width="35%">
        <div class="rowdesc">
            <?php echo HelpIcon(t('page2', 'Port'), t('page2', 'Type the port that your MySQL server is running on'));?>
            <?php echo t('page2', 'Port')?>
        </div>
    </td>
    <td>
        <div align="left">
            <input type="text" TABINDEX=1 class="inputbox" id="port" name="port" value="<?php echo isset($_POST['port'])?$_POST['port']:3306;?>" />
        </div>
        <div id="port.msg" style="color:#CC0000;"></div>
    </td>
  </tr>
  <tr>
    <td valign="top" width="35%">
        <div class="rowdesc">
            <?php echo HelpIcon(t('page2', 'Username'), t('page2', 'Type your MySQL username'));?>
            <?php echo t('page2', 'Username')?>
        </div>
    </td>
    <td>
        <div align="left">
            <input type="text" TABINDEX=1 class="inputbox" id="username" name="username" value="<?php echo isset($_POST['username'])?$_POST['username']:'';?>" />
        </div>
        <div id="user.msg" style="color:#CC0000;"></div>
    </td>
  </tr>
  
   <tr>
        <td valign="top" width="35%">
            <div class="rowdesc">
                <?php echo HelpIcon(t('page2', 'Password'), t('page2', 'Type your MySQL password'));?>
                <?php echo t('page2', 'Password')?>
            </div>
        </td>
        <td>
            <div align="left">
                <input type="password" TABINDEX=1 class="inputbox" id="password" name="password" value="<?php echo isset($_POST['password'])?$_POST['password']:'';?>" />
            </div>
            <div id="password.msg" style="color:#CC0000;"></div>
        </td>
    </tr>
  
    <tr>
        <td valign="top" width="35%">
            <div class="rowdesc">
                <?php echo HelpIcon(t('page2', 'Database'), t('page2', 'Type name of the database you want to use for SourceBans'));?>
                <?php echo t('page2', 'Database')?>
            </div>
        </td>
        <td>
            <div align="left">
                <input type="text" TABINDEX=1 class="inputbox" id="database" name="database" value="<?php echo isset($_POST['database'])?$_POST['database']:'';?>" />
            </div>
            <div id="database.msg" style="color:#CC0000;"></div>
        </td>
    </tr>
  
    <tr>
        <td valign="top" width="35%">
            <div class="rowdesc">
                <?php echo HelpIcon(t('page2', 'Prefix'), t('page2', 'Type a prefix you want to use for the tables'));?>
                <?php echo t('page2', 'Prefix')?>
            </div>
        </td>
        <td>
            <div align="left">
                <input type="text" TABINDEX=1 class="inputbox" id="prefix" name="prefix" value="<?php echo isset($_POST['prefix'])?$_POST['prefix']:'sb';?>" />
            </div>
            <div id="database.msg" style="color:#CC0000;"></div>
        </td>
    </tr>
 </table>

<div align="center">
    <input type="submit" TABINDEX=2 onclick="" name="button" class="btn ok" id="button" value="<?php echo t('page2', 'Ok')?>" />
</div>
<input type="hidden" name="postd" value="1">
</div>
</form>
<script type="text/javascript">
	$E('html').onkeydown = function(event){
	    var event = new Event(event);
	    if (event.key == 'enter' ) $('submit').submit();
	}
</script>