<?php
if(!defined("IN_SB")){
    echo "You should not be here. Only follow links!";
    die();
}
$errors = 0;
$warnings = 0;

require(ROOT . "../includes/adodb/adodb.inc.php");
include_once(ROOT . "../includes/adodb/adodb-errorhandler.inc.php");
$server = "mysqli://" . $_POST['username'] . ":" . $_POST['password'] . "@" . $_POST['server'] . ":" . $_POST['port'] . "/" . $_POST['database'];
$db = ADONewConnection($server);

$file = file_get_contents(INCLUDES_PATH . "struc.sql");
$file = str_replace("{prefix}", $_POST['prefix'], $file);
$querys = explode(";", $file);
foreach($querys AS $q) {
    if(strlen($q) > 2) {
        $res = $db->Execute(stripslashes($q) . ";");
        if (!$res) {
            $errors++;
        }
    }	
}	
?>
<div id="install-progress">
    <b>
        <u>
            <?php echo t('install-progress', 'Installation Progress')?>
        </u>
    </b>
    <br />
    <strike>
        <?php echo t('install-progress', 'Step 1: License Agreement')?>
    </strike>
    <br />
    <strike>
        <?php echo t('install-progress', 'Step 2: Database Information')?>
    </strike>
    <br />
    <strike>
        <?php echo t('install-progress', 'Step 3: System Requirements')?>
    </strike>
    <br />
    <b>
        <?php echo t('install-progress', 'Step 4: Table Creation')?>
    </b>
    <br />
    <?php echo t('install-progress', 'Step 5: Initial Setup')?>
    <br />
</div>
<br />
<div id="submit-main" style="width:75%;">
    <h3><?php echo t('page4', 'Table installation')?></h3>
    <?php if($errors > 0): ?>
    <script type="text/javascript">
        ShowBox(
            '<?php echo t('page4', 'Error')?>',
            '<?php echo t('page4', 'There was an error creating the table structure. Please read the message above to help debug the problem.')?>',
            'red',
            '',
            true
        );
    </script>
    <?php else:?>
        <script>
            ShowBox('<?php echo t('page4', 'Success')?>', '<?php echo t('page4', 'The tables were created successfully')?>', 'green', '', true);
        </script>
    <?php endif;?>
    <form action="index.php?step=5" method="post" name="send" id="send">
        <input type="hidden" name="username" value="<?php echo $_POST['username']?>">
        <input type="hidden" name="password" value="<?php echo $_POST['password']?>">
        <input type="hidden" name="server" value="<?php echo $_POST['server']?>">
        <input type="hidden" name="database" value="<?php echo $_POST['database']?>">
        <input type="hidden" name="port" value="<?php echo $_POST['port']?>">
        <input type="hidden" name="prefix" value="<?php echo $_POST['prefix']?>">
    </form>
    <div align="center">
        <input 
            type="submit" 
            TABINDEX=2 
            onclick="next();" 
            name="button" 
            class="btn ok" 
            id="button" 
            value="<?php echo t('page4', 'Ok')?>" />
    </div>
</div>
<script type="text/javascript">
    $E('html').onkeydown = function(event){
        var event = new Event(event);
        if (event.key == 'enter' ) next();
    };
    function next()
    {
        var errors = <?php echo $errors?>;
        if(errors > 0) {
            ShowBox('<?php echo t('page4', 'Errors')?>', '<?php echo t('page4', 'There were some errors in your setup that prevent SourceBans from being installed. <br />Please refer to the documentation to find possible fixes for these problems.')?>', 'red', '', true);
        } else {
            $('send').submit();
        }
    }
</script>
