ALTER TABLE  `{prefix}_admins` ADD  `expired` INT( 10 ) UNSIGNED NOT NULL DEFAULT  '0';

INSERT INTO `{prefix}_settings` (`setting`, `value`) VALUES
('config.language', '{language}'),
('banlist.hidesteamid', '1'),
('config.enableclientlanguage', '1'),
('config.email.transport', 'mail'),
('config.email.smtp.login', ''),
('config.email.smtp.password', ''),
('config.email.smtp.host', ''),
('config.email.smtp.port', '25'),
('config.email.from.email', 'noreply@example.com'),
('config.email.from.name', 'SourceBans'),
('config.email.smtp.secure', '');
