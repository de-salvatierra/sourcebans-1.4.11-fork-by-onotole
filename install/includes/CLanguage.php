<?php

class CLanguage {
    
    /**
     *
     * @var string Текущий язык пользователя
     */
    private $language;
    
    /**
     *
     * @var array Имеющиеся языки
     */
    private $languages;
    
    /**
     *
     * @var array Массив со всеми подключенными файлами текущей сессии
     */
    private $_loadedFiles = array();
    
    /**
     * 
     */
    public function __construct() {
        $this->language = $this->getLanguage();
    }
    
    /**
     * 
     * @return array Массив с кодами языка
     */
    public function getLanguages() {
        if(is_array($this->languages)) {
            return $this->languages;
        }
        $langs = array('english');
        foreach(glob(SB_LANGUAGES_PATH . '*') as $dir) {
            $langs[] = basename($dir);
        }
        $this->languages = $langs;
        return $langs;
    }

    public function setClientLanguage($lang) {
        if($lang == $this->getLanguage()) {
            return true;
        }
        $phpSelf = filter_input(INPUT_SERVER, 'PHP_SELF');
        $baseurl =  dirname($phpSelf);
        $expire = strtotime('+1 MONTH');
        return setcookie('sb_language', $lang, $expire, $baseurl);
    }
    
    /**
     * 
     * @return string Текущий язык пользователя
     */
    public function getLanguage() {
        if($this->language) {
            return $this->language;
        }
        
        $cookie = filter_input(INPUT_COOKIE, 'sb_language');
        
        if($cookie) {
            $lang = $cookie;
        }
        
        if(empty($lang)) {
            $lang = 'english';
        }
        
        if(!in_array($lang, $this->getLanguages())) {
            exit('ERROR! WRONG LANGUAGE CODE!!! ('.$lang.')');
        }
        $this->language = $lang;
        return $lang;
    }
    
    /**
     * Переводит строку
     * @param string $section Секция сообщений (Путь к файлу, начиная от папки текущего языка)
     * <br>
     * Например, для Русского языка секция admin/bans значит, что будет использован файл
     * <br>
     * <pre>languages/ru/admin/bans.php</pre>
     * <br>
     * А секция servers означает, что будет использован файл languages/ru/servers.php
     * @param string $message Сообщение, которое будет переведено
     * @param array $params [Optional] Массив с параметрами перевода.
     * Если нужно передать переменную, то можно передать массив параметров
     * [[param_name]] => $some_var
     * И в нужном месте в строке $message вставить шоткод [[param_name]]
     * @return string
     */
    public function translate($section, $message, $params) {
        if($this->language != 'english') {
            $messages = $this->getMessages($section);

            if(!empty($messages[$message])) {
                $message = $messages[$message];
            }
        }
        if(!empty($params)) {
            return str_replace(array_keys($params), array_values($params), $message);
        }
        return $message;
    }
    
    /**
     * Получает полный путь к файлу с переводами
     * @param string $section Секция перевода
     * @return string
     * @throws Exception
     */
    private function getFilePathBySection($section) {
        $path = SB_LANGUAGES_PATH . $this->language . DIRECTORY_SEPARATOR . $section . '.php';
        if(!file_exists($path)) {
            throw new Exception('Language File Not Found In Path: ' . $path);
        }
        return $path;
    }
    
    private function getMessages($section) {
        if (empty($this->_loadedFiles[$section])) {
            $path = $this->getFilePathBySection($section);
            $this->_loadedFiles[$section] = include_once $path;
        }
        return $this->_loadedFiles[$section];
    }
    
    public function getFiles() {
        return $this->_loadedFiles;
    }
}
