<div id="admin-page-content">
    <h3>
        -{t section="modules/bans/admin" message="Email Player <i>[[email_addr]]</i>" email_addr=$email_addr}-
    </h3>
    <table width="90%" style="border-collapse:collapse;" id="group.details" cellpadding="3">
        <tr>
            <td valign="top" width="35%">
                <div class="rowdesc">
                    -{help_icon title="Subject" message="Type the subject of the email." translate="modules/bans/admin"}-
                    -{t section="modules/bans/admin" message="Subject"}-
                </div>
            </td>
            <td>
                <div align="left">
                    <input type="text" TABINDEX=1 class="submit-fields" id="subject" name="subject" />
                </div>
                <div id="subject.msg" class="badentry"></div>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <div class="rowdesc">
                    -{help_icon title="Message" message="Type your message here." translate="modules/bans/admin"}-
                    -{t section="modules/bans/admin" message="Message"}-
                </div>
            </td>
            <td>
                <div align="left">
                    <textarea class="submit-fields" TABINDEX=2 cols="35" rows="7" id="message" name="message"></textarea>
                </div>
                <div id="message.msg" class="badentry"></div>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                -{sb_button text="Send Email" onclick="CheckEmail()" class="ok" id="aemail" submit=false translate="modules/bans/admin"}-
                 &nbsp;
                -{sb_button text="Back" onclick="history.go(-1)" class="cancel" id="back" submit=false translate="modules/bans/admin"}-
            </td>
        </tr>
    </table>
</div>
<script>
    function CheckEmail()
    {
        var err = 0, type = '-{$email_type}-', id = -{$email_id}-;
        if($('subject').value == "") {
            $('subject.msg').setHTML('-{t section="modules/bans/admin" message="You must type a subject for the email."}-');
            $('subject.msg').setStyle('display', 'block');
            err++;
        } else {
            $('subject.msg').setHTML('');
            $('subject.msg').setStyle('display', 'none');
        }

        if($('message').value == "") {
            $('message.msg').setHTML('-{t section="modules/bans/admin" message="You must type a message for the email."}-');
            $('message.msg').setStyle('display', 'block');
            err++;
        } else {
            $('message.msg').setHTML('');
            $('message.msg').setStyle('display', 'none');
        }

        if(err>0)
            return;
        xajax_SendMail($('subject').value, $('message').value, type, id);
    }
</script>