{if NOT $permission_addban}
	Access Denied!
{else}
	{if NOT $groupbanning_enabled}
		{t section="modules/bans/admin" message="This feature is disabled! Only follow links!"}
	{else}
		<h3>{t section="modules/bans/admin" message="Add Group Ban"}</h3>
		{if NOT $list_steam_groups}
		{t section="modules/bans/admin" message="Here you can add a ban for a whole steam community group."}
        <br />
		{t section="modules/bans/admin" message="e.g."}
        <strong><i>http://steamcommunity.com/groups/interwavestudios</i></strong>
        <br />
        <br />
		<table width="90%" style="border-collapse:collapse;" id="group.details" cellpadding="3">
            <tr>
                <td valign="top" width="35%">
                    <div class="rowdesc">
                        {help_icon 
                            title="Group Link"
                            message="Type the link to a steam community group."
                            translate="modules/bans/admin"
                        }
                        {t section="modules/bans/admin" message="Group Link"}
                    </div>
                </td>
                <td>
                    <div align="left">
                        <input type="text" TABINDEX=1 class="submit-fields" id="groupurl" name="groupurl" />
                    </div>
                    <div id="groupurl.msg" class="badentry"></div>
                </td>
            </tr>
            <tr>
                <td valign="top" width="35%">
                    <div class="rowdesc">
                        {help_icon 
                            title="Group Ban Reason"
                            message="Type the reason, why you are going to ban this steam community group."
                            translate="modules/bans/admin"
                        }
                        {t section="modules/bans/admin" message="Group Ban Reason"}
                    </div>
                </td>
                <td>
                    <div align="left">
                        <textarea 
                            class="submit-fields" 
                            TABINDEX=2 
                            cols="30" 
                            rows="5" 
                            id="groupreason" name="groupreason" /></textarea>
                    </div>
                    <div id="groupreason.msg" class="badentry"></div>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    {sb_button
                        text="Add Group Ban"
                        onclick="ProcessGroupBan();"
                        class="ok" id="agban" submit=false
                        translate="modules/bans/admin"
                    }
                    &nbsp;
                    {sb_button
                        text="Back"
                        onclick="history.go(-1)"
                        class="cancel" id="aback"
                        translate="modules/bans/admin"
                    }
                </td>
            </tr>
		</table>
		{else}
            {t 
                section="modules/bans/admin" 
                message="All groups the player  is member of [[player_name]] are listed here."
                player_name=$list_steam_groups
            }
        <br />
		{t section="modules/bans/admin" message="Choose the steam groups you want to ban."}
        <br />
        <br />
		<div id="steamGroupsText" name="steamGroupsText">
            <b>{t section="modules/bans/admin" message="Loading the groups..."}</b>
        </div>
		<div id="steamGroups" name="steamGroups" style="display:none;">
			<table id="steamGroupsTable" name="steamGroupsTable" border="0" width="500px">
                <tr>
                    <td height="16" class="listtable_1" style="padding:0px;width:3px;" align="center">
                        <div 
                            class="ok" 
                            style="height:16px;width:16px;cursor:pointer;" 
                            id="tickswitch" 
                            name="tickswitch" 
                            onclick="TickSelectAll();"></div>
                    </td>
                    <td height="16" class="listtable_top" align="center">
                        <b>{t section="modules/bans/admin" message="Group"}</b>
                    </td>
                </tr>
			</table>
			&nbsp;&nbsp;L&nbsp;&nbsp;
            <a 
                href="#" 
                onclick="TickSelectAll();return false;" 
                title="{t section="modules/bans/admin" message="Select All"}" 
                name="tickswitchlink" 
                id="tickswitchlink">
                {t section="modules/bans/admin" message="Select All"}
            </a>
            <br />
            <br />
			<table width="90%" style="border-collapse:collapse;" id="group.details" cellpadding="3">
				<tr>
					<td valign="top" width="35%">
						<div class="rowdesc">
							{help_icon
                                title="Group Ban Reason"
                                message="Type the reason, why you are going to ban this steam community group."
                                translate="modules/bans/admin"
                            }
                            {t section="modules/bans/admin" message="Group Ban Reason"}
						</div>
					</td>
					<td>
						<div align="left">
							<textarea 
                                class="submit-fields" 
                                TABINDEX=2 
                                cols="30" 
                                rows="5" 
                                id="groupreason" 
                                name="groupreason" /></textarea>
						</div>
						<div id="groupreason.msg" class="badentry"></div>
					</td>
				</tr>
			</table>
			<input 
                type="button" 
                class="btn ok" 
                onclick="CheckGroupBan();" 
                name="gban" 
                id="gban" 
                onmouseover="ButtonOver('gban');" 
                onmouseout="ButtonOver('gban');"
                value="{t section="modules/bans/admin" message="Add Group Ban"}">
		</div>
		<div id="steamGroupStatus" name="steamGroupStatus" width="100%"></div>
		<script type="text/javascript">
            $('tickswitch').value = 0;
            xajax_GetGroups('{$list_steam_groups}');
        </script>
		{/if}
	{/if}
{/if}
<script>
    {literal}
    function TickSelectAll()
    {
        for(var i=0;$('chkb_' + i);i++) {
            if($('tickswitch').value==0) {
                $('chkb_' + i).checked = true;
            } else {
                $('chkb_' + i).checked = false;
            }
        }
        if($('tickswitch').value==0) {
    {/literal}
            $('tickswitch').value=1;
            $('tickswitch').setProperty('title', '{t section="bans" message="Deselect All"}');
            $('tickswitchlink').setProperty('title', '{t section="bans" message="Deselect All"}');
            $('tickswitchlink').innerHTML = '{t section="bans" message="Deselect All"}';
    {literal}
        } else {
    {/literal}
            $('tickswitch').value=0;
            $('tickswitch').setProperty('title', '{t section="bans" message="Select All"}');
            $('tickswitchlink').setProperty('title', '{t section="bans" message="Select All"}');
            $('tickswitchlink').innerHTML = '{t section="bans" message="Select All"}';
    {literal}
        }
    }
    {/literal}
</script>