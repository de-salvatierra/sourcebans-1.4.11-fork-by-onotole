{if NOT $permission_add}
	{t section="admin/mods" message="Access Denied!"}
{else}
	<div id="add-group1">
		<h3>{t section="admin/mods" message="Add Mod"}</h3>
		{t section="admin/mods" message="For more information or help regarding a certain subject move your mouse over the question mark."}
        <br />
        <br />
		<table width="90%" style="border-collapse:collapse;" id="group.details" cellpadding="3">
			<tr>
		    	<td valign="top" width="35%">
		    		<div class="rowdesc">
		    			{help_icon title="Mod Name" message="Type the name of the mod you are adding." translate="admin/mods"}
                        {t section="admin/mods" message="Mod Name"}
		    		</div>
		    	</td>
		    	<td>
		    		<div align="left">
		    			<input type="hidden" id="fromsub" value="" />
		      			<input type="text" TABINDEX=1 class="submit-fields" id="name" name="name" />
		    		</div>
		    		<div id="name.msg" class="badentry"></div>
		    	</td>
			</tr>
		  	<tr>
		    	<td valign="top">
		    		<div class="rowdesc">
		    			{help_icon
                            title="Folder Name"
                            message="Type the name of this mods folder. For example, Counter-Strike: Source\'s mod folder is \'cstrike\'"
                            translate="admin/mods"
                        }
                        {t section="admin/mods" message="Mod Folder"}
		    		</div>
		    	</td>
		    	<td>
			    	<div align="left">
			      		<input type="text" TABINDEX=2 class="submit-fields" id="folder" name="folder" />
			    	</div>
			    	<div id="folder.msg" class="badentry"></div>
				</td>
			</tr>
      <tr>
				<td valign="top">
                    <div class="rowdesc">
                        {help_icon
                            title="Steam Universe Number"
                            message="(STEAM_<b>X</b>:Y:Z) Some games display the steamid differently than others. Type the first number in the SteamID (<b>X</b>) depending on how it's rendered by this mod. (Default: 0)."
                            translate="admin/mods"
                        }
                        {t section="admin/mods" message="Steam Universe Number"}
                    </div>
                </td>
		    	<td>
		    		<div align="left">
		      			<input type="text" TABINDEX=3 class="submit-fields" id="steam_universe" name="steam_universe" value="0" />
		    		</div>
		    	</td>
		  </tr>
      <tr>
			<td valign="top">
                <div class="rowdesc">
                    {help_icon
                        title="Mod Enabled"
                        message="Select if this mod is enabled and assignable to bans and servers."
                        translate="admin/mods"
                    }
                    {t section="admin/mods" message="Enabled"}
                </div>
            </td>
		    	<td>
		    		<div align="left">
		      			<input type="checkbox" TABINDEX=4 id="enabled" name="enabled" value="1" checked="checked" />
		    		</div>
		    	</td>
		  </tr>
		  	<tr>
		    	<td valign="top" width="35%">
		    		<div class="rowdesc">
		    			{help_icon
                            title="Upload Icon"
                            message="Click here to upload an icon to associate with this mod."
                            translate="admin/mods"
                        }
                        {t section="admin/mods" message="Upload Icon"}
		    		</div>
		    	</td>
		    	<td>
		    		<div align="left">
		      			{sb_button 
                            text="Upload MOD Icon"
                            onclick="childWindow=open('pages/admin.uploadicon.php','upload','resizable=yes,width=300,height=160');"
                            class="save"
                            id="upload"
                            submit=false
                            translate="admin/mods"
                        }
		    		</div>
		    		<div id="icon.msg" style="color:#CC0000;"></div>
		    	</td>
		  	</tr>
		 	<tr>
		    	<td>&nbsp;</td>
		    	<td>		
			     	{sb_button
                        text="Add Mod"
                        onclick="ProcessMod();"
                        class="ok"
                        id="amod"
                        translate="admin/mods"
                    }
                    &nbsp;
			     	{sb_button text="Back" onclick="history.go(-1)" class="cancel" id="aback" translate="admin/mods"}      
		      	</td>
		  	</tr>
		</table>
	</div>
    <script>
        var icname = "";
        {literal}
        function icon(name)
        {
        {/literal}
            $('icon.msg').setHTML('{t section="admin/mods" message="Uploaded"} ' + ":" +name);
        {literal}
            icname = name;
            if($('icon_hid')) {
                $('icon_hid').value = name;
            }
        }
        function ProcessMod()
        {
            var err = 0;
            if(!$('name').value) {
        {/literal}
                $('name.msg').setHTML('{t section="admin/mods" message="You must enter the name of the mod you are adding."}');
                $('name.msg').setStyle('display', 'block');
                err++;
        {literal}
            } else {
                $('name.msg').setHTML('');
                $('name.msg').setStyle('display', 'none');
            }

            if(!$('folder').value) {
        {/literal}
                $('folder.msg').setHTML('{t section="admin/mods" message="You must enter mod's folder name."}');
                $('folder.msg').setStyle('display', 'block');
                err++;
        {literal}
            } else {
                $('folder.msg').setHTML('');
                $('folder.msg').setStyle('display', 'none');
            }

            if(err) {
                return 0;
            }

            xajax_AddMod($('name').value,
                         $('folder').value,
                         icname,
                         $('steam_universe').value,
                         $('enabled').checked);
        }
        {/literal}
    </script>
{/if}
