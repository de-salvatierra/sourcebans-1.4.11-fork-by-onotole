<div id="admin-page-content">
	<div id="0">
        <h3>SourceBans Database Config</h3>
        {t
            section="modules/servers/admin"
            message="This code must be added <b>INSIDE</b> the <b>/[mod]/addons/sourcemod/configs/databases.cfg</b> on your game server"
        }
        <br>
        <br />
        <table width="90%" style="border-collapse:collapse;" id="group.details" cellpadding="3">
            <tr>
                <td>
                    <textarea cols="75" rows="23" readonly>{$conf}</textarea>
                </td>
            </tr>
            <tr>
                <td align="center">
                    {sb_button
                        text="Back"
                        onclick="history.go(-1)"
                        class="cancel"
                        id="aconf"
                        submit=false
                        translate="modules/servers/admin"
                    }
                </td>
            </tr>
        </table>
    </div>
</div>