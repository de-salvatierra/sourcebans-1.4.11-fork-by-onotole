<div id="admin-page-content">
    <div id="add-group">
        <h3>-{t section="admin/groups" message="Edit Group"}-</h3>
        <br />
        <input type="hidden" id="group_id" value="-{$aid}-">
        <table width="90%" style="border-collapse:collapse;" id="group.details" cellpadding="3">
            <tr>
                <td valign="top" width="35%">
                    <div class="rowdesc">
                        -{help_icon title="Group Name" message="Type the name of the new group you want to create." translate="admin/groups"}-
                        -{t section="admin/groups" message="Group Name"}-
                    </div>
                </td>
                <td>
                    <div align="left">
                        <input type="text" TABINDEX=1 class="inputbox" id="groupname" name="groupname" />
                    </div>
                    <div id="groupname.msg" style="color:#CC0000;"></div>
                </td>
            </tr>
        </table>
        -{$permissions}-
        -{if $group_type == "srv"}-
        <br />
        <form action="" method="post" name="group_overrides_form">
            <div class="rowdesc">
                -{t section="admin/groups" message="Group Overrides"}-
            </div>
            -{t section="admin/groups" message="Group Overrides allow specific commands or groups of commands to be completely allowed or denied to members of the group."}-
            <br />
            <i>-{t section="admin/groups" message='Read about <a href="http://wiki.alliedmods.net/Adding_Groups_%28SourceMod%29" title="Adding Groups (SourceMod)" target="_blank"><b>group overrides</b></a> in the AlliedModders Wiki!'}-</i>
            <br />
            <br />
            -{t section="admin/groups" message="Blanking out an overrides' name will delete it."}-
            <br />
            <br />
            <table align="center" cellspacing="0" cellpadding="4" id="overrides" width="90%">
                <tr>
                    <td class="tablerow4">-{t section="admin/groups" message="Type"}-</td>
                    <td class="tablerow4">-{t section="admin/groups" message="Name"}-</td>
                    <td class="tablerow4">-{t section="admin/groups" message="Access"}-</td>
                </tr>
                -{foreach from=$overrides_list item="override"}-
                <tr>
                    <td class="tablerow1">
                        <select name="override_type[]">
                            <option value="command"-{if $override.type == "command"}- selected="selected"-{/if}->
                                -{t section="admin/groups" message="Command"}-
                            </option>
                            <option value="group"-{if $override.type == "group"}- selected="selected"-{/if}->
                                -{t section="admin/groups" message="Group"}-
                            </option>
                        </select>
                        <input type="hidden" name="override_id[]" value="-{$override.id}-" />
                    </td>
                    <td class="tablerow1">
                        <input name="override_name[]" value="-{$override.name|strip_tags}-" />
                    </td>
                    <td class="tablerow1">
                        <select name="override_access[]">
                            <option value="allow"-{if $override.access == "allow"}- selected="selected"-{/if}->
                                -{t section="admin/groups" message="Allow"}-
                            </option>
                            <option value="deny"-{if $override.access == "deny"}- selected="selected"-{/if}->
                                -{t section="admin/groups" message="Deny"}-
                            </option>
                        </select>
                    </td>
                </tr>
                -{/foreach}-
                <tr>
                    <td class="tablerow1">
                        <select id="new_override_type">
                            <option value="command">-{t section="admin/groups" message="Command"}-</option>
                            <option value="group">-{t section="admin/groups" message="Group"}-</option>
                        </select>
                    </td>
                    <td class="tablerow1"><input id="new_override_name" /></td>
                    <td class="tablerow1">
                        <select id="new_override_access">
                            <option value="allow">-{t section="admin/groups" message="Allow"}-</option>
                            <option value="deny">-{t section="admin/groups" message="Deny"}-</option>
                        </select>
                    </td>
                </tr>
            </table>
        </form>
        -{/if}-
        <table width="100%">
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr align="center">
            <td>&nbsp;</td>
                <td>
                    <div align="center">
                        <input
                            type="button"
                            class="btn ok"
                            id="editgroup"
                            onclick="ProcessEditGroup('-{$group_type}-', $('groupname').value);"
                            value="-{t section="admin/groups" message="Save Changes"}-" />
                        &nbsp;
                        -{sb_button text="Back" onclick="history.go(-1)" class="cancel" id="back" submit=false translate="admin/groups"}- 
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
<script>
    function ProcessEditGroup(type, name)
    {
        var Mask = BoxToMask();
        var srvMask = BoxToSrvMask();
        var group = $('group_id').value;

        if(name == ""){
            ShowBox(
                '-{t section="admin/groups" message="Error"}-',
                '-{t section="admin/groups" message="You have to type a name for the group."}-',
                "red",
                "",
                true
            );
            $('groupname.msg').innerHTML = '-{t section="admin/groups" message="You have to type a name for the group."}-';
            $('groupname.msg').setStyle('display', 'block');
            return;
        } else {
            $('groupname.msg').innerHTML = '';
            $('groupname.msg').setStyle('display', 'none');
        }

        if($('immunity') && !IsNumeric($('immunity').value)) {
            ShowBox(
                '-{t section="admin/groups" message="Error"}-',
                '-{t section="admin/groups" message="Immunity must be a numerical value (0-9)"}-',
                "red",
                "",
                true
            );
            return;
        }

        var overrides = [];
        var new_override = {};

        // Handle group overrides
        if(type == "srv") {
            var override_id = document.group_overrides_form.elements["override_id[]"];
            //alert(typeof override_id);
            //    return;
            // Are there any old overrides to change?
            if(override_id != null)
            {
                var override_type = document.group_overrides_form.elements["override_type[]"];
                var override_name = document.group_overrides_form.elements["override_name[]"];
                var override_access = document.group_overrides_form.elements["override_access[]"];

                // Make sure they're arrays!
                if($type(override_id) == "element")
                    override_id = [override_id];
                if($type(override_type) == "element")
                    override_type = [override_type];
                if($type(override_name) == "element")
                    override_name = [override_name];
                if($type(override_access) == "element")
                    override_access = [override_access];

                overrides = new Array(override_id.length);

                for(var i=0;i<override_id.length;i++) {
                    overrides[i] = {
                        'id': override_id[i].value,
                        'type': override_type[i][override_type[i].selectedIndex].value,
                        'name': override_name[i].value,
                        'access': override_access[i][override_access[i].selectedIndex].value
                    };
                }
            }

            new_override = {
                'type': $('new_override_type')[$('new_override_type').selectedIndex].value,
                'name': $('new_override_name').value,
                'access': $('new_override_access')[$('new_override_access').selectedIndex].value
            };
        }

        xajax_EditGroup(group, Mask, srvMask, type, name, overrides, new_override);
    }
</script>