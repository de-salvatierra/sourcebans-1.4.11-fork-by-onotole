{if not $permission_listadmin}
	{t section="admin/admins" message="Access Denied"}
{else}

<h3>
    {t section="admin/admins" message="Admins"} (<span id="admincount">{$admin_count}</span>)
</h3>
{t section="admin/admins" message="Click on an admin to see more detailed information and actions to perform on them."}<br /><br />
{php} require (TEMPLATES_PATH . "admin.admins.search.php");{/php}
<div id="banlist-nav"> 
{$admin_nav}
</div>
<div id="banlist">
<table width="99%" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td width="34%" class="listtable_top">
            <b>
                {t section="admin/admins" message="Name"}
            </b>
        </td>
		<td width="33%" class="listtable_top">
            <b>
                {t section="admin/admins" message="Server Admin Group"}
            </b>
        </td>
		<td width="33%" class="listtable_top">
            <b>
                {t section="admin/admins" message="Web Admin Group"}
            </b>
        </td>
	</tr>
	{foreach from="$admins" item="admin"}
		<tr onmouseout="this.className='tbl_out'" onmouseover="this.className='tbl_hover'" class="tbl_out opener">
			<td class="admin-row" style="padding:3px;">
                {$admin.user} 
                (
                    <a 
                        href="./index.php?m=bans&advSearch={$admin.aid}&advType=admin" 
                        title="{t section="admin/admins" message="Show bans"}">
                    {t 
                        section="admin/admins"
                        message="[[bancount]] bans"
                        bancount=$admin.bancount
                    }
                </a> 
                    | 
                <a 
                    href="./index.php?m=bans&advSearch={$admin.aid}&advType=nodemo" 
                    title="{t section="admin/admins" message="Show bans without demo"}">
                    {t 
                        section="admin/admins" 
                        message="[[nodemocount]] w.d."
                        nodemocount=$admin.nodemocount
                    }
                </a>)
            </td>
			<td class="admin-row" style="padding:3px;">{$admin.server_group}</td>
			<td class="admin-row" style="padding:3px;">{$admin.web_group}</td>
 		</tr>
		<tr>
			<td colspan="3">
				<div class="opener" align="center" border="1">
					<table width="100%" cellspacing="0" cellpadding="3" bgcolor="#eaebeb">
						<tr>
			            	<td align="left" colspan="3" class="front-module-header">
								<b>{t section="admin/admins" message="Admin Details of"} {$admin.user}</b>
							</td>
			          	</tr>
			          	<tr align="left">
				            <td width="35%" class="front-module-line">
                                <b>
                                    {t section="admin/admins" message="Server Admin Permissions"}
                                </b>
                            </td>
				            <td width="35%" class="front-module-line">
                                <b>
                                    {t section="admin/admins" message="Web Admin Permissions"}
                                </b>
                            </td>
				            <td width="30%" valign="top" class="front-module-line">
                                <b>
                                    {t section="admin/admins" message="Action"}
                                </b>
                            </td>
			          	</tr>
			          	<tr align="left">
				            <td valign="top">{$admin.server_flag_string}</td>
				            <td valign="top">{$admin.web_flag_string}</td>
				            <td width="30%" valign="top">
								<div class="ban-edit">
						        	<ul>
						        	{if $permission_editadmin}
							        	<li>
							        		<a href="index.php?p=admin&c=admins&o=editdetails&id={$admin.aid}">
                                                <img src="images/details.png" border="0" alt="" style="vertical-align:middle"/> 
                                                {t section="admin/admins" message="Edit Details"}
                                            </a>
							        	</li>
							        	<li>
							        		<a href="index.php?p=admin&c=admins&o=editpermissions&id={$admin.aid}">
                                                <img src="images/permissions.png" border="0" alt="" style="vertical-align:middle" /> 
                                                {t section="admin/admins" message="Edit Permissions"}
                                            </a>
							        	</li>
							        	<li>
							        		<a href="index.php?p=admin&c=admins&o=editservers&id={$admin.aid}">
                                                <img src="images/server_small.png" border="0" alt="" style="vertical-align:middle" /> 
                                                {t section="admin/admins" message="Edit Server Access"}
                                            </a>
							        	</li>
							        	<li>
							        		<a href="index.php?p=admin&c=admins&o=editgroup&id={$admin.aid}">
                                                <img src="images/groups.png" border="0" alt="" style="vertical-align:middle" /> 
                                                {t section="admin/admins" message="Edit Groups"}
                                            </a>
							        	</li>
						        	{/if}
						        	{if $permission_deleteadmin}
						            	<li>
							        		<a href="#" onclick="RemoveAdmin({$admin.aid}, '{$admin.user}');">
                                                <img src="images/delete.png" border="0" alt="" style="vertical-align:middle" /> 
                                                {t section="admin/admins" message="Delete Admin"}
                                            </a>
							        	</li>
						            {/if}
						          	</ul>
								</div>
							   	<div class="front-module-line" style="padding:3px;">
                                    {t section="admin/admins" message="Immunity Level"}: <b>{$admin.immunity}</b>
                                </div>
							   	<div class="front-module-line" style="padding:3px;">
                                    {t section="admin/admins" message="Last Visited"}: <b><small>{$admin.lastvisit}</small></b>
                                </div>
							   	<div class="front-module-line" style="padding:3px;">
                                    {t section="admin/admins" message="Expired"}: <b><small>{$admin.expDate}</small></b>
                                </div>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
	{/foreach}
</table>
</div>
<script type="text/javascript">
    {literal}
    function RemoveAdmin(id, name)
    {
    {/literal}
        var noPerm = confirm('{t section="admin/admins" message="Are you sure you want to delete admin?"}');
    {literal}
        if(noPerm == false)
        {
            return;
        }
        xajax_RemoveAdmin(id);
    }
    {/literal}
    InitAccordion('tr.opener', 'div.opener', 'mainwrapper');
</script>
{/if}
