	</div></div>
	<div id="footer">
		<div id="gc">
            {t section="layout" message='By <a href="http://www.gameconnect.net" target="_blank" class="footer_link">GameConnect.net</a>'}
        </div>
		<div id="sb">
            <br />
            <a href="http://www.sourcebans.net" target="_blank"><img src="images/sb.png" alt="SourceBans" border="0" /></a>
            <br />
            <div id="footqversion">
                {t section="layout" message="Version [[sbVersion]] [[sbRev]]" sbVersion=$SB_VERSION sbRev=$SB_REV}
            </div>
            <div id="footquote">
                {$SB_QUOTE}
            </div>
		</div>
		<div id="sm">
            {t section="layout" message='Powered by <a class="footer_link" href="http://www.sourcemod.net" target="_blank">SourceMod</a>'}
		</div>
	</div>