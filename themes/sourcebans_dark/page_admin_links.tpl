<div id="admin-page-content">
    <div id="0" style="display:none;">
        <h3>{t section="admin/links" message="Site links (Total: [[links_count]])" links_count=$links_count}</h3>
        <table width="99%" cellspacing="0" cellpadding="0" align="center">
            <tr>
                <td width="35%" class="listtable_top">
                    <b>{t section="admin/links" message="Label"}</b>
                </td>
                <td width="50%" class="listtable_top">
                    <b>{t section="admin/links" message="URL"}</b>
                </td>
                <td width="15%" class="listtable_top" style="text-align: center">
                    <b>{t section="admin/links" message="Order"}</b>
                </td>
            </tr>
            {foreach from=$links item='link'}
            <tr onmouseout="this.className='tbl_out'" onmouseover="this.className='tbl_hover'" class="tbl_out opener">
                <td class="admin-row" style="padding:3px;">
                    {$link.label}
                </td>
                <td class="admin-row" style="padding:3px;">
                    {$link.url}
                </td>
                <td class="admin-row" style="padding:3px; text-align: center">
                    {if $link.position != $firstLast.min}
                    <a href="#" onclick="xajax_SortLink({$link.id}, 'up');" style="font-size: 16px; font-weight: bold">⇑</a>
                    {/if}
                    {if $link.position != $firstLast.max}
                    <a href="#" onclick="xajax_SortLink({$link.id}, 'down');" style="font-size: 16px; font-weight: bold">⇓</a>
                    {/if}
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="opener" align="center" border="1">
                        <table width="100%" cellspacing="0" cellpadding="3" bgcolor="#eaebeb">
                            <tr>
                                <td align="left" colspan="3" class="front-module-header">
                                    <b>{t section="admin/links" message="Link [[link_label]]" link_label=$link.label}</b>
                                </td>
                            </tr>
                            <tr align="left">
                                <td width="70%" class="front-module-line">
                                    <b>{t section="admin/links" message="Link info"}</b>
                                </td>
                                <td width="30%" valign="top" class="front-module-line">
                                    <b>{t section="admin/links" message="Actions"}</b>
                                </td>
                            </tr>
                            <tr align="left">
                                <td valign="top">
                                    <table width="100%">
                                        <tr>
                                            <td class="listtable_1">
                                                {t section="admin/links" message="Label"}
                                            </td>
                                            <td class="listtable_1">
                                                {$link.label}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="listtable_1">
                                                {t section="admin/links" message="Title"}
                                            </td>
                                            <td class="listtable_1">
                                                {$link.title}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="listtable_1">
                                                {t section="admin/links" message="URL"}
                                            </td>
                                            <td class="listtable_1">
                                                {$link.url}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="listtable_1">
                                                {t section="admin/links" message="Active"}
                                            </td>
                                            <td class="listtable_1">
                                                {$link.active}
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="30%" valign="top">
                                    <div class="ban-edit">
                                        <ul>
                                            <li>
                                                <a href="#" onclick="updateLink({$link.id});"><img src="images/details.png" border="0" alt="" style="vertical-align:middle"/>{t section="admin/links" message="Edit"}</a>
                                            </li>
                                            <li>
                                                <a href="#" onClick="deleteLink({$link.id});"><img src="images/delete.png" border="0" alt="" style="vertical-align:middle" />{t section="admin/links" message="Delete"}</a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            {/foreach}
        </table>
    </div>

    <div id="1" style="display:none;">
        <div id="add-group">
            <h3 id="link_form_title">{t section="admin/links" message="Create link"}</h3>
            <div class="badentry">
                {t section="admin/links" message='If you are using a multilingual site, you need to create files in the file transfer /languages/mainmenu.php'}
            </div>
            {t section="admin/links" message="For more information or help regarding a certain subject move your mouse over the question mark."}
            <br />
            <br />
            <table width="90%" border="0" style="border-collapse:collapse;" id="group.details" cellpadding="3">
                <tr>
                    <td valign="top" width="35%">
                        <div class="rowdesc">
                            {help_icon
                                title="Link label"
                                message="Link label"
                                translate="admin/links"
                            }
                            {t section="admin/links" message="Link label"}
                        </div>
                    </td>
                    <td>
                        <div align="left">
                            <input type="text" class="submit-fields" name="link_label" id="link_label" style="width: 400px" />
                        </div>
                        <div id="link_label.msg" class="badentry"></div>
                    </td>
                </tr>
                <tr>
                    <td valign="top" width="35%">
                        <div class="rowdesc">
                            {help_icon
                                title="Link URL"
                                message="The address to which the link will lead"
                                translate="admin/links"
                            }
                            {t section="admin/links" message="Link URL"}
                        </div>
                    </td>
                    <td>
                        <div align="left">
                            <input type="text" class="submit-fields" name="link_url" id="link_url" style="width: 400px" />
                        </div>
                        <div id="link_url.msg" class="badentry"></div>
                    </td>
                </tr>
                <tr>
                    <td valign="top" width="35%">
                        <div class="rowdesc">
                            {help_icon
                                title="Link title"
                                message="This description will be shown when you hover over the link."
                                translate="admin/links"
                            }
                            {t section="admin/links" message="Link title"}
                        </div>
                    </td>
                    <td>
                        <div align="left">
                            <input type="text" class="submit-fields" name="link_title" id="link_title" style="width: 400px" />
                        </div>
                        <div id="link_title.msg" class="badentry"></div>
                    </td>
                </tr>
                <tr>
                    <td valign="top" width="35%">
                        <div class="rowdesc">
                            {help_icon
                                title="Link active"
                                message="If checked, the link will be displayed in the main menu."
                                translate="admin/links"
                            }
                            {t section="admin/links" message="Link active"}
                        </div>
                    </td>
                    <td>
                        <div align="left">
                            <input type="checkbox" id="link_active" name="link_active" /> 
                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="top" width="35%">
                        <div class="rowdesc">
                            {help_icon
                                title="Hide for guests"
                                message="Check the box next to the link is visible only to authorized users"
                                translate="admin/links"
                            }
                            {t section="admin/links" message="Hide for guests"}
                        </div>
                    </td>
                    <td>
                        <div align="left">
                            <input type="checkbox" id="link_hideGuests" name="link_hideGuests" /> 
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        {sb_button text="Save" class="ok" onclick="SaveLink();" submit=false translate="admin/links"}
                          &nbsp;
                        {sb_button text="Cancel" onclick="document.location.href='index.php?p=admin&c=links'" class="cancel" id="aback" translate="admin/links"}
                        <input type="hidden" name="link_id" id="link_id" value="0">
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    InitAccordion('tr.opener', 'div.opener', 'mainwrapper');
    {literal}
    function SaveLink() {
        var linkLabel = $('link_label').value,
            linkUrl = $('link_url').value,
            linkTitle = $('link_title').value,
            linkActive = $('link_active').checked,
            hideGuests = $('link_hideGuests').checked,
            linkId = $('link_id').value,
            errors = 0;
        if(!linkLabel) {
            errors++;
            $('link_label.msg').setHTML(t('Please type link label'));
            $('link_label.msg').setStyle('display', 'block');
        } else {
            $('link_label.msg').setHTML('');
            $('link_label.msg').setStyle('display', 'none');
        }
        if(!linkUrl) {
            errors++;
            $('link_url.msg').setHTML(t('Please type link URL'));
            $('link_url.msg').setStyle('display', 'block');
        } else {
            $('link_url.msg').setHTML('');
            $('link_url.msg').setStyle('display', 'none');
        }
        if(!linkTitle) {
            errors++;
            $('link_title.msg').setHTML(t('Please type link title'));
            $('link_title.msg').setStyle('display', 'block');
        } else {
            $('link_title.msg').setHTML('');
            $('link_title.msg').setStyle('display', 'none');
        }
        
        if(errors === 0) {
            var xaj = xajax_SaveLink(linkLabel, linkUrl, linkTitle, linkActive, linkId, hideGuests);
            console.log(xaj);
        }
        return false;
    }
    function deleteLink(id) {
        if(confirm(t('Are you sure you want to delete the link?'))) {
            xajax_DeleteLink(id);
        }
        return false;
    }
    {/literal}
</script>