{if NOT $permission_protests}
	Access Denied!
{else}
	<div id="protests">
		<h3 style="margin-top:0px;">
            {t section="admin/bans" message="Ban Protests"} (<span id="protcount">{$protest_count}</span>)
        </h3>
		{t section="admin/bans" message="Click a player's nickname to view information about their ban"}
        <br />
        <br />
        <div id="banlist-nav"> 
            {$protest_nav}
        </div>
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
            	<td width="40%" height="16" class="listtable_top">
                    <strong>{t section="admin/bans" message="Nickname"}</strong>
                </td>
      			<td width="20%" height="16" class="listtable_top">
                    <strong>{t section="admin/bans" message="SteamID"}</strong>
                </td>
            	<td width="25%" height="16" class="listtable_top">
                    <strong>{t section="admin/bans" message="Action"}</strong>
                </td>
			</tr>
			{foreach from="$protest_list" item="protest"}
			<tr id="pid_{$protest.pid}" class="opener2 tbl_out" onmouseout="this.className='tbl_out'" onmouseover="this.className='tbl_hover'">
                <td class="toggler" style="border-bottom: solid 1px #ccc" height="16">
                    <a href="./index.php?m=bans&advSearch={$protest.authid}&advType=steamid" title="{t section="admin/bans" message="Show ban"}">
                        {$protest.name}
                    </a>
                </td>
                <td style="border-bottom: solid 1px #ccc" height="16">
                    {if $protest.authid!=""}
                        {$protest.authid}
                    {else}
                        {$protest.ip}
                    {/if}
                </td>
                <td style="border-bottom: solid 1px #ccc" height="16">
                    {if $permission_editban}
                    <a href="#" onclick="RemoveProtest('{$protest.pid}', '{if $protest.authid!=""}{$protest.authid}{else}{$protest.ip}{/if}', '1');">
                        {t section="admin/bans" message="Remove"}
                    </a> -
                    {/if}
                    <a href="index.php?p=admin&c=bans&o=email&type=p&id={$protest.pid}">
                        {t section="admin/bans" message="Contact"}
                    </a>
                </td>
            </tr>
            <tr id="pid_{$protest.pid}a" >
                <td colspan="3" align="center" id="ban_details_{$protest.pid}">
                    <div class="opener2">
                        <table width="90%" cellspacing="0" cellpadding="0" class="listtable">
                            <tr>
                                <td height="16" align="left" class="listtable_top" colspan="3">
                                    {t section="admin/bans" message="<b>Bandetails</b>"}
                                </td>
                            </tr>
                            <tr align="left">
                                <td width="20%" height="16" class="listtable_1">
                                    {t section="admin/bans" message="Player"}
                                </td>
                                <td height="16" class="listtable_1">{$protest.name}</td>
                                <td width="30%" rowspan="11" class="listtable_2">
                                    <div class="ban-edit">
                                        <ul>
                                          <li>{$protest.protaddcomment}</li>	
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            <tr align="left">
                                <td width="20%" height="16" class="listtable_1">
                                    {t section="admin/bans" message="SteamID"}
                                </td>
                                <td height="16" class="listtable_1">
                                {if $protest.authid == ""}
                                    {t section="admin/bans" message='<i><font color="#677882">no steamid present</font></i>'}
                                {else}
                                    {$protest.authid}
                                {/if}
                                </td>
                            </tr>
                            <tr align="left">
                                <td width="20%" height="16" class="listtable_1">
                                    {t section="admin/bans" message="IP address"}
                                </td>
                                <td height="16" class="listtable_1">
                                    {if $protest.ip == 'none' || $protest.ip == ''}
                                        {t section="admin/bans" message='<i><font color="#677882">no IP address present</font></i>'}
                                    {else}
                                        {$protest.ip}
                                    {/if}
                                </td>
                            </tr>
                            <tr align="left">
                                <td width="20%" height="16" class="listtable_1">
                                    {t section="admin/bans" message="Invoked on"}
                                </td>
                                <td height="16" class="listtable_1">
                                    {$protest.date}
                                </td>
                            </tr>
                            <tr align="left">
                                <td width="20%" height="16" class="listtable_1">
                                    {t section="admin/bans" message="End Date"}
                                </td>
                                <td height="16" class="listtable_1">
                                    {if $protest.ends == 'never'}
                                        {t section="admin/bans" message='<i><font color="#677882">Not applicable.</font></i>'}
                                    {else}
                                        {$protest.ends}
                                    {/if}
                                </td>
                            </tr>
                            <tr align="left">
                                <td width="20%" height="16" class="listtable_1">
                                    {t section="admin/bans" message="Reason"}
                                </td>
                                <td height="16" class="listtable_1">
                                    {$protest.ban_reason}
                                </td>
                            </tr>
                            <tr align="left">
                                <td width="20%" height="16" class="listtable_1">
                                    {t section="admin/bans" message="Banned by Admin"}
                                </td>
                                <td height="16" class="listtable_1">
                                    {$protest.admin}
                                </td>
                            </tr>
                            <tr align="left">
                                <td width="20%" height="16" class="listtable_1">
                                    {t section="admin/bans" message="Banned from"}
                                </td>
                                <td height="16" class="listtable_1">
                                    {$protest.server}
                                </td>
                            </tr>
                            <tr align="left">
                                <td width="20%" height="16" class="listtable_1">
                                    {t section="admin/bans" message="Protester IP"}
                                </td>
                                <td height="16" class="listtable_1">
                                    {$protest.pip}
                                </td>
                            </tr>
                            <tr align="left">
                                <td width="20%" height="16" class="listtable_1">
                                    {t section="admin/bans" message="Protested on"}
                                </td>
                                <td height="16" class="listtable_1">
                                    {$protest.datesubmitted}
                                </td>
                            </tr>
                            <tr align="left">
                                <td width="20%" height="16" class="listtable_1">
                                    {t section="admin/bans" message="Protest message"}
                                </td>
                                <td height="16" class="listtable_1">
                                    {$protest.reason}
                                </td>
                            </tr>
                            <tr align="left">
                                <td width="20%" height="16" class="listtable_1">
                                    {t section="admin/bans" message="Comments"}
                                </td>
                                <td height="60" class="listtable_1" colspan="3">
                                    {if $protest.commentscount}
                                    <table width="100%" border="0">
                                    {foreach from=$protest.commentdata item=commenta}
                                        {if $commenta.morecom}
                                        <tr>
                                            <td colspan="3">
                                                <hr />
                                            </td>
                                        </tr>
                                        {/if}
                                        <tr>
                                            <td>
                                                {if !empty($commenta.comname)}
                                                    <b>{$commenta.comname|escape:'html'}</b>
                                                {else}
                                                    {t section="admin/bans" message='<i><font color="#677882">Admin deleted</font></i>'}
                                                {/if}
                                            </td>
                                            <td align="right">
                                                <b>{$commenta.added}</b>
                                            </td>
                                            {if $commenta.editcomlink != ""}
                                            <td align="right">
                                              {$commenta.editcomlink} {$commenta.delcomlink}
                                            </td>
                                            {/if}
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="word-break: break-all;word-wrap: break-word;">
                                              {$commenta.commenttxt}
                                            </td>
                                        </tr>
                                        {if !empty($commenta.edittime)}
                                        <tr>
                                            <td colspan="3">
                                                <span style="font-size:6pt;color:grey;">
                                                    {t section="admin/bans" message="last edit"} 
                                                    {$commenta.edittime} 
                                                    {t section="admin/bans" message="by"} 
                                                    {if !empty($commenta.editname)}
                                                        {$commenta.editname}
                                                    {else}
                                                        {t section="admin/bans" message='<i><font color="#677882">Admin deleted</font></i>'}
                                                    {/if}
                                                </span>
                                            </td>
                                        </tr>
                                        {/if}
                                    {/foreach}
                                    </table>
                                    {else}
                                          {$protest.commentdata}
                                    {/if}
                                </td>
                            </tr>
          				</table>
          			</div>
				</td>
			</tr>
			{/foreach}
		</table>
	</div>
	<script>
        InitAccordion('tr.opener2', 'div.opener2', 'protests');
        {literal}
        function RemoveProtest(id, name, archiv)
        {
            var noPerm;
            if(archiv == '2') {
        {/literal}
                noPerm = '{t section="admin/bans" message="Are you sure you want to restore the ban protest from the archive?"}';
        {literal}
            } else if(archiv == '1') {
        {/literal}
                noPerm = '{t section="admin/bans" message="Are you sure you want to move the ban protest to the archive?"}';
        {literal}
            } else {
        {/literal}
                noPerm = '{t section="admin/bans" message="Are you sure you want to delete the ban protest?"}';
        {literal}
            }
            if(!confirm(noPerm)) {
                return false;
            }
            xajax_RemoveProtest(id, archiv);
        }
        {/literal}
    </script>
{/if}
