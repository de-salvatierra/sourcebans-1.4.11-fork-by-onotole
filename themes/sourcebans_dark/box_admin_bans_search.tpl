<div align="center">
	<table width="80%" cellpadding="0" class="listtable" cellspacing="0">
		<tr class="sea_open">
			<td width="2%" height="16" class="listtable_top" colspan="3">
                {t section="admin/bans" message="<b>Advanced Search</b> (Click)"}
            </td>
	  	</tr>
	  	<tr>
	  		<td>
	  		<div class="panel">
	  			<table width="100%" cellpadding="0" class="listtable" cellspacing="0">
			    <tr>
					<td class="listtable_1" width="8%" align="center">
                        <input id="name" name="search_type" type="radio" value="name">
                    </td>
			        <td class="listtable_1" width="26%">
                        {t section="admin/bans" message="Nickname"}
                    </td>
			        <td class="listtable_1" width="66%">
                        <input 
                            type="text" 
                            id="nick" 
                            value="" 
                            onmouseup="$('name').checked = true" 
                            style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 250px;">
                    </td>
				</tr>       
			    <tr>
			        <td align="center" class="listtable_1" >
                        <input id="steam_" type="radio" name="search_type" value="radiobutton">
                    </td>
			        <td class="listtable_1" >
                        {t section="admin/bans" message="SteamID"}
                    </td>
			        <td class="listtable_1" >
			            <input 
                            type="text" 
                            id="steamid" 
                            value="" 
                            onmouseup="$('steam_').checked = true"
                            style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 150px;">
                        <select 
                            id="steam_match" 
                            onmouseup="$('steam_').checked = true" 
                            style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 100px;">
                            <option value="0" selected>{t section="admin/bans" message="Exact Match"}</option>
                            <option value="1">{t section="admin/bans" message="Partial Match"}</option>
				    </select>
				</td>
			    </tr>
				{if !$hideplayerips}
				<tr>
					<td align="center" class="listtable_1" >
                        <input id="ip_" type="radio" name="search_type" value="radiobutton">
                    </td>
					<td class="listtable_1" >
                        {t section="admin/bans" message="IP Address"}
                    </td>
					<td class="listtable_1" >
                        <input 
                            type="text" 
                            id="ip" 
                            value="" 
                            onmouseup="$('ip_').checked = true"
                            style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 250px;"></td>
				</tr>
				{/if}
			    <tr>
			        <td align="center" class="listtable_1" >
                        <input id="reason_" type="radio" name="search_type" value="radiobutton">
                    </td>
			        <td class="listtable_1" >
                        {t section="admin/bans" message="Reason"}
                    </td>
			        <td class="listtable_1" >
                        <input 
                            type="text" 
                            id="ban_reason" 
                            value="" 
                            onmouseup="$('reason_').checked = true" 
                            style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 250px;">
                    </td>
			    </tr>
				<tr>
					<td align="center" class="listtable_1" >
                        <input id="date" type="radio" name="search_type" value="radiobutton">
                    </td>
			        <td class="listtable_1" >
                        {t section="admin/bans" message="Date"}
                    </td>
			        <td class="listtable_1" >
			        	<input type="text" id="day" value="DD" onmouseup="$('date').checked = true" style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 75px;">
			            <input type="text" id="month" value="MM" onmouseup="$('date').checked = true" style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 75px;">
			            <input type="text" id="year" value="YY" onmouseup="$('date').checked = true" style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 75px;"> 
			        </td>
			  	</tr>
				<tr>
			        <td align="center" class="listtable_1" >
                        <input id="length_" type="radio" name="search_type" value="radiobutton">
                    </td>
			        <td class="listtable_1" >
                        {t section="admin/bans" message="Length"}
                    </td>
			        <td class="listtable_1" >
						<table border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td>
						            <select id="length_type" onmouseup="$('length_').checked = true" style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 40px;">
										<option value="e" title="{t section="admin/bans" message="equal to"}">=</option>
										<option value="h" title="{t section="admin/bans" message="greater"}">&gt;</option>
										<option value="l" title="{t section="admin/bans" message="smaller"}">&lt;</option>
										<option value="eh" title="{t section="admin/bans" message="equal to or greater"}">&gt;=</option>
										<option value="el" title="{t section="admin/bans" message="equal to or smaller"}">&lt;=</option>
									</select>
								</td>
								<td>
									<input type="text" id="other_length" name="other_length" onmouseup="$('length_').checked = true" style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 190px;display:none;">
								</td>
								<td>
									<select id="length" onmouseup="$('length_').checked = true" onchange="switch_length(this);" onmouseover="if(this.options[this.selectedIndex].value=='other')$('length').setStyle('width', '210px');if(this.options[this.selectedIndex].value=='other')this.focus();" onblur="if(this.options[this.selectedIndex].value=='other')$('length').setStyle('width', '20px');" style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 210px;">
								        {$bantimes}
										<option value="other">{t section="admin/bans" message="smaller"}</option>
									</select>
								</td>
							</tr>
						</table>
					</td>
			    </tr>
				<tr>
			        <td align="center" class="listtable_1" ><input id="ban_type_" type="radio" name="search_type" value="radiobutton"></td>
			        <td class="listtable_1" >
                        {t section="admin/bans" message="Ban Type"}
                    </td>
			        <td class="listtable_1" >
			            <select 
                            id="ban_type" 
                            onmouseup="$('ban_type_').checked = true" 
                            style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 250px;">
					        <option value="0" selected>{t section="admin/bans" message="Steam ID"}</option>
					        <option value="1">{t section="admin/bans" message="IP Address"}</option>
						</select>
					</td>
			    </tr>
                {if !$hideadminname}
			    <tr>
			    	<td class="listtable_1"  align="center">
                        <input id="admin" name="search_type" type="radio" value="radiobutton">
                    </td>
			        <td class="listtable_1" >
                        {t section="admin/bans" message="Admin"}
                    </td>
			        <td class="listtable_1" >
						<select id="ban_admin" onmouseup="$('admin').checked = true" style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 250px;">
							{foreach from=$admin_list item="admin"}
								<option label="{$admin.user}" value="{$admin.aid}">{$admin.user}</option>
							{/foreach}
						</select>           
					</td> 
				</tr>
                {/if}
			    <tr>
			    	<td class="listtable_1"  align="center">
                        <input id="where_banned" name="search_type" type="radio" value="radiobutton">
                    </td>
					<td class="listtable_1" >
                        {t section="admin/bans" message="Server"}
                    </td>
			        <td class="listtable_1" >
						<select id="server" onmouseup="$('where_banned').checked = true" style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 250px;">
						<option label="Web Ban" value="0">Web Ban</option>
							{foreach from=$server_list item="server"}
								<option value="{$server.sid}" id="ss{$server.sid}">
                                    {t section="admin/bans" message="Retrieving Hostname..."} ({$server.ip}:{$server.port})
                                </option>
							{/foreach}
						</select>            
					</td>
			    </tr>
				{if $is_admin}
				<tr>
			        <td align="center" class="listtable_1" >
                        <input id="comment_" type="radio" name="search_type" value="radiobutton">
                    </td>
			        <td class="listtable_1" >
                        {t section="admin/bans" message="Comment"}
                    </td>
			        <td class="listtable_1" >
                        <input 
                            type="text" 
                            id="ban_comment" 
                            value="" 
                            onmouseup="$('comment_').checked = true" 
                            style="border: 1px solid #000000; font-size: 12px; background-color: rgb(215, 215, 215);width: 250px;">
                    </td>
			    </tr>
				{/if}
			    <tr>
				    <td> </td>
				    <td> </td>
			        <td>
                        {sb_button 
                            text="Search"
                            onclick="search_bans();"
                            class="ok"
                            id="searchbtn"
                            submit=false,
                            translate="admin/bans"
                        }
                    </td>
			    </tr>
			   </table>
			   </div>
		  </td>
		</tr>
	</table>
</div>
{$server_script}
<script>
    InitAccordion('tr.sea_open', 'div.panel', 'mainwrapper');
</script>
