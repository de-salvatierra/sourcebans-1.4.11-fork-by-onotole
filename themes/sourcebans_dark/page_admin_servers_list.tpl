{if NOT $permission_list}
	{t section="admin/servers" message="Access Denied!"}
{else}
	<h3>{t section="admin/servers" message='Servers (<span id="srvcount">[[server_count]]</span>)' server_count=$server_count}</h3>
	
	{if $permission_config}
        {t section="admin/servers" message='To view the database config file you need to upload to your game server, click <a href="index.php?p=admin&c=servers&o=dbsetup">here</a>.'}
        <br />
        <br />
	{/if}
	<table width="100%" cellpadding="1">
	<tr>
		<td class="front-module-header" width="3%" height="16">
            <strong>{t section="admin/servers" message="ID"}</strong>
        </td>
		<td class="front-module-header" width="40%" height="16">
            <strong>{t section="admin/servers" message="Hostname"}</strong>
        </td>
		<td class="front-module-header" height="16">
            <strong>{t section="admin/servers" message="Players"}</strong>
        </td>
		<td class="front-module-header" height="16">
            <strong>{t section="admin/servers" message="Mod"}</strong>
        </td>
		<td class="front-module-header" height="16">
            <strong>{t section="admin/servers" message="Action"}</strong>
        </td>
	</tr>
	{foreach from=$server_list item="server"}
	<tr id="sid_{$server.sid}" {if $server.enabled==0}style="background-color:#eaeaea" title="Disabled"{/if}>
		<td style="border-bottom: solid 1px #ccc" height="16">
            {$server.sid}
        </td>
		<td style="border-bottom: solid 1px #ccc" height="16" id="host_{$server.sid}">
            <i>{t section="admin/servers" message="Querying Server Data..."}</i>
        </td>
		<td style="border-bottom: solid 1px #ccc; text-align: center" height="16" id="players_{$server.sid}">
            {t section="admin/servers" message="N/A"}
        </td>
		<td style="border-bottom: solid 1px #ccc; text-align: center" height="16">
            <img src="images/games/{$server.icon}">
        </td>
		<td style="border-bottom: solid 1px #ccc; text-align: center" height="16">
            {if $server.rcon_access}
                <a href="index.php?p=admin&c=servers&o=rcon&id={$server.sid}">
                    {t section="admin/servers" message="RCON"}
                </a> - 
            {/if}

            <a href="index.php?p=admin&c=servers&o=admincheck&id={$server.sid}">
                {t section="admin/servers" message="Admins"}
            </a>

            {if $permission_editserver}
                - <a href="index.php?p=admin&c=servers&o=edit&id={$server.sid}">
                    {t section="admin/servers" message="Edit"}
                </a>
            {/if}

            {if $pemission_delserver}
                - <a href="#" onclick="RemoveServer({$server.sid}, '{$server.ip}:{$server.port}');">
                    {t section="admin/servers" message="Delete"}
                </a>
            {/if}
	 	</td>
	</tr>
	
	{/foreach}
</table>
{if $permission_addserver}
<br />
<div class="rowdesc">
	{help_icon
        title="Upload Map Image"
        message="Click here to upload a screenshot of a map. Use the mapname as filename."
        translate="admin/servers"
    }
    {t section="admin/servers" message="Upload Map Image"}
	{sb_button
        text="Upload"
        onclick="childWindow=open('pages/admin.uploadmapimg.php','upload','resizable=yes,width=300,height=160');"
        class="save"
        id="upload"
        translate="admin/servers"
    }
</div>
<div id="mapimg.msg" style="color:#CC0000;"></div>
{/if}
<script type="text/javascript">
{foreach from=$server_list item="server"}
    xajax_ServerHostPlayers({$server.sid});
{/foreach}
    {literal}
    function RemoveServer(id, name)
    {
    {/literal}
        var noPerm = '{t section="admin/servers" message="Are you sure you want to delete the server?"}';
    {literal}
        if(!confirm(noPerm)) {
            return;
        }
        xajax_RemoveServer(id);
    }
    {/literal}
</script>
{/if}
