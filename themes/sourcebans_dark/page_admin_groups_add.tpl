<div id="1" style="display:none;">
    {if NOT $permission_addgroup}
        {t section="admin/groups" message="Access Denied!"}
    {else}
        <div id="add-group">
            <h3>{t section="admin/groups" message="New Group"}</h3>
            <table width="90%" style="border-collapse:collapse;" id="group.details" cellpadding="3">
                <tr>
                    <td valign="top" width="35%">
                        <div class="rowdesc">
                            {help_icon title="Group Name" message="Type the name of the new group you want to create." translate="admin/groups"}
                            {t section="admin/groups" message="Group Name"}
                        </div>
                    </td>
                    <td>
                        <div align="left">
                            <input type="text" TABINDEX=1 class="submit-fields" id="groupname" name="groupname" />
                        </div>
                        <div id="name.msg" class="badentry"></div>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <div class="rowdesc">
                            {help_icon
                                title="Group Type"
                                message="This defines the type of group you are about to create. This helps identify and catagorize the groups list."
                                translate="admin/groups"
                            }
                            {t section="admin/groups" message="Group Type"}
                        </div>
                    </td>
                    <td>
                        <div align="left">
                            <select
                                onchange="UpdateGroupPermissionCheckBoxes()"
                                TABINDEX=2
                                class="submit-fields"
                                name="grouptype"
                                id="grouptype">
                                <option value="0">{t section="admin/groups" message="Please Select..."}</option>
                                <option value="1">{t section="admin/groups" message="Web Admin Group"}</option>
                                <option value="2">{t section="admin/groups" message="Server Admin Group"}</option>
                                <option value="3">{t section="admin/groups" message="Server Group"}</option>
                            </select>
                        </div>
                        <div id="type.msg" class="badentry"></div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" id="perms" valign="top" style="height:5px;overflow:hidden;"></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>      
                        {sb_button
                            text="Save Changes"
                            onclick="ProcessGroup();"
                            class="ok"
                            id="agroup"
                            submit=false
                            translate="admin/groups"
                        }
                             &nbsp;
                        {sb_button
                            text="Back"
                            onclick="history.go(-1)"
                            class="cancel"
                            id="back"
                            submit=false
                            translate="admin/groups"
                        } 
                    </td>
                </tr>
            </table>
        </div>
        <script>
            {literal}
            function ProcessGroup()
            {
            {/literal}
                var Mask = BoxToMask();
                var Smask = BoxToSrvMask();
                xajax_AddGroup(document.getElementById('groupname').value, document.getElementById('grouptype').value, Mask, Smask);
            {literal}
            }
            function UpdateGroupPermissionCheckBoxes()
            {
                $('perms').setHTML('');
                var grouptype = document.getElementById('grouptype').value, height;
                if(grouptype != 3 && grouptype != 0) {
            {/literal}
                    $('type.msg').setHTML('{t section="admin/groups" message="Please wait..."}');
                    $('type.msg').setStyle('display', 'block');
            {literal}
                }
                if(grouptype == 1) {
                    height = 285;
                } else if(grouptype == 2) {
                    height = 435;
                } else {
                    $('type.msg').setStyle('display', 'none');
                    height = 2;
                }
                Shrink('perms', 1000, height);
                if(grouptype != 3 && grouptype != 0) {
                    setTimeout("xajax_UpdateGroupPermissions(document.getElementById('grouptype').value)",1000);
                }
            }
            {/literal}
        </script>
    {/if}
</div>