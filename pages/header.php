<?php 
/**
 * =============================================================================
 * Page header
 * 
 * @author SteamFriends Development Team
 * @version 1.0.0
 * @copyright SourceBans (C)2007 SteamFriends.com.  All rights reserved.
 * @package SourceBans
 * @link http://www.sourcebans.net
 * 
 * @version $Id: header.php 190 2008-12-30 02:06:27Z peace-maker $
 * =============================================================================
 */

global $userbank, $theme, $xajax,$user,$start, $language;
$time = microtime();
$time = explode(" ", $time);
$time = $time[1] + $time[0];
$start = $time;

if(!defined("IN_SB")) {
	echo "You should not be here. Only follow links!";
	die();
}

$currentLanguage = $language->getLanguage();

if($userbank->is_logged_in()) {
    $username = $userbank->GetProperty("user");
} else {
    $username = t('login', 'Guest');
}

$rehashJs = "function ShowRehashBox(servers, title, msg, color, redir)
{
	// Don't show anything sm_rehash related, if there are no servers to rcon.
	if(servers == '') {
		ShowBox(title, msg, color, redir, true);
		return;
	}
	msg = msg + '<br /><hr /><i>".t('home', 'Rehashing Admin and Group data on all related servers...')."</i><div id=\"rehashDiv\" name=\"rehashDiv\" width=\"100%\"></div>';
	ShowBox(title, msg, color, redir, true);
	$('dialog-control').setStyle('display', 'none');
	xajax_RehashAdmins(servers);
}
function RemoveComment(cid, type, page)
{
	var checkUp = confirm('".t('home', 'Are you sure you want to delete the comment?')."');
	if(checkUp == false)
		return;
	xajax_RemoveComment(cid, type, page);
}
function OpenMessageBox(sid, name, popup)
{
	if(popup==1) {
		ShowBox(
            '".t('servers', 'Send Message')."',
            '".t('servers', 'Please type the message you want to send to selected player<br />You need to have basechat.smx enabled as we use <i>&lt;sm_psay&gt;</i>.')."'
             +'<br><textarea rows=\"3\" cols=\"40\" name=\"ingamemsg\" id=\"ingamemsg\" style=\"overflow:auto;\"></textarea><br><div id=\"ingamemsg.msg\" class=\"badentry\"></div>',
            'blue',
            '',
            true
        );
		$('dialog-control').setHTML('<input type=\"button\" name=\"ingmsg\" class=\"btn ok\" onmouseover=\"ButtonOver(\'ingmsg\')\" onmouseout=\"ButtonOver(\'ingmsg\')\" id=\"ingmsg\" value=\"".t('servers', 'Send Message')."\" />&nbsp;<input type=\"button\" onclick=\"closeMsg(\'\');\" name=\"astop\" class=\"btn cancel\" onmouseover=\"ButtonOver(\'astop\')\" onmouseout=\"ButtonOver(\'astop\')\" id=\"astop\" value=\"".t('servers', 'Cancel')."\" />');
		$('ingmsg').addEvent('click', function(){OpenMessageBox(sid, name, 0);});
	} else if(popup==0) {
		var message = $('ingamemsg').value;
		if(message == \"\") {
			$('ingamemsg.msg').setHTML('".t('servers', 'Please type your message.')."');
			$('ingamemsg.msg').setStyle('display', 'block');
			return;
		} else {
			$('ingamemsg.msg').setHTML('');
			$('ingamemsg.msg').setStyle('display', 'none');
		}
		$('dialog-control').setStyle('display', 'none');
		$('ingamemsg').readOnly = true;
		xajax_SendMessage(sid, name, message);
	}
}";

$theme->assign('rehashJs', $rehashJs);
$theme->assign('languages', $language->getLanguages());
$theme->assign('currentLang', $currentLanguage);
$theme->assign('canUserChangeLanguage', $language->canUserChange());
$theme->assign('xajax_functions',  $xajax->printJavascript("scripts", "xajax.js"));
$theme->assign('header_title', !empty($sbViewParams['pageTitle']) ? RemoveCode($sbViewParams['pageTitle']) : false);
$theme->assign('header_logo', $GLOBALS['config']['template.logo']);
$theme->assign('username', $username);
$theme->assign('logged_in', $userbank->is_logged_in());
$theme->assign('theme_name', isset($GLOBALS['config']['config.theme'])?$GLOBALS['config']['config.theme']:'default');
$theme->display('page_header.tpl');
