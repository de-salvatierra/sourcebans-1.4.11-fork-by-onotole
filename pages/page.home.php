<?php

/**
 * =============================================================================
 * Dashboard
 * 
 * @author SteamFriends Development Team
 * @version 1.0.0
 * @copyright SourceBans (C)2007 SteamFriends.com.  All rights reserved.
 * @package SourceBans
 * @link http://www.sourcebans.net
 * 
 * @version $Id: page.home.php 278 2009-07-07 11:42:36Z tsunami $
 * =============================================================================
 */
global $theme;
if (!defined("IN_SB")) {
    echo "You should not be here. Only follow links!";
    die();
}

$bansInfo = CModule::bansBansForDashboard();
if($bansInfo === null) {
    $bansInfo = array(
        'players_blocked' => array(),
        'total_blocked' => 0,
        'players_banned' => array(),
        'total_bans' => 0
    );
}

$servers = CModule::serversForDashboard();
if($servers === null) {
    $servers = array();
}

$sbViewParams['pageTitle'] = $sbViewParams['title'] = t('home', 'Home');


$theme->assign('dashboard_lognopopup', (isset($GLOBALS['config']['dash.lognopopup']) && $GLOBALS['config']['dash.lognopopup'] == "1"));
$theme->assign('dashboard_title', stripslashes($GLOBALS['config']['dash.intro.title']));
$theme->assign('dashboard_text', stripslashes($GLOBALS['config']['dash.intro.text']));
$theme->assign('players_blocked', $bansInfo['players_blocked']);
$theme->assign('total_blocked', $bansInfo['total_blocked']);

$theme->assign('players_banned', $bansInfo['players_banned']);
$theme->assign('total_bans', $bansInfo['total_bans']);

$theme->assign('server_list', $servers);

$theme->display('page_dashboard.tpl');
