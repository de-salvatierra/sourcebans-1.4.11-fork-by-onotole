<?php
/**
 * =============================================================================
 * Our main handler for admin requests
 * 
 * @author SteamFriends Development Team
 * @version 1.0.0
 * @copyright SourceBans (C)2007 SteamFriends.com.  All rights reserved.
 * @package SourceBans
 * @link http://www.sourcebans.net
 * 
 * @version $Id: admin.php 210 2009-02-06 20:00:15Z tsunami $
 * =============================================================================
 */

global $userbank, $theme;

if(!$userbank->is_admin()) {
    echo t('admin', 'You dont have admin. Be gone!');
    RedirectJS('index.php?p=login');
    PageDie();
}

// BreadCrumbs
if(isset($_GET['c'])) {
    $sbViewParams['breadCrumbs'][] = array(
        'url' => 'index.php?p=admin',
        'label' => t('breadcrumbs', 'Admin')
    );
} else {
    $sbViewParams['breadCrumbs'][] = "<b>" . t('breadcrumbs', 'Admin') . "</b>";
}

if(!isset($_GET['c'])) {
    
    $sbViewParams['pageTitle'] = t('admin', 'Administration');
    $sbViewParams['title'] = t('admin', 'Administration');
    
	$counts = $GLOBALS['db']->GetRow("SELECT 
								 (SELECT COUNT(bid) FROM `" . DB_PREFIX . "_banlog`) AS blocks, 
								 (SELECT COUNT(bid) FROM `" . DB_PREFIX . "_bans`) AS bans,
								 (SELECT COUNT(aid) FROM `" . DB_PREFIX . "_admins` WHERE aid > 0) AS admins,
								 (SELECT COUNT(subid) FROM `" . DB_PREFIX . "_submissions` WHERE archiv = '0') AS subs,
								 (SELECT COUNT(subid) FROM `" . DB_PREFIX . "_submissions` WHERE archiv > 0) AS archiv_subs,
								 (SELECT COUNT(pid) FROM `" . DB_PREFIX . "_protests` WHERE archiv = '0') AS protests,
								 (SELECT COUNT(pid) FROM `" . DB_PREFIX . "_protests` WHERE archiv > 0) AS archiv_protests,
								 (SELECT COUNT(sid) FROM `" . DB_PREFIX . "_servers`) AS servers");

    $demsi = getDirectorySize(SB_DEMOS);

    $theme->assign('modules', CModule::getAllModules());
    
    $theme->assign('access_admins', 	$userbank->HasAccess(ADMIN_OWNER|ADMIN_LIST_ADMINS|ADMIN_ADD_ADMINS|ADMIN_EDIT_ADMINS|ADMIN_DELETE_ADMINS));
    $theme->assign('access_servers', 	$userbank->HasAccess(ADMIN_OWNER|ADMIN_LIST_SERVERS|ADMIN_ADD_SERVER|ADMIN_EDIT_SERVERS|ADMIN_DELETE_SERVERS));
    $theme->assign('access_bans', 		$userbank->HasAccess(ADMIN_OWNER|ADMIN_ADD_BAN|ADMIN_EDIT_OWN_BANS|ADMIN_EDIT_GROUP_BANS|ADMIN_EDIT_ALL_BANS|ADMIN_BAN_PROTESTS|ADMIN_BAN_SUBMISSIONS));
    $theme->assign('access_groups', 	$userbank->HasAccess( ADMIN_OWNER|ADMIN_LIST_GROUPS|ADMIN_ADD_GROUP|ADMIN_EDIT_GROUPS|ADMIN_DELETE_GROUPS ));
    $theme->assign('access_settings', 	$userbank->HasAccess(ADMIN_OWNER|ADMIN_WEB_SETTINGS));
    $theme->assign('access_mods', 		$userbank->HasAccess(ADMIN_OWNER|ADMIN_LIST_MODS|ADMIN_ADD_MODS|ADMIN_EDIT_MODS|ADMIN_DELETE_MODS ));

    $theme->assign('sb_svn', defined('SB_SVN'));

    $theme->assign('demosize', sizeFormat($demsi['size']));
    $theme->assign('total_admins', $counts['admins']);
    $theme->assign('total_bans', $counts['bans']);
    $theme->assign('total_blocks', $counts['blocks']);
    $theme->assign('total_servers', $counts['servers']);
    $theme->assign('total_protests', $counts['protests']);
    $theme->assign('archived_protests', $counts['archiv_protests']);
    $theme->assign('total_submissions', $counts['subs']);
    $theme->assign('archived_submissions', $counts['archiv_subs']);

    $theme->display('page_admin.tpl');
} elseif($_GET['c'] == "groups") {
	CheckAdminAccess( ADMIN_OWNER|ADMIN_LIST_GROUPS|ADMIN_ADD_GROUP|ADMIN_EDIT_GROUPS|ADMIN_DELETE_GROUPS );
    $sbViewParams['breadCrumbs'][] = t('breadcrumbs', 'Group Management');
    if(!isset($_GET['o'])) {
        // ====================[ ADMIN SIDE MENU START ] ===================
        $groupsTabMenu = new CTabsMenu();
        if ($userbank->HasAccess(ADMIN_OWNER | ADMIN_LIST_GROUPS)) {
            $groupsTabMenu->addMenuItem(t('admin/leftmenu', "List groups"), 0);
        }
        if ($userbank->HasAccess(ADMIN_OWNER | ADMIN_ADD_GROUP)) {
            $groupsTabMenu->addMenuItem(t('admin/leftmenu', "Add a group"), 1);
        }
        $groupsTabMenu->outputMenu();
        // ====================[ ADMIN SIDE MENU END ] ===================	

        include TEMPLATES_PATH . "admin.groups.php";	

        $sbViewParams['pageTitle'] = t('admin', 'Group Management');
        $sbViewParams['title'] = t('admin', 'Group Management');
    } elseif($_GET['o'] == 'edit') {
        $groupsTabMenu = new CTabsMenu();
        $groupsTabMenu->addMenuItem(t('admin/leftmenu', "Back"),0, "", "javascript:history.go(-1);", true);
        $groupsTabMenu->outputMenu();

        include TEMPLATES_PATH . "admin.edit.group.php";
        
        $sbViewParams['pageTitle'] = t('admin', 'Edit Groups');
        $sbViewParams['title'] = t('admin', 'Edit Groups');
    }
} elseif($_GET['c'] == "admins") {
    // ###################[ Admins ]##################################################################
    // Make sure they are allowed here oO
    CheckAdminAccess( ADMIN_OWNER|ADMIN_LIST_ADMINS|ADMIN_ADD_ADMINS|ADMIN_EDIT_ADMINS|ADMIN_DELETE_ADMINS );
    
    $sbViewParams['breadCrumbs'][] = t('breadcrumbs', 'Admin management');
    
    if(!isset($_GET['o'])) {	
        // ====================[ ADMIN SIDE MENU START ] ===================
        $adminTabMenu = new CTabsMenu();
        if ($userbank->HasAccess(ADMIN_OWNER | ADMIN_LIST_ADMINS)) {
            $adminTabMenu->addMenuItem(t('admin/leftmenu', "List admins"), 0);
        }
        if($userbank->HasAccess(ADMIN_OWNER|ADMIN_ADD_ADMINS ) ) {
            $adminTabMenu->addMenuItem(t('admin/leftmenu', "Add new admin"), 1);
            $adminTabMenu->addMenuItem(t('admin/leftmenu', "Overrides"), 2);
        }
        $adminTabMenu->outputMenu();
        // ====================[ ADMIN SIDE MENU END ] ===================
        $AdminsPerPage = SB_BANS_PER_PAGE;
        $page = 1;
        $join = "";
        $where = "";
        $advSearchString = "";
        if (isset($_GET['page']) && $_GET['page'] > 0) {
            $page = intval($_GET['page']);
        }
        if(isset($_GET['advSearch'])) {
            // Escape the value, but strip the leading and trailing quote
            $value = substr($GLOBALS['db']->qstr($_GET['advSearch'], get_magic_quotes_gpc()), 1, -1);
            $type = $_GET['advType'];
            switch($type)
            {
                case "name":
                    $where = " AND ADM.user LIKE '%" . $value . "%'";
                break;
                case "steamid":
                    $where = " AND ADM.authid = '" . $value . "'";
                break;
                case "steam":
                    $where = " AND ADM.authid LIKE '%" . $value . "%'";
                break;
                case "admemail":
                    $where = " AND ADM.email LIKE '%" . $value . "%'";
                break;
                case "webgroup":
                    $where = " AND ADM.gid = '" . $value . "'";
                break;
                case "srvadmgroup":
                    $where = " AND ADM.srv_group = '" . $value . "'";
                break;
                case "srvgroup":
                    $where = " AND SG.srv_group_id = '" . $value . "'";
                    $join = " LEFT JOIN `" . DB_PREFIX . "_admins_servers_groups` AS SG ON SG.admin_id = ADM.aid";
                break;
                case "admwebflag":
                    $findflags = explode(",",$value);
                    foreach ($findflags AS $flag) {
                        $flags[] = constant($flag);
                    }
                    $flagstring = implode('|',$flags);
                    $alladmins = $GLOBALS['db']->Execute("SELECT aid FROM `" . DB_PREFIX . "_admins` WHERE aid > 0");
                    while(!$alladmins->EOF) {
                        if($userbank->HasAccess($flagstring, $alladmins->fields["aid"])) {
                            if (!isset($accessaid)) {
                                $accessaid = $alladmins->fields["aid"];
                            }
                            $accessaid .= ",".$alladmins->fields["aid"];
                        }
                        $alladmins->MoveNext();
                    }
                    $where = " AND ADM.aid IN(".$accessaid.")";
                break;
                case "admsrvflag":
                    $findflags = explode(",",$value);
                    foreach ($findflags AS $flag) {
                        $flags[] = constant($flag);
                    }
                    $alladmins = $GLOBALS['db']->Execute("SELECT aid, authid FROM `" . DB_PREFIX . "_admins` WHERE aid > 0");
                    while(!$alladmins->EOF) {
                        foreach($flags AS $fla) {
                            if(strstr(get_user_admin($alladmins->fields["authid"]), $fla)) {
                                if (!isset($accessaid)) {
                                    $accessaid = $alladmins->fields["aid"];
                                }
                                $accessaid .= ",".$alladmins->fields["aid"];
                            }
                        }
                        if(strstr(get_user_admin($alladmins->fields["authid"]), 'z')) {
                            if (!isset($accessaid)) {
                                $accessaid = $alladmins->fields["aid"];
                            }
                            $accessaid .= ",".$alladmins->fields["aid"];
                        }
                        $alladmins->MoveNext();
                    }
                    $where = " AND ADM.aid IN(".$accessaid.")";
                break;
                case "server":
                    $where = " AND (ASG.server_id = '" . $value . "' OR SG.server_id = '" . $value . "')";
                    $join = " LEFT JOIN `" . DB_PREFIX . "_admins_servers_groups` AS ASG ON ASG.admin_id = ADM.aid LEFT JOIN `" . DB_PREFIX . "_servers_groups` AS SG ON SG.group_id = ASG.srv_group_id";
                break;
                default:
                    $_GET['advSearch'] = "";
                    $_GET['advType'] = "";
                    $where = "";
                break;
            }
            $advSearchString = "&advSearch=".$_GET['advSearch']."&advType=".$_GET['advType'];
        }
        $sql = "SELECT * FROM `" 
                . DB_PREFIX . "_admins` AS ADM" 
                . $join
                . " WHERE ADM.aid > 0" 
                . $where . " ORDER BY user LIMIT " 
                . intval(($page-1) * $AdminsPerPage) 
                . "," 
                . intval($AdminsPerPage);
        $admins = $GLOBALS['db']->GetAll($sql);
        // quick fix for the server search showing admins mulitple times.
        if(isset($_GET['advSearch']) && isset($_GET['advType']) && $_GET['advType'] == 'server') {
            $aadm = array();
            $num = 0;
            foreach($admins as $aadmin) {
                if (!in_array($aadmin['aid'], $aadm)) {
                    $aadm[] = $aadmin['aid'];
                } else {
                    unset($admins[$num]);
                }
                $num++;
            }
        }

        $query = $GLOBALS['db']->GetRow("SELECT COUNT(ADM.aid) AS cnt FROM `" . DB_PREFIX . "_admins` AS ADM".$join." WHERE ADM.aid > 0".$where);
        $admin_count = $query['cnt'];
        include TEMPLATES_PATH . "admin.admins.php";
        $sbViewParams['pageTitle'] = t('admin', 'Admin Management');
        $sbViewParams['title'] = t('admin', 'Admin Management');
    } elseif(
        $_GET['o'] == 'editgroup'
            ||
        $_GET['o'] == 'editdetails'
            ||
        $_GET['o'] == 'editpermissions'
            ||
        $_GET['o'] == 'editservers'
    ) {
        $adminTabMenu = new CTabsMenu();
        $adminTabMenu->addMenuItem(t('admin/leftmenu', "Back"), 0,"", "javascript:history.go(-1);", true);
        $adminTabMenu->outputMenu();

        if($_GET['o'] == 'editgroup') {
            include TEMPLATES_PATH . "admin.edit.admingroup.php";
            $sbViewParams['pageTitle'] = t('admin', 'Edit Admin Groups');
            $sbViewParams['title'] = t('admin', 'Edit Admin Groups');
        } elseif($_GET['o'] == 'editdetails') {
            include TEMPLATES_PATH . "admin.edit.admindetails.php";
            $sbViewParams['pageTitle'] = t('admin', 'Edit Admin Details');
            $sbViewParams['title'] = t('admin', 'Edit Admin Details');
        } elseif($_GET['o'] == 'editpermissions') {
            include TEMPLATES_PATH . "admin.edit.adminperms.php";
            $sbViewParams['pageTitle'] = t('admin', 'Edit Admin Permissions');
            $sbViewParams['title'] = t('admin', 'Edit Admin Permissions');
        } elseif($_GET['o'] == 'editservers') {
            include TEMPLATES_PATH . "admin.edit.adminservers.php";
            $sbViewParams['pageTitle'] = t('admin', 'Edit Server Access');
            $sbViewParams['title'] = t('admin', 'Edit Server Access');
        }
    }
} elseif($_GET['c'] == "servers") {
    
} elseif($_GET['c'] == "bans") {
    
} elseif($_GET['c'] == "mods") {
    // ###################[ Mods ]##################################################################
    CheckAdminAccess( ADMIN_OWNER|ADMIN_LIST_MODS|ADMIN_ADD_MODS|ADMIN_EDIT_MODS|ADMIN_DELETE_MODS );
    
    $sbViewParams['breadCrumbs'][] = t('breadcrumbs', 'Mod management');
    
    if(!isset($_GET['o'])) {
        // ====================[ ADMIN SIDE MENU START ] ===================
        $modTabMenu = new CTabsMenu();
        if ($userbank->HasAccess(ADMIN_OWNER | ADMIN_LIST_MODS)) {
            $modTabMenu->addMenuItem(t('admin/leftmenu', 'MODs'), 0);
        }
        if ($userbank->HasAccess(ADMIN_OWNER | ADMIN_ADD_MODS)) {
            $modTabMenu->addMenuItem(t('admin/leftmenu', 'Add new MOD'), 1);
        }
        $modTabMenu->outputMenu();
        // ====================[ ADMIN SIDE MENU END ] ===================	

        $mod_list = $GLOBALS['db']->GetAll("SELECT * FROM `" . DB_PREFIX . "_mods` WHERE mid > 0 ORDER BY name ASC") ;
        $query = $GLOBALS['db']->GetRow("SELECT COUNT(mid) AS cnt FROM `" . DB_PREFIX . "_mods`") ;
        $mod_count = $query['cnt'];
        include TEMPLATES_PATH . "admin.mods.php";
        $sbViewParams['pageTitle'] = t('admin', 'Manage Mods');
        $sbViewParams['title'] = t('admin', 'Manage Mods');
    } elseif($_GET['o'] == 'edit') {
        
        $modTabMenu = new CTabsMenu();
        $modTabMenu->addMenuItem(t('admin/leftmenu', 'Back'),0, "", "javascript:history.go(-1);", true);
        $modTabMenu->outputMenu();					

        include TEMPLATES_PATH . "admin.edit.mod.php";
        $sbViewParams['pageTitle'] = t('admin', 'Edit Mod Details');
        $sbViewParams['title'] = t('admin', 'Edit Mod Details');
    }	
} elseif($_GET['c'] == "settings") {
    // ###################[ Settings ]##################################################################
    CheckAdminAccess( ADMIN_OWNER|ADMIN_WEB_SETTINGS );	
    
    $sbViewParams['breadCrumbs'][] = t('breadcrumbs', 'SourceBans settings');
    
    // ====================[ ADMIN SIDE MENU START ] ===================
    $settingsTabMenu = new CTabsMenu();
    $settingsTabMenu->addMenuItem(t('admin/leftmenu', 'Main Settings'),0);
    $settingsTabMenu->addMenuItem(t('admin/leftmenu', 'Features'),3);
    $settingsTabMenu->addMenuItem(t('admin/leftmenu', 'Email settings'),4);
    $settingsTabMenu->addMenuItem(t('admin/leftmenu', 'Themes'), 1);
    $settingsTabMenu->addMenuItem(t('admin/leftmenu', 'System Log'), 2);
    $settingsTabMenu->outputMenu();
    // ====================[ ADMIN SIDE MENU END ] ===================

    include TEMPLATES_PATH . "admin.settings.php";
    $sbViewParams['pageTitle'] = t('admin', 'SourceBans Settings');
    $sbViewParams['title'] = t('admin', 'SourceBans Settings');
} elseif($_GET['c'] == "links") {
    CheckAdminAccess( ADMIN_OWNER|ADMIN_WEB_SETTINGS );	
    
    $sbViewParams['breadCrumbs'][] = t('breadcrumbs', 'Links management');
    
    $adminTabMenu = new CTabsMenu();
    $adminTabMenu->addMenuItem("Все ссылки", 0);
    $adminTabMenu->addMenuItem("Новая ссылка", 1);
    $adminTabMenu->outputMenu();
    include TEMPLATES_PATH . "admin.links.php";
    $sbViewParams['pageTitle'] = t('admin', 'Manage links');
    $sbViewParams['title'] = t('admin', 'Manage links');
}
