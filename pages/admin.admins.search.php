<?php
/**
 * =============================================================================
 * Admin search box
 * 
 * @author SteamFriends Development Team
 * @version 1.0.0
 * @copyright SourceBans (C)2007 SteamFriends.com.  All rights reserved.
 * @package SourceBans
 * @link http://www.sourcebans.net
 * 
 * @version $Id: admin.admins.search.php 24 2007-11-06 18:17:05Z olly $
 * =============================================================================
 */

global $userbank, $theme;

//serverlist
$server_list = $GLOBALS['db']->Execute("SELECT sid, ip, port FROM `" . DB_PREFIX . "_servers` WHERE enabled = 1");
$servers = array();
$serverscript = "<script type=\"text/javascript\">";
while (!$server_list->EOF)
{
	$info = array();
    $serverscript .= "xajax_ServerHostPlayers('".$server_list->fields[0]."', 'id', 'ss".$server_list->fields[0]."', '', '', false, 200);";
	$info['sid'] = $server_list->fields[0];
	$info['ip'] = $server_list->fields[1];
	$info['port'] = $server_list->fields[2];
	array_push($servers,$info);
	$server_list->MoveNext();
}
$serverscript .= "</script>";

//webgrouplist
$webgroup_list = $GLOBALS['db']->Execute("SELECT gid, name FROM ". DB_PREFIX ."_groups WHERE type = '1'");
$webgroups = array();
while (!$webgroup_list->EOF)
{
	$data = array();
	$data['gid'] = $webgroup_list->fields['gid'];
	$data['name'] = $webgroup_list->fields['name'];

	array_push($webgroups,$data);
	$webgroup_list->MoveNext();
}

//serveradmingrouplist
$srvadmgroup_list = $GLOBALS['db']->Execute("SELECT name FROM ". DB_PREFIX ."_srvgroups ORDER BY name ASC");
$srvadmgroups = array();
while (!$srvadmgroup_list->EOF)
{
	$data = array();
	$data['name'] = $srvadmgroup_list->fields['name'];
	
	array_push($srvadmgroups,$data);
	$srvadmgroup_list->MoveNext();
}

//servergroup
$srvgroup_list = $GLOBALS['db']->Execute("SELECT gid, name FROM " . DB_PREFIX . "_groups WHERE type = '3'");
$srvgroups = array();
while (!$srvgroup_list->EOF)
{
	$data = array();
	$data['gid'] = $srvgroup_list->fields['gid'];
	$data['name'] = $srvgroup_list->fields['name'];
	
	array_push($srvgroups,$data);
	$srvgroup_list->MoveNext();
}

//webpermissions

$webflag = array(
    array(
        'name' => t('webperms', 'Owner'),
        'flag'=>'ADMIN_OWNER'
    ),
    array(
        'name' => t('webperms', 'View admins'),
        'flag'=>'ADMIN_LIST_ADMINS'
    ),
    array(
        'name' => t('webperms', 'Add admins'),
        'flag'=>'ADMIN_ADD_ADMINS'
    ),
    array(
        'name' => t('webperms', 'Edit admins'),
        'flag'=>'ADMIN_EDIT_ADMINS'
    ),
    array(
        'name' => t('webperms', 'Delete admins'),
        'flag'=>'ADMIN_DELETE_ADMINS'
    ),
    array(
        'name' => t('webperms', 'View servers'),
        'flag'=>'ADMIN_LIST_SERVERS'
    ),
    array(
        'name' => t('webperms', 'Add servers'),
        'flag'=>'ADMIN_ADD_SERVER'
    ),
    array(
        'name' => t('webperms', 'Edit servers'),
        'flag'=>'ADMIN_EDIT_SERVERS'
    ),
    array(
        'name' => t('webperms', 'Delete servers'),
        'flag'=>'ADMIN_DELETE_SERVERS'
    ),
    array(
        'name' => t('webperms', 'Add bans'),
        'flag'=>'ADMIN_ADD_BAN'
    ),
    array(
        'name' => t('webperms', 'Edit own bans'),
        'flag'=>'ADMIN_EDIT_OWN_BANS'
    ),
    array(
        'name' => t('webperms', 'Edit groups bans'),
        'flag'=>'ADMIN_EDIT_GROUP_BANS'
    ),
    array(
        'name' => t('webperms', 'Edit all bans'),
        'flag'=>'ADMIN_EDIT_ALL_BANS'
    ),
    array(
        'name' => t('webperms', 'Ban protests'),
        'flag'=>'ADMIN_BAN_PROTESTS'
    ),
    array(
        'name' => t('webperms', 'Ban submissions'),
        'flag'=>'ADMIN_BAN_SUBMISSIONS'
    ),
    array(
        'name' => t('webperms', 'Delete All bans'),
        'flag'=>'ADMIN_DELETE_BAN'
    ),
    array(
        'name' => t('webperms', 'Unban own bans'),
        'flag'=>'ADMIN_UNBAN_OWN_BANS'
    ),
    array(
        'name' => t('webperms', 'Unban group bans'),
        'flag'=>'ADMIN_UNBAN_GROUP_BANS'
    ),
    array(
        'name' => t('webperms', 'Unban all bans'),
        'flag'=>'ADMIN_UNBAN'
    ),
    array(
        'name' => t('webperms', 'Import bans'),
        'flag'=>'ADMIN_BAN_IMPORT'
    ),
    array(
        'name' => t('webperms', 'Submission email notifying'),
        'flag'=>'ADMIN_NOTIFY_SUB'
    ),
    array(
        'name' => t('webperms', 'Protest email notifying'),
        'flag'=>'ADMIN_NOTIFY_PROTEST'
    ),
    array(
        'name' => t('webperms', 'List groups'),
        'flag'=>'ADMIN_LIST_GROUPS'
    ),
    array(
        'name' => t('webperms', 'Add groups'),
        'flag'=>'ADMIN_ADD_GROUP'
    ),
    array(
        'name' => t('webperms', 'Edit groups'),
        'flag'=>'ADMIN_EDIT_GROUPS'
    ),
    array(
        'name' => t('webperms', 'Delete groups'),
        'flag'=>'ADMIN_DELETE_GROUPS'
    ),
    array(
        'name' => t('webperms', 'Web settings'),
        'flag'=>'ADMIN_WEB_SETTINGS'
    ),
    array(
        'name' => t('webperms', 'List mods'),
        'flag'=>'ADMIN_LIST_MODS'
    ),
    array(
        'name' => t('webperms', 'Add mods'),
        'flag'=>'ADMIN_ADD_MODS'
    ),
    array(
        'name' => t('webperms', 'Edit mods'),
        'flag'=>'ADMIN_EDIT_MODS'
    ),
    array(
        'name' => t('webperms', 'Delete mods'),
        'flag'=>'ADMIN_DELETE_MODS'
    ),
);

$webflags = array();
foreach($webflag AS $flag) {
	$data['name'] = $flag["name"];
	$data['flag'] = $flag["flag"];
	array_push($webflags, $data);
}

//server permissions
$serverflag = array(
    array(
        'name' => t('srvperms', 'Full Admin'),
        'flag' => 'SM_ROOT'
    ),
    array(
        'name' => t('srvperms', 'Reserved slot'),
        'flag' => 'SM_RESERVED_SLOT'
    ),
    array(
        'name' => t('srvperms', 'Generic admin'),
        'flag' => 'SM_GENERIC'
    ),
    array(
        'name' => t('srvperms', 'Kick'),
        'flag' => 'SM_KICK'
    ),
    array(
        'name' => t('srvperms', 'Ban'),
        'flag' => 'SM_BAN'
    ),
    array(
        'name' => t('srvperms', 'Unban'),
        'flag' => 'SM_UNBAN'
    ),
    array(
        'name' => t('srvperms', 'Slay'),
        'flag' => 'SM_SLAY'
    ),
    array(
        'name' => t('srvperms', 'Map change'),
        'flag' => 'SM_MAP'
    ),
    array(
        'name' => t('srvperms', 'Change cvars'),
        'flag' => 'SM_CVAR'
    ),
    array(
        'name' => t('srvperms', 'Run configs'),
        'flag' => 'SM_CONFIG'
    ),
    array(
        'name' => t('srvperms', 'Admin chat'),
        'flag' => 'SM_CHAT'
    ),
    array(
        'name' => t('srvperms', 'Start votes'),
        'flag' => 'SM_VOTE'
    ),
    array(
        'name' => t('srvperms', 'Password server'),
        'flag' => 'SM_PASSWORD'
    ),
    array(
        'name' => t('srvperms', 'RCON'),
        'flag' => 'SM_RCON'
    ),
    array(
        'name' => t('srvperms', 'Enable Cheats'),
        'flag' => 'SM_CHEATS'
    ),
    array(
        'name' => t('srvperms', 'Custom flag 1'),
        'flag' => 'SM_CUSTOM1'
    ),
    array(
        'name' => t('srvperms', 'Custom flag 2'),
        'flag' => 'SM_CUSTOM2'
    ),
    array(
        'name' => t('srvperms', 'Custom flag 3'),
        'flag' => 'SM_CUSTOM3'
    ),
    array(
        'name' => t('srvperms', 'Custom flag 4'),
        'flag' => 'SM_CUSTOM4'
    ),
    array(
        'name' => t('srvperms', 'Custom flag 5'),
        'flag' => 'SM_CUSTOM5'
    ),
    array(
        'name' => t('srvperms', 'Custom flag 6'),
        'flag' => 'SM_CUSTOM6'
    ),
);


$serverflags = array();
foreach($serverflag AS $flag) {
	$data['name'] = $flag["name"];
	$data['flag'] = $flag["flag"];
	array_push($serverflags, $data);
}


$theme->assign('server_list', $servers);
$theme->assign('server_script', $serverscript);
$theme->assign('webgroup_list', $webgroups);
$theme->assign('srvadmgroup_list', $srvadmgroups);
$theme->assign('srvgroup_list', $srvgroups);
$theme->assign('admwebflag_list', $webflags);
$theme->assign('admsrvflag_list', $serverflags);
$theme->assign('can_editadmin', $userbank->HasAccess(ADMIN_EDIT_ADMINS|ADMIN_OWNER));

$theme->display('box_admin_admins_search.tpl');
