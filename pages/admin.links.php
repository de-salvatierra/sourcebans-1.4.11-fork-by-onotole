<?php
if(!defined("IN_SB")){echo "You should not be here. Only follow links!";die();} 

global $theme, $userbank;

$links = $GLOBALS['db']->GetAll("SELECT * FROM `".DB_PREFIX."_links` ORDER BY `position` ASC, `id` ASC");
$firstLast = $GLOBALS['db']->GetRow("SELECT MIN(`position`) as `min`, MAX(`position`) as `max` FROM `".DB_PREFIX."_links`");

$theme->assign('links_count', count($links));
$theme->assign('links', $links);
$theme->assign('firstLast', $firstLast);
$theme->display('page_admin_links.tpl');
