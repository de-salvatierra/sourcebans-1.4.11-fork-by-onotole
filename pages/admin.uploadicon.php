<?php
/**
 * =============================================================================
 * Update an icon
 * 
 * @author SteamFriends Development Team
 * @version 1.0.0
 * @copyright SourceBans (C)2007 SteamFriends.com.  All rights reserved.
 * @package SourceBans
 * @link http://www.sourcebans.net
 * 
 * @version $Id: admin.uploadicon.php 179 2008-12-11 20:37:10Z peace-maker $
 * =============================================================================
 */

include_once("../init.php");
include_once("../includes/system-functions.php");
global $theme, $userbank;

if (!$userbank->HasAccess(ADMIN_OWNER|ADMIN_EDIT_MODS|ADMIN_ADD_MODS))
{
    new CSystemLog(
        "w",
        t('log', "Hacking Attempt"),
        t('log', '[[user]] tried to upload a mod icon, but doesn\'t have access.', array(
            '[[user]]' => $userbank->GetProperty("user"),
        ))
    );
    
	echo t('admin', 'You don\'t have access to this!');
	die();
}

$message = "";

$approwedExts = array(
    "gif",
    "jpg",
    "png",
);
$approwedExtsStr = implode(', ', $approwedExts);
if(isset($_POST['upload'])) {
	if(CheckExt($_FILES['icon_file']['name'], $approwedExts)) {
		move_uploaded_file($_FILES['icon_file']['tmp_name'],SB_ICONS.$_FILES['icon_file']['name']);
		$message =  "<script>window.opener.icon('" . $_FILES['icon_file']['name'] . "');self.close()</script>";
        new CSystemLog(
            "m",
            t('log', "Mod Icon Uploaded"),
            t('log', 'A new mod icon has been uploaded: [[iconfile]]', array(
                '[[iconfile]]' => htmlspecialchars($_FILES['icon_file']['name']),
            ))
        );
	} else {
		$message = t('admin', 'The file must have the following extension: [[extensions]]', array(
            '[[extensions]]' => $approwedExtsStr
        ));
	}
}

$theme->assign("title", t('admin', 'Upload Icon'));
$theme->assign("message", $message);
$theme->assign("input_name", "icon_file");
$theme->assign("form_name", "iconup");
$theme->assign("formats", $approwedExtsStr);

$theme->display('page_uploadfile.tpl');
