<?php
/* @var $GLOBALS['db'] ADODB_mysqli */
/**
 * =============================================================================
 * AJAX Callback handler
 *
 * @author SteamFriends Development Team
 * @version 1.0.0
 * @copyright SourceBans (C)2007 SteamFriends.com.  All rights reserved.
 * @package SourceBans
 * @link http://www.sourcebans.net
 *
 * @version $Id: sb-callback.php 140 2008-08-31 15:30:35Z peace-maker
 * =============================================================================
 */
require_once('xajax.inc.php');

global $xajax;
$xajax = new xajax();
//$xajax->debugOn();
$xajax->setRequestURI('./index.php');
global $userbank;

if (isset($_COOKIE['aid'], $_COOKIE['password']) && $userbank->CheckLogin($_COOKIE['password'], $_COOKIE['aid'])) {
    $xajax->registerFunction("AddGroup");
    $xajax->registerFunction("RemoveGroup");
    $xajax->registerFunction("RemoveAdmin");
    $xajax->registerFunction("UpdateGroupPermissions");
    $xajax->registerFunction("UpdateAdminPermissions");
    $xajax->registerFunction("AddAdmin");
    $xajax->registerFunction("SetupEditServer");
    $xajax->registerFunction("AddServerGroupName");
    $xajax->registerFunction("EditGroup");
    $xajax->registerFunction("EditAdminPerms");
    $xajax->registerFunction("SelTheme");
    $xajax->registerFunction("ApplyTheme");
    $xajax->registerFunction("ClearCache");
    $xajax->registerFunction("KickPlayer");
    $xajax->registerFunction("RehashAdmins");
    $xajax->registerFunction("GetGroups");
    $xajax->registerFunction("SendMessage");
    $xajax->registerFunction("ViewCommunityProfile");
    $xajax->registerFunction("CheckPassword");
    $xajax->registerFunction("ChangePassword");
    $xajax->registerFunction("CheckSrvPassword");
    $xajax->registerFunction("ChangeSrvPassword");
    $xajax->registerFunction("ChangeEmail");
    $xajax->registerFunction("CheckVersion");
    $xajax->registerFunction("SaveLink");
    $xajax->registerFunction("DeleteLink");
}

$xajax->registerFunction("Plogin");
$xajax->registerFunction("ServerHostPlayers");
$xajax->registerFunction("ServerHostProperty");
$xajax->registerFunction("ServerHostPlayers_list");
$xajax->registerFunction("ServerPlayers");
$xajax->registerFunction("LostPassword");
$xajax->registerFunction("RefreshServer");
$xajax->registerFunction("SelectLanguage");
$xajax->registerFunction("SortLink");

global $userbank;
$username = $userbank->GetProperty("user");

function SortLink($id, $operation) {
    $id = intval($id);
    $objResponse = new xajaxResponse();
    $link = $GLOBALS['db']->GetRow("SELECT `position` FROM `".DB_PREFIX."_links` WHERE `id` = {$id}");
    $maxPos = $GLOBALS['db']->GetRow("SELECT MAX(`position`) as `max` FROM `".DB_PREFIX."_links`");
    if(
        !$link
            ||
        !$maxPos
            ||
            /*
        (
            $operation == 'up' && $link['position'] == 1
        )
            ||
        (
            $operation == 'down' && $link['position'] == intval($maxPos['max'])
        )
            ||
             * 
             */
        $link['position'] > (intval($maxPos['max']) + 1)
    ) {
        return $objResponse;
    }
    if($operation == 'up') {
        $newPos = intval($link['position'] - 1);
        $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_links` SET `position` = (`position` + 1) WHERE `position` = {$newPos}");
        $GLOBALS['db']->Execute("UPDATE `".DB_PREFIX."_links` SET `position` = {$newPos} WHERE `id` = {$id}");
    } elseif($operation == 'down') {
        $newPos = intval($link['position'] + 1);
        $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_links` SET `position` = (`position` - 1) WHERE `position` = {$newPos}");
        $GLOBALS['db']->Execute("UPDATE `".DB_PREFIX."_links` SET `position` = {$newPos} WHERE `id` = {$id}");
    } else {
        return $objResponse;
    }
    $GLOBALS['db']->CacheFlush();
    $objResponse->addRedirect('index.php?p=admin&c=links');
    return $objResponse;
}

function DeleteLink($id) {
    global $userbank, $username;
    $objResponse = new xajaxResponse();
    if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_WEB_SETTINGS)) {
        $objResponse->redirect("index.php?p=login&n=no_access", 0);
        new CSystemLog(
            'w',
            t('log', 'Hacking Attempt'),
            t('log', '[[username]] tried delete a link, but doesnt have access.', array(
                '[[username]]' => $username,
            ))
        );
        return $objResponse;
    }
    
    $del = $GLOBALS['db']->Execute("DELETE FROM `".DB_PREFIX."_links` WHERE `id` = " . intval($id));
    if(!$del) {
        $objResponse->addAlert(t('admin/links', 'When you delete a link error occurred'));
    }
    $GLOBALS['db']->CacheFlush();
    $objResponse->addRedirect('index.php?p=admin&c=links');
    return $objResponse;
}

function SaveLink($linkLabel, $linkUrl, $linkTitle, $linkActive, $linkId, $hideGuests) {
    global $userbank, $username;
    $objResponse = new xajaxResponse();
    
    if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_WEB_SETTINGS)) {
        $objResponse->redirect("index.php?p=login&n=no_access", 0);
        new CSystemLog(
            'w',
            t('log', 'Hacking Attempt'),
            t('log', '[[username]] tried to create or edit a link, but doesnt have access.', array(
                '[[username]]' => $username,
            ))
        );
        return $objResponse;
    }
    
    $linkActive = $linkActive =='true' ? 1 : 0;
    $hideGuests = $hideGuests =='true' ? 1 : 0;

    if($linkId) {
        $oldLink = $GLOBALS['db']->GetRow("SELECT * FROM `".DB_PREFIX."_links` WHERE `id` = " . intval($linkId));
        if(!$oldLink) {
            $objResponse->addAlert(t('admin/links', 'Link #[[id]] not found', array(
                '[[id]]' => intval($linkId)
            )));
            return $objResponse;
        }
        $andWhere = " AND `id` <> " . intval($linkId);
        $sql = "UPDATE `".DB_PREFIX."_links` SET `label` = ?, `url` = ?, `title` = ?, `active` = ?, `hideGuests` = ? WHERE `id` = ?";
        $params = array($linkLabel, $linkUrl, $linkTitle, $linkActive, $linkId, $hideGuests);
    } else {
        $andWhere = "";
        $pos = $GLOBALS['db']->GetRow("SELECT MAX(`position`) FROM `".DB_PREFIX."_links`");
        $pos = $pos[0] + 1;
        $sql = "INSERT INTO
            `".DB_PREFIX."_links` (`label`, `url`, `title`, `position`, `active`, hideGuests)
            VALUES (
                ?,
                ?,
                ?,
                ?,
                ?,
                ?
            )";
        $params = array($linkLabel, $linkUrl, $linkTitle, $pos, $linkActive, $hideGuests);
    }
    
    $errors = 0;
    
    $checkLabel = $GLOBALS['db']->GetRow(
        "SELECT * FROM `".DB_PREFIX."_links` WHERE `label` = '".RemoveCode($linkLabel)."'" . $andWhere
    );
    $checkUrl = $GLOBALS['db']->GetRow(
        "SELECT * FROM `".DB_PREFIX."_links` WHERE `url` = '".RemoveCode($linkUrl)."'" . $andWhere
    );
    if($checkLabel) {
        $errors++;
        $objResponse->addScript("$('link_label.msg').setHTML('".t('admin/links', 'Link with the same label already exists')."');");
        $objResponse->addScript("$('link_label.msg').setStyle('display', 'block');");
    } else {
        $objResponse->addScript("$('link_label.msg').setHTML('');");
        $objResponse->addScript("$('link_label.msg').setStyle('display', 'none');");
    }
    
    if($checkUrl) {
        $errors++;
        $objResponse->addScript("$('link_url.msg').setHTML('".t('admin/links', 'Link with the same URL already exists')."');");
        $objResponse->addScript("$('link_url.msg').setStyle('display', 'block');");
    } else {
        $objResponse->addScript("$('link_url.msg').setHTML('');");
        $objResponse->addScript("$('link_url.msg').setStyle('display', 'none');");
    }
    
    $urlRegexp = $r = '/(https?:\/\/)?(www\.)?([-а-яёa-z0-9_\.]{2,}\.)'
        . '(рф|[a-z]{2,6})((\/[-а-яёa-z0-9_]{1,})?\/?([a-z0-9_-]{2,}\.[a-z]{2,6})?'
        . '(\?[a-z0-9_]{2,}=[-0-9]{1,})?((\&[a-z0-9_]{2,}=[-0-9]{1,}){1,})?)/i';
    
    if(!preg_match($urlRegexp, $linkUrl)) {
        $errors++;
        $objResponse->addScript("$('link_url.msg').setHTML('".t('admin/links', 'Incorrect URL')."');");
        $objResponse->addScript("$('link_url.msg').setStyle('display', 'block');");
    } else {
        $objResponse->addScript("$('link_url.msg').setHTML('');");
        $objResponse->addScript("$('link_url.msg').setStyle('display', 'none');");
    }
    
    if($errors > 0) {
        return $objResponse;
    }
    $pre = $GLOBALS['db']->Prepare($sql);
    $GLOBALS['db']->Execute($pre, $params);
    if(!$linkId) {
        new CSystemLog(
            'm',
            t('log', 'New link created'),
            t('log', '[[username]] has added a new link "[[linkLabel]]"', array(
                '[[username]]' => $username,
                '[[linkLabel]]' => $linkLabel
            ))
        );
    } else {
        new CSystemLog(
            'm',
            t('log', 'Link updated'),
            t('log', '[[username]] has edited the link "[[linkLabel]]"', array(
                '[[username]]' => $username,
                '[[linkLabel]]' => $linkLabel
            ))
        );
    }
    $GLOBALS['db']->CacheFlush();
    $objResponse->addRedirect('index.php?p=admin&c=links');
    return $objResponse;
}

function SelectLanguage($lang) {
    global $language;
    $objResponse = new xajaxResponse();
    if($lang != $language->getLanguage() && $language->setClientLanguage($lang)) {
        $objResponse->addScript('location.href=location.href');
    }
    return $objResponse;
}

function Plogin($username, $password, $remember, $redirect, $nopass) {
    global $userbank;
    $objResponse = new xajaxResponse();
    $q = $GLOBALS['db']->GetRow("SELECT `aid`, `password` FROM `" . DB_PREFIX . "_admins` WHERE `user` = ?", array($username));
    if ($q) {
        $aid = $q[0];
    }
    if ($q && strlen($q[1]) == 0 && count($q) != 0) {
        $objResponse->addScript('ShowBox("'.t('login', 'Information').'", "'.t('login', 'You can\'t login. No password set.').'", "blue", "", true);');
        return $objResponse;
    } else if (!$q || !$userbank->CheckLogin($userbank->encrypt_password($password), $aid)) {
        if ($nopass != 1) {
            $objResponse->addScript('ShowBox("'.t('login', 'Login Failed').'", "'.t('login', 'The username or password you supplied was incorrect.<br \> If you have forgotten your password, use the <a href=\"index.php?p=lostpassword\" title=\"Lost password\">Lost Password</a> link.').'", "red", "", true);');
        }
        return $objResponse;
    } else {
        $objResponse->addScript("$('msg-red').setStyle('display', 'none');");
    }

    $userbank->login($aid, $password, $remember);

    if (strstr($redirect, "validation") || empty($redirect)) {
        $objResponse->addRedirect("?", 0);
    } else {
        $objResponse->addRedirect("?" . $redirect, 0);
    }
    return $objResponse;
}

function LostPassword($email) {
    global $userbank;
    $objResponse = new xajaxResponse();
    
    if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $objResponse->addScript("ShowBox('".t('sb-callback', 'Error')."', '".t('lostpassword', 'Please enter a valid E-mail address')."', 'red', '');");
        return $objResponse;
    } else {
        $objResponse->addScript("$('msg-red').setStyle('display', 'none');");
    }
    
    $q = $GLOBALS['db']->GetRow("SELECT * FROM `" . DB_PREFIX . "_admins` WHERE `email` = ?", array($email));

    if (!$q[0]) {
        $objResponse->addScript("ShowBox('".t('sb-callback', 'Error')."', '".t('lostpassword', 'The email address you supplied is not registered on the system')."', 'red', '');");
        return $objResponse;
    } else {
        $objResponse->addScript("$('msg-red').setStyle('display', 'none');");
    }

    $validation = md5(generate_salt(20) . generate_salt(20)) . md5(generate_salt(20) . generate_salt(20));
    $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_admins` SET `validate` = ? WHERE `email` = ?", array($validation, $email));
    $message = "";
    $message .= t('mail/lostpasswd', "Hello [[user]]", array('[[user]]' => $q['user'])) . PHP_EOL;
    $message .= t('mail/lostpasswd', "You have requested to have your password reset for your SourceBans account.") . PHP_EOL;
    $message .= t('mail/lostpasswd', "To complete this process, please click the following link.") . PHP_EOL;
    $message .= t('mail/lostpasswd', "NOTE: If you didnt request this reset, then simply ignore this email.") . PHP_EOL;

    $message .= "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . "?p=lostpassword&email=" . RemoveCode($email) . "&validation=" . $validation;

    $userbank->sendMail(
        $email,
        t('mail/lostpasswd', "SourceBans Password Reset"),
        $message
    );

    $objResponse->addScript("ShowBox('".t('mail/lostpasswd', 'Check E-Mail')."', '".t('mail/lostpasswd', 'Please check your email inbox (and spam) for a link which will help you reset your password.')."', 'blue', '');");
    return $objResponse;
}

function CheckSrvPassword($aid, $srv_pass) {
    $objResponse = new xajaxResponse();
    global $userbank, $username;
    $aid = (int) $aid;
    if (!$userbank->is_logged_in() || $aid != $userbank->GetAid()) {
        $objResponse->redirect("index.php?p=login&n=no_access", 0);
        
        new CSystemLog(
            'w',
            t('log', 'Hacking Attempt'),
            t('log', '[[username]] tried to check [[admin]]\'s server password, but doesn\'t have access.', array(
                '[[username]]' => $username,
                '[[admin]]' => $userbank->GetProperty('user', $aid)
            ))
        );
        
        
        return $objResponse;
    }
    $res = $GLOBALS['db']->Execute("SELECT `srv_password` FROM `" . DB_PREFIX . "_admins` WHERE `aid` = '" . $aid . "'");
    if ($res->fields['srv_password'] != NULL && $res->fields['srv_password'] != $srv_pass) {
        $objResponse->addScript("$('scurrent.msg').setStyle('display', 'block');");
        $objResponse->addScript("$('scurrent.msg').setHTML('".t('youraccount', 'Incorrect password.')."');");
        $objResponse->addScript("set_error(1);");
    } else {
        $objResponse->addScript("$('scurrent.msg').setStyle('display', 'none');");
        $objResponse->addScript("set_error(0);");
    }
    return $objResponse;
}

function ChangeSrvPassword($aid, $srv_pass) {
    $objResponse = new xajaxResponse();
    global $userbank, $username;
    $aid = (int) $aid;
    if (!$userbank->is_logged_in() || $aid != $userbank->GetAid()) {
        $objResponse->redirect("index.php?p=login&n=no_access", 0);
        new CSystemLog(
            'w',
            t('log', 'Hacking Attempt'),
            t('log', '[[username]] tried to change [[admin]]\'s server password, but doesn\'t have access.', array(
                '[[username]]' => $username,
                '[[admin]]' => $userbank->GetProperty('user', $aid)
            ))
        );
        return $objResponse;
    }

    if ($srv_pass == "NULL") {
        $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_admins` SET `srv_password` = NULL WHERE `aid` = '" . $aid . "'");
    } else {
        $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_admins` SET `srv_password` = ? WHERE `aid` = ?", array($srv_pass, $aid));
    }
    $objResponse->addScript("ShowBox('".t('youraccount', 'Server Password changed')."', '".t('youraccount', 'Your server password has been changed successfully.')."', 'green', 'index.php?p=account', true);");

    new CSystemLog(
        'm',
        t('log', 'Srv Password Changed'),
        t('log', 'Password changed for admin ([[adminid]])', array(
            '[[adminid]]' => $aid,
        ))
    );
    
    return $objResponse;
}

function ChangeEmail($aid, $email, $password) {
    global $userbank, $username;
    $objResponse = new xajaxResponse();
    $aid = (int) $aid;

    if (!$userbank->is_logged_in() || $aid != $userbank->GetAid()) {
        $objResponse->redirect("index.php?p=login&n=no_access", 0);
        
        new CSystemLog(
            'w',
            t('log', 'Hacking Attempt'),
            t('log', '[[username]] tried to change [[admin]]\'s email, but doesn\'t have access.', array(
                '[[username]]' => $username,
                '[[admin]]' => $userbank->GetProperty('user', $aid)
            ))
        );
        
        
        return $objResponse;
    }

    if ($userbank->encrypt_password($password) != $userbank->getProperty('password')) {
        $objResponse->addScript("$('emailpw.msg').setStyle('display', 'block');");
        $objResponse->addScript("$('emailpw.msg').setHTML('".t('youraccount', 'The password you supplied is wrong.')."');");
        $objResponse->addScript("set_error(1);");
        return $objResponse;
    } else {
        $objResponse->addScript("$('emailpw.msg').setStyle('display', 'none');");
        $objResponse->addScript("set_error(0);");
    }

    if (!check_email($email)) {
        $objResponse->addScript("$('email1.msg').setStyle('display', 'block');");
        $objResponse->addScript("$('email1.msg').setHTML('".t('youraccount', 'You must type a valid email address.')."');");
        $objResponse->addScript("set_error(1);");
        return $objResponse;
    } else {
        $objResponse->addScript("$('email1.msg').setStyle('display', 'none');");
        $objResponse->addScript("set_error(0);");
    }

    $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_admins` SET `email` = ? WHERE `aid` = ?", array($email, $aid));
    $objResponse->addScript("ShowBox('".t('youraccount', 'E-mail address changed')."', '".t('youraccount', 'Your E-mail address has been changed successfully.')."', 'green', 'index.php?p=account', true);");

    new CSystemLog(
        'm',
        t('log', 'E-mail Changed'),
        t('log', 'E-mail changed for admin ([[adminid]])', array(
            '[[adminid]]' => $aid,
        ))
    );
    
    return $objResponse;
}

function AddGroup($name, $type, $bitmask, $srvflags) {
    $objResponse = new xajaxResponse();
    global $userbank, $username;
    if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_ADD_GROUP)) {
        
        $objResponse->redirect("index.php?p=login&n=no_access", 0);

        new CSystemLog(
            'w',
            t('log', 'Hacking Attempt'),
            t('log', '[[username]] tried to Add a new group, but doesnt have access.', array(
                '[[username]]' => $username,
            ))
        );
        
        return $objResponse;
    }

    $error = 0;
    $query = $GLOBALS['db']->GetRow("SELECT `gid` FROM `" . DB_PREFIX . "_groups` WHERE `name` = ?", array($name));
    $query2 = $GLOBALS['db']->GetRow("SELECT `id` FROM `" . DB_PREFIX . "_srvgroups` WHERE `name` = ?", array($name));
    if (strlen($name) == 0 || count($query) > 0 || count($query2) > 0) {
        if (strlen($name) == 0) {
            $objResponse->addScript("$('name.msg').setStyle('display', 'block');");
            $objResponse->addScript("$('name.msg').setHTML('".t('admin/groups', 'Please enter a name for this group.')."');");
            $error++;
        } else if (strstr($name, ',')) {
            $objResponse->addScript("$('name.msg').setStyle('display', 'block');");
            $objResponse->addScript("$('name.msg').setHTML('".t('admin/groups', 'You cannot have a comma \',\' in a group name.')."');");
            $error++;
        } else if (count($query) > 0 || count($query2) > 0) {
            $objResponse->addScript("$('name.msg').setStyle('display', 'block');");
            $objResponse->addScript("$('name.msg').setHTML('".t('admin/groups', 'A group is already named [[name]]', array('[[name]]' => $name))."');");
            $error++;
        } else {
            $objResponse->addScript("$('name.msg').setStyle('display', 'none');");
            $objResponse->addScript("$('name.msg').setHTML('');");
        }
    }

    if ($type == "0") {
        $objResponse->addScript("$('type.msg').setStyle('display', 'block');");
        $objResponse->addScript("$('type.msg').setHTML('".t('admin/groups', 'Please choose a type for the group.')."');");
        $error++;
    } else {
        $objResponse->addScript("$('type.msg').setStyle('display', 'none');");
        $objResponse->addScript("$('type.msg').setHTML('');");
    }

    if ($error > 0) {
        return $objResponse;
    }

    $query = $GLOBALS['db']->GetRow("SELECT MAX(gid) AS next_gid FROM `" . DB_PREFIX . "_groups`");
    if ($type == "1") {
        // add the web group
        $query1 = $GLOBALS['db']->Execute("INSERT INTO `" . DB_PREFIX . "_groups` (`gid`, `type`, `name`, `flags`) VALUES (" . (int) ($query['next_gid'] + 1) . ", '" . (int) $type . "', ?, '" . (int) $bitmask . "')", array($name));
    } elseif ($type == "2") {
        if (strstr($srvflags, "#")) {
            $immunity = "0";
            $immunity = substr($srvflags, strpos($srvflags, "#") + 1);
            $srvflags = substr($srvflags, 0, strlen($srvflags) - strlen($immunity) - 1);
        }
        $immunity = (isset($immunity) && $immunity > 0) ? $immunity : 0;
        $add_group = $GLOBALS['db']->Prepare("INSERT INTO " . DB_PREFIX . "_srvgroups(immunity,flags,name,groups_immune)
					VALUES (?,?,?,?)");
        $GLOBALS['db']->Execute($add_group, array($immunity, $srvflags, $name, " "));
    } elseif ($type == "3") {
        // We need to add the server into the table
        $query1 = $GLOBALS['db']->Execute("INSERT INTO `" . DB_PREFIX . "_groups` (`gid`, `type`, `name`, `flags`) VALUES (" . ($query['next_gid'] + 1) . ", '3', ?, '0')", array($name));
    }

    new CSystemLog(
        'm',
        t('log', 'Group Created'),
        t('log', 'A new group was created ([[name]])', array(
            '[[name]]' => $name,
        ))
    );
    
    $objResponse->addScript("ShowBox('".t('admin/groups', 'Group Created')."', '".t('admin/groups', 'Your group has been successfully created.')."', 'green', 'index.php?p=admin&c=groups', true);");
    $objResponse->addScript("TabToReload();");
    return $objResponse;
}

function RemoveGroup($gid, $type) {
    $objResponse = new xajaxResponse();
    global $userbank, $username;
    if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_DELETE_GROUPS)) {
        $objResponse->redirect("index.php?p=login&n=no_access", 0);

        new CSystemLog(
            'w',
            t('log', 'Hacking Attempt'),
            t('log', '[[username]] tried to remove a group, but doesnt have access.', array(
                '[[username]]' => $username,
            ))
        );
        
        return $objResponse;
    }

    $gid = (int) $gid;


    if ($type == "web") {
        $query2 = $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_admins` SET gid = -1 WHERE gid = $gid");
        $query1 = $GLOBALS['db']->Execute("DELETE FROM `" . DB_PREFIX . "_groups` WHERE gid = $gid");
    } else if ($type == "server") {
        $query2 = $GLOBALS['db']->Execute("DELETE FROM `" . DB_PREFIX . "_servers_groups` WHERE group_id = $gid");
        $query1 = $GLOBALS['db']->Execute("DELETE FROM `" . DB_PREFIX . "_groups` WHERE gid = $gid");
    } else {
        $query2 = $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_admins` SET srv_group = NULL WHERE srv_group = (SELECT name FROM `" . DB_PREFIX . "_srvgroups` WHERE id = $gid)");
        $query1 = $GLOBALS['db']->Execute("DELETE FROM `" . DB_PREFIX . "_srvgroups` WHERE id = $gid");
        $GLOBALS['db']->Execute("DELETE FROM `" . DB_PREFIX . "_srvgroups_overrides` WHERE group_id = $gid");
    }

    if (isset($GLOBALS['config']['config.enableadminrehashing']) && $GLOBALS['config']['config.enableadminrehashing'] == 1) {
        // rehash the settings out of the database on all servers
        $serveraccessq = $GLOBALS['db']->GetAll("SELECT sid FROM " . DB_PREFIX . "_servers WHERE enabled = 1;");
        $allservers = array();
        foreach ($serveraccessq as $access) {
            if (!in_array($access['sid'], $allservers)) {
                $allservers[] = $access['sid'];
            }
        }
        $rehashing = true;
    }

    $objResponse->addScript("SlideUp('gid_$gid');");
    if ($query1) {
        if (isset($rehashing)) {
            $objResponse->addScript("ShowRehashBox('" . implode(",", $allservers) . "', '".t('sb-callback', 'Group Deleted')."', '".t('sb-callback', 'The selected group has been deleted from the database')."', 'green', 'index.php?p=admin&c=groups', true);");
        } else {
            $objResponse->addScript("ShowBox('".t('sb-callback', 'Group Deleted')."', '".t('sb-callback', 'The selected group has been deleted from the database')."', 'green', 'index.php?p=admin&c=groups', true);");
        }

        new CSystemLog(
            'm',
            t('log', 'Group Deleted'),
            t('log', 'Group ([[group]]) has been deleted', array(
                '[[group]]' => $gid,
            ))
        );
        
    } else {
        $objResponse->addScript("ShowBox('".t('sb-callback', 'Error')."', '".t('sb-callback', 'There was a problem deleting the group from the database. Check the logs for more info')."', 'red', 'index.php?p=admin&c=groups', true);");
    }

    return $objResponse;
}

function RemoveAdmin($aid) {
    $objResponse = new xajaxResponse();
    global $userbank, $username;
    if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_DELETE_ADMINS)) {
        $objResponse->redirect("index.php?p=login&n=no_access", 0);

        new CSystemLog(
            'w',
            t('log', 'Hacking Attempt'),
            t('log', '[[username]] tried to remove an admin, but doesnt have access.', array(
                '[[username]]' => $username,
            ))
        );
        
        return $objResponse;
    }
    $aid = (int) $aid;
    $gid = $GLOBALS['db']->GetRow("SELECT gid, authid, extraflags, user FROM `" . DB_PREFIX . "_admins` WHERE aid = $aid");
    if ((intval($gid[2]) & ADMIN_OWNER) != 0) {
        $objResponse->addAlert(t('sb-callback', 'Error: You cannot delete the owner.'));
        return $objResponse;
    }

    $delquery = $GLOBALS['db']->Execute(sprintf("DELETE FROM `%s_admins` WHERE aid = %d LIMIT 1", DB_PREFIX, $aid));
    if ($delquery) {
        if (isset($GLOBALS['config']['config.enableadminrehashing']) && $GLOBALS['config']['config.enableadminrehashing'] == 1) {
            // rehash the admins for the servers where this admin was on
            $serveraccessq = $GLOBALS['db']->GetAll("SELECT s.sid FROM `" . DB_PREFIX . "_servers` s
												LEFT JOIN `" . DB_PREFIX . "_admins_servers_groups` asg ON asg.admin_id = '" . (int) $aid . "'
												LEFT JOIN `" . DB_PREFIX . "_servers_groups` sg ON sg.group_id = asg.srv_group_id
												WHERE ((asg.server_id != '-1' AND asg.srv_group_id = '-1')
												OR (asg.srv_group_id != '-1' AND asg.server_id = '-1'))
												AND (s.sid IN(asg.server_id) OR s.sid IN(sg.server_id)) AND s.enabled = 1");
            $allservers = array();
            foreach ($serveraccessq as $access) {
                if (!in_array($access['sid'], $allservers)) {
                    $allservers[] = $access['sid'];
                }
            }
            $rehashing = true;
        }

        $GLOBALS['db']->Execute(sprintf("DELETE FROM `%s_admins_servers_groups` WHERE admin_id = %d", DB_PREFIX, $aid));
    }

    $query = $GLOBALS['db']->GetRow("SELECT count(aid) AS cnt FROM `" . DB_PREFIX . "_admins`");
    $objResponse->addScript("SlideUp('aid_{$aid}');");
    $objResponse->addScript("$('admincount').setHTML('" . $query['cnt'] . "');");
    if ($delquery) {
        if (isset($rehashing)) {
            $objResponse->addScript("ShowRehashBox('" . implode(",", $allservers) . "', '".t('sb-callback', 'Admin Deleted')."', '".t('sb-callback', 'The selected admin has been deleted from the database')."', 'green', 'index.php?p=admin&c=admins', true);");
        } else {
            $objResponse->addScript("ShowBox('".t('sb-callback', 'Admin Deleted')."', '".t('sb-callback', 'The selected admin has been deleted from the database')."', 'green', 'index.php?p=admin&c=admins', true);");
        }

        new CSystemLog(
            'm',
            t('log', 'Admin Deleted'),
            t('log', 'Admin ([[admin]]) has been deleted', array(
                '[[admin]]' => $gid['user'],
            ))
        );
        
    } else {
        $objResponse->addScript("ShowBox('".t('sb-callback', 'Error')."', '".t('sb-callback', 'There was an error removing the admin from the database, please check the logs')."', 'red', 'index.php?p=admin&c=admins', true);");
    }
    return $objResponse;
}

function UpdateGroupPermissions($gid) {
    $objResponse = new xajaxResponse();
    global $userbank;
    $gid = (int) $gid;
    if ($gid == 1) {
        ob_start();
        $title = t('webperms', 'Web Permissions');
        include TEMPLATES_PATH . "groups.web.perm.php";
        $permissions = ob_get_clean();
    } elseif ($gid == 2) {
        ob_start();
        $title = t('srvperms', 'Server Permissions');
        include TEMPLATES_PATH . "groups.server.perm.php";
        $permissions = ob_get_clean();
    } elseif ($gid == 3) {
        $permissions = "";
    }

    $objResponse->addAssign("perms", "innerHTML", $permissions);
    if (!$userbank->HasAccess(ADMIN_OWNER)) {
        $objResponse->addScript('if($("wrootcheckbox")) { 
									$("wrootcheckbox").setStyle("display", "none");
								}
								if($("srootcheckbox")) { 
									$("srootcheckbox").setStyle("display", "none");
								}');
    }

    $objResponse->addScript("$('type.msg').setHTML('');");
    $objResponse->addScript("$('type.msg').setStyle('display', 'none');");
    return $objResponse;
}

function UpdateAdminPermissions($type, $value) {
    $objResponse = new xajaxResponse();
    global $userbank;
    $type = (int) $type;
    if ($type == 1) {
        $id = "web";
        if ($value == "c") {
            ob_start();
            $title = t('webperms', 'Web Permissions');
            include TEMPLATES_PATH . "groups.web.perm.php";
            $permissions = ob_get_clean();
        } elseif ($value == "n") {
            ob_start();
            $name = "webname";
            $title = t('webperms', 'New Group Permissions');
            
            include TEMPLATES_PATH . "group.name.php";
            include TEMPLATES_PATH . "groups.web.perm.php";
            $permissions = ob_get_clean();
        } else {
            $permissions = "";
        }
    }
    if ($type == 2) {
        $id = "server";
        if ($value == "c") {
            ob_start();
            $title = t('srvperms', 'Server Permissions');
            include TEMPLATES_PATH . "groups.server.perm.php";
            $permissions = ob_get_clean();
        } elseif ($value == "n") {
            ob_start();
            $name = "webname";
            $title = t('srvperms', 'New Group Permissions');
            
            include TEMPLATES_PATH . "group.name.php";
            include TEMPLATES_PATH . "groups.web.perm.php";
            $permissions = ob_get_clean();
        } else {
            $permissions = "";
        }
    }

    $objResponse->addAssign($id . "perm", "innerHTML", $permissions);
    if (!$userbank->HasAccess(ADMIN_OWNER)) {
        $objResponse->addScript('if($("wrootcheckbox")) { 
									$("wrootcheckbox").setStyle("display", "none");
								}
								if($("srootcheckbox")) { 
									$("srootcheckbox").setStyle("display", "none");
								}');
    }
    $objResponse->addAssign($id . ".msg", "innerHTML", "");
    return $objResponse;
}

function AddServerGroupName() {
    $objResponse = new xajaxResponse();
    global $userbank, $username;
    if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_EDIT_GROUPS)) {
        $objResponse->redirect("index.php?p=login&n=no_access", 0);

        new CSystemLog(
            'w',
            t('log', 'Hacking Attempt'),
            t('log', '[[username]] tried to edit group name, but doesnt have access.', array(
                '[[username]]' => $username,
            ))
        );
        
        return $objResponse;
    }
    $inject = '<td valign="top"><div class="rowdesc">' 
        . HelpIcon(
            t('sb-callback', 'Server Group Name'),
            t('sb-callback', 'Please type the name of the new group you wish to create.')
        )
        . t('sb-callback', 'Group Name') .' </div></td>';
    $inject .= '<td><div align="left">
        <input type="text" style="border: 1px solid #000000; width: 105px; font-size: 14px; background-color: rgb(215, 215, 215);width: 200px;" id="sgroup" name="sgroup" />
      </div>
        <div id="group_name.msg" style="color:#CC0000;width:195px;display:none;"></div></td>
  ';
    $objResponse->addAssign("nsgroup", "innerHTML", $inject);
    $objResponse->addAssign("group.msg", "innerHTML", "");
    return $objResponse;
}

function AddAdmin(
    $mask,
    $srv_mask,
    $a_name,
    $a_steam,
    $a_email,
    $a_password,
    $a_password2,
    $a_sg, $a_wg,
    $a_serverpass,
    $a_webname,
    $a_servername,
    $server,
    $singlesrv
)
{
    $objResponse = new xajaxResponse();
    global $userbank, $username;
    if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_ADD_ADMINS)) {
        $objResponse->redirect("index.php?p=login&n=no_access", 0);

        new CSystemLog(
            'w',
            t('log', 'Hacking Attempt'),
            t('log', '[[username]] tried to add an admin, but doesnt have access.', array(
                '[[username]]' => $username,
            ))
        );
        
        return $objResponse;
    }
    $a_name = RemoveCode($a_name);
    $a_steam = RemoveCode($a_steam);
    $a_email = RemoveCode($a_email);
    $a_servername = ($a_servername == "0" ? null : RemoveCode($a_servername));
    $a_webname = RemoveCode($a_webname);
    $mask = (int) $mask;

    $error = 0;

    //No name
    if (empty($a_name)) {
        $error++;
        $objResponse->addAssign("name.msg", "innerHTML", t('sb-callback', 'You must type a name for the admin.'));
        $objResponse->addScript("$('name.msg').setStyle('display', 'block');");
    } else {
        if (strstr($a_name, "'")) {
            $error++;
            $objResponse->addAssign("name.msg", "innerHTML", t('sb-callback', 'An admin name can not contain a " \' ".'));
            $objResponse->addScript("$('name.msg').setStyle('display', 'block');");
        } else {
            if (is_taken("admins", "user", $a_name)) {
                $error++;
                $objResponse->addAssign("name.msg", "innerHTML", t('sb-callback', 'An admin with this name already exists'));
                $objResponse->addScript("$('name.msg').setStyle('display', 'block');");
            } else {
                $objResponse->addAssign("name.msg", "innerHTML", "");
                $objResponse->addScript("$('name.msg').setStyle('display', 'none');");
            }
        }
    }
    // If they didnt type a steamid
    if ((empty($a_steam) || strlen($a_steam) < 10)) {
        $error++;
        $objResponse->addAssign("steam.msg", "innerHTML", t('sb-callback', 'You must type a Steam ID or Community ID for the admin.'));
        $objResponse->addScript("$('steam.msg').setStyle('display', 'block');");
    } else {
        // Validate the steamid or fetch it from the community id
        if (
                (!is_numeric($a_steam) && !validate_steam($a_steam))
                    ||
                (
                    is_numeric($a_steam)
                        &&
                    (
                        strlen($a_steam) < 15
                            ||
                        !validate_steam($a_steam = FriendIDToSteamID($a_steam))
                    )
                )
            ) {
            $error++;
            $objResponse->addAssign("steam.msg", "innerHTML", t('sb-callback', 'Please enter a valid Steam ID or Community ID.'));
            $objResponse->addScript("$('steam.msg').setStyle('display', 'block');");
        } else {
            if (is_taken("admins", "authid", $a_steam)) {
                $admins = $userbank->GetAllAdmins();
                foreach ($admins as $admin) {
                    if ($admin['authid'] == $a_steam) {
                        $name = $admin['user'];
                        break;
                    }
                }
                $error++;
                $objResponse->addAssign(
                    "steam.msg",
                    "innerHTML",
                    t('sb-callback', 'Admin [[admin]] already uses this Steam ID.', array(
                        '[[admin]]' => htmlspecialchars(addslashes($name))
                    ))
                );
                $objResponse->addScript("$('steam.msg').setStyle('display', 'block');");
            } else {
                $objResponse->addAssign("steam.msg", "innerHTML", "");
                $objResponse->addScript("$('steam.msg').setStyle('display', 'none');");
            }
        }
    }

    // No email
    if (empty($a_email)) {
        // An E-Mail address is only required for users with web permissions.
        if ($mask != 0) {
            $error++;
            $objResponse->addAssign("email.msg", "innerHTML", t('sb-callback', 'You must type an e-mail address.'));
            $objResponse->addScript("$('email.msg').setStyle('display', 'block');");
        }
    } else {
        // Is an other admin already registred with that email address?
        if (is_taken("admins", "email", $a_email)) {
            $admins = $userbank->GetAllAdmins();
            foreach ($admins as $admin) {
                if ($admin['email'] == $a_email) {
                    $name = $admin['user'];
                    break;
                }
            }
            $error++;
            $objResponse->addAssign(
                "email.msg",
                "innerHTML",
                t('sb-callback', 'This email address is already being used by [[admin]]', array(
                    '[[admin]]' => htmlspecialchars(addslashes($name))
                ))
            );
            $objResponse->addScript("$('email.msg').setStyle('display', 'block');");
        } else {
            $objResponse->addAssign("email.msg", "innerHTML", "");
            $objResponse->addScript("$('email.msg').setStyle('display', 'none');");
            /* 	if(!validate_email($a_email))
              {
              $error++;
              $objResponse->addAssign("email.msg", "innerHTML", "Please enter a valid email address.");
              $objResponse->addScript("$('email.msg').setStyle('display', 'block');");
              }
              else
              {
              $objResponse->addAssign("email.msg", "innerHTML", "");
              $objResponse->addScript("$('email.msg').setStyle('display', 'none');");

              } */
        }
    }

    // no pass
    if (empty($a_password)) {
        // A password is only required for users with web permissions.
        if ($mask != 0) {
            $error++;
            $objResponse->addAssign("password.msg", "innerHTML", t('sb-callback', 'You must type a password.'));
            $objResponse->addScript("$('password.msg').setStyle('display', 'block');");
        }
    }
    // Password too short?
    else if (strlen($a_password) < MIN_PASS_LENGTH) {
        $error++;
        $objResponse->addAssign(
            "password.msg",
            "innerHTML",
            t('sb-callback', 'Your password must be at-least [[minpass]] characters long.', array(
                '[[minpass]]' => MIN_PASS_LENGTH
            ))
        );
        $objResponse->addScript("$('password.msg').setStyle('display', 'block');");
    } else {
        $objResponse->addAssign("password.msg", "innerHTML", "");
        $objResponse->addScript("$('password.msg').setStyle('display', 'none');");

        // No confirmation typed
        if (empty($a_password2)) {
            $error++;
            $objResponse->addAssign("password2.msg", "innerHTML", t('sb-callback', 'You must confirm the password'));
            $objResponse->addScript("$('password2.msg').setStyle('display', 'block');");
        }
        // Passwords match?
        else if ($a_password != $a_password2) {
            $error++;
            $objResponse->addAssign("password2.msg", "innerHTML", t('sb-callback', 'Your passwords don\'t match'));
            $objResponse->addScript("$('password2.msg').setStyle('display', 'block');");
        } else {
            $objResponse->addAssign("password2.msg", "innerHTML", "");
            $objResponse->addScript("$('password2.msg').setStyle('display', 'none');");
        }
    }

    // Choose to use a server password
    if ($a_serverpass != "-1") {
        // No password given?
        if (empty($a_serverpass)) {
            $error++;
            $objResponse->addAssign("a_serverpass.msg", "innerHTML", t('sb-callback', 'You must type a server password or uncheck the box.'));
            $objResponse->addScript("$('a_serverpass.msg').setStyle('display', 'block');");
        }
        // Password too short?
        else if (strlen($a_serverpass) < MIN_PASS_LENGTH) {
            $error++;
            $objResponse->addAssign(
                "a_serverpass.msg",
                "innerHTML",
                t('sb-callback', 'Your password must be at-least [[minpass]] characters long.', array(
                    '[[minpass]]' => MIN_PASS_LENGTH
                ))
            );
            $objResponse->addScript("$('a_serverpass.msg').setStyle('display', 'block');");
        } else {
            $objResponse->addAssign("a_serverpass.msg", "innerHTML", "");
            $objResponse->addScript("$('a_serverpass.msg').setStyle('display', 'none');");
        }
    } else {
        $objResponse->addAssign("a_serverpass.msg", "innerHTML", "");
        $objResponse->addScript("$('a_serverpass.msg').setStyle('display', 'none');");
        // Don't set "-1" as password ;)
        $a_serverpass = "";
    }

    // didn't choose a server group
    if ($a_sg == "-2") {
        $error++;
        $objResponse->addAssign("server.msg", "innerHTML", t('sb-callback', 'You must choose a group.'));
        $objResponse->addScript("$('server.msg').setStyle('display', 'block');");
    } else {
        $objResponse->addAssign("server.msg", "innerHTML", "");
        $objResponse->addScript("$('server.msg').setStyle('display', 'none');");
    }

    // chose to create a new server group
    if ($a_sg == 'n') {
        // didn't type a name
        if (empty($a_servername)) {
            $error++;
            $objResponse->addAssign("servername_err", "innerHTML", t('sb-callback', 'You need to type a name for the new group.'));
            $objResponse->addScript("$('servername_err').setStyle('display', 'block');");
        }
        // Group names can't contain ,
        else if (strstr($a_servername, ',')) {
            $error++;
            $objResponse->addAssign("servername_err", "innerHTML", t('sb-callback', 'Group name cannot contain a \',\''));
            $objResponse->addScript("$('servername_err').setStyle('display', 'block');");
        } else {
            $objResponse->addAssign("servername_err", "innerHTML", "");
            $objResponse->addScript("$('servername_err').setStyle('display', 'none');");
        }
    }

    // didn't choose a web group
    if ($a_wg == "-2") {
        $error++;
        $objResponse->addAssign("web.msg", "innerHTML", t('sb-callback', 'You must choose a group.'));
        $objResponse->addScript("$('web.msg').setStyle('display', 'block');");
    } else {
        $objResponse->addAssign("web.msg", "innerHTML", "");
        $objResponse->addScript("$('web.msg').setStyle('display', 'none');");
    }

    // Choose to create a new webgroup
    if ($a_wg == 'n') {
        // But didn't type a name
        if (empty($a_webname)) {
            $error++;
            $objResponse->addAssign("webname_err", "innerHTML", t('sb-callback', 'You need to type a name for the new group.'));
            $objResponse->addScript("$('webname_err').setStyle('display', 'block');");
        }
        // Group names can't contain ,
        else if (strstr($a_webname, ',')) {
            $error++;
            $objResponse->addAssign("webname_err", "innerHTML", t('sb-callback', 'Group name cannot contain a \',\''));
            $objResponse->addScript("$('webname_err').setStyle('display', 'block');");
        } else {
            $objResponse->addAssign("webname_err", "innerHTML", "");
            $objResponse->addScript("$('webname_err').setStyle('display', 'none');");
        }
    }

    // Ohnoes! something went wrong, stop and show errs
    if ($error) {
        ShowBox_ajx(
            t('sb-callback', 'Error'),
            t('sb-callback', 'There are some errors in your input. Please correct them.'),
            "red",
            "",
            true,
            $objResponse
        );
        return $objResponse;
    }

    // ##############################################################
    // ##                     Start adding to DB                   ##
    // ##############################################################

    $immunity = 0;

    // Extract immunity from server mask string
    if (strstr($srv_mask, "#")) {
        $immunity = "0";
        $immunity = substr($srv_mask, strpos($srv_mask, "#") + 1);
        $srv_mask = substr($srv_mask, 0, strlen($srv_mask) - strlen($immunity) - 1);
    }

    // Avoid negative immunity
    $immunity = ($immunity > 0) ? $immunity : 0;

    // Handle Webpermissions
    // Chose to create a new webgroup
    if ($a_wg == 'n') {
        $GLOBALS['db']->Execute("INSERT INTO " . DB_PREFIX . "_groups(type, name, flags) VALUES (?,?,?)", array(
            1,
            $a_webname,
            $mask
        ));
        $web_group = (int) $GLOBALS['db']->Insert_ID();

        // We added those permissons to the group, so don't add them as custom permissions again
        $mask = 0;
    } else if ($a_wg != 'c' && $a_wg > 0) {
        // Chose an existing group
        $web_group = (int) $a_wg;
    } else {
        // Custom permissions -> no group
        $web_group = -1;
    }

    // Handle Serverpermissions
    // Chose to create a new server admin group
    if ($a_sg == 'n') {
        $GLOBALS['db']->Execute(
            "INSERT INTO " . DB_PREFIX . "_srvgroups(immunity, flags, name, groups_immune) VALUES (?,?,?,?)",
            array($immunity, $srv_mask, $a_servername, " ")
        );

        $server_admin_group = $a_servername;
        $server_admin_group_int = (int) $GLOBALS['db']->Insert_ID();

        // We added those permissons to the group, so don't add them as custom permissions again
        $srv_mask = "";
    } else if ($a_sg != 'c' && $a_sg > 0) {
        // Chose an existing group
        $server_admin_group = $GLOBALS['db']->GetOne("SELECT `name` FROM " . DB_PREFIX . "_srvgroups WHERE id = '" . (int) $a_sg . "'");
        $server_admin_group_int = (int) $a_sg;
    } else {
        // Custom permissions -> no group
        $server_admin_group = "";
        $server_admin_group_int = -1;
    }

    // Add the admin
    $aid = $userbank->AddAdmin(
        $a_name,
        $a_steam,
        $a_password,
        $a_email,
        $web_group,
        $mask,
        $server_admin_group,
        $srv_mask,
        $immunity,
        $a_serverpass
    );

    if ($aid > -1) {
        // Grant permissions to the selected server groups
        $srv_groups = explode(",", $server);
        $addtosrvgrp = $GLOBALS['db']->Prepare("INSERT INTO " . DB_PREFIX . "_admins_servers_groups(admin_id,group_id,srv_group_id,server_id) VALUES (?,?,?,?)");
        foreach ($srv_groups AS $srv_group) {
            if (!empty($srv_group)) {
                $GLOBALS['db']->Execute($addtosrvgrp, array($aid, $server_admin_group_int, substr($srv_group, 1), '-1'));
            }
        }

        // Grant permissions to individual servers
        $srv_arr = explode(",", $singlesrv);
        $addtosrv = $GLOBALS['db']->Prepare("INSERT INTO " . DB_PREFIX . "_admins_servers_groups(admin_id,group_id,srv_group_id,server_id) VALUES (?,?,?,?)");
        foreach ($srv_arr AS $server) {
            if (!empty($server)) {
                $GLOBALS['db']->Execute($addtosrv, array($aid, $server_admin_group_int, '-1', substr($server, 1)));
            }
        }
        if (isset($GLOBALS['config']['config.enableadminrehashing']) && $GLOBALS['config']['config.enableadminrehashing'] == 1) {
            // rehash the admins on the servers
            $serveraccessq = $GLOBALS['db']->GetAll("SELECT s.sid FROM `" . DB_PREFIX . "_servers` s
												LEFT JOIN `" . DB_PREFIX . "_admins_servers_groups` asg ON asg.admin_id = '" . (int) $aid . "'
												LEFT JOIN `" . DB_PREFIX . "_servers_groups` sg ON sg.group_id = asg.srv_group_id
												WHERE ((asg.server_id != '-1' AND asg.srv_group_id = '-1')
												OR (asg.srv_group_id != '-1' AND asg.server_id = '-1'))
												AND (s.sid IN(asg.server_id) OR s.sid IN(sg.server_id)) AND s.enabled = 1");
            $allservers = array();
            foreach ($serveraccessq as $access) {
                if (!in_array($access['sid'], $allservers)) {
                    $allservers[] = $access['sid'];
                }
            }
            $objResponse->addScript("ShowRehashBox('" . implode(",", $allservers) . "','".t('sb-callback', 'Admin Added')."', '".t('sb-callback', 'The admin has been added successfully')."', 'green', 'index.php?p=admin&c=admins');TabToReload();");
        } else {
            $objResponse->addScript("ShowBox('".t('sb-callback', 'Admin Added')."', '".t('sb-callback', 'The admin has been added successfully')."', 'green', 'index.php?p=admin&c=admins');TabToReload();");
        }

        new CSystemLog(
            'm',
            t('log', 'Admin added'),
            t('log', 'Admin ([[admin]]) has been added', array(
                '[[admin]]' => $a_name
            ))
        );
        
        return $objResponse;
    }
    else {
        $objResponse->addScript("ShowBox('".t('sb-callback', 'User NOT Added')."', '".t('sb-callback', 'The admin failed to be added to the database. Check the logs for any SQL errors.')."', 'red', 'index.php?p=admin&c=admins');");
    }
}

function ServerHostPlayers($sid, $type = "servers", $obId = "", $tplsid = "", $open = "", $inHome = false, $trunchostname = 48) {
    $objResponse = new xajaxResponse();

    global $userbank;

    $sid = (int) $sid;

    $res = $GLOBALS['db']->GetRow("SELECT sid, ip, port FROM " . DB_PREFIX . "_servers WHERE sid = $sid");
    if (empty($res[1]) || empty($res[2])) {
        return $objResponse;
    }
    $info = array();
    $query = QueryInstance($res[1], $res[2]);
    $connected = true;
    if($query === false) {
        $connected = false;
    }
    
    $info = $query->GetInfo();
    $players = $query->GetPlayers();
    if(!$players) {
        $players = array();
    }
    if(!$info) {
        $connected = false;
    }
    
    if ($type == "servers") {
        if ($connected) {
            $objResponse->addAssign("host_{$sid}", "innerHTML", trunc($info['HostName'], $trunchostname, false));
            $objResponse->addAssign("players_{$sid}", "innerHTML", $info['Players'] . "/" . $info['MaxPlayers']);
            $objResponse->addAssign("os_{$sid}", "innerHTML", "<img src='images/" . (!empty($info['Os']) ? $info['Os'] : 'server_small') . ".png'>");
            if ($info['Secure']) {
                $objResponse->addAssign("vac_{$sid}", "innerHTML", "<img src='images/shield.png'>");
            }
            $objResponse->addAssign("map_{$sid}", "innerHTML", basename($info['Map'])); // Strip Steam Workshop folder
            if (!$inHome) {
                $objResponse->addScript("$('mapimg_{$sid}').setProperty('src', '" . GetMapImage($info['Map']) . "').setProperty('alt', '" . $info['Map'] . "').setProperty('title', '" . basename($info['Map']) . "');");
                if ($info['Players'] == 0 || empty($info['Players'])) {
                    $objResponse->addScript("$('sinfo_{$sid}').setStyle('display', 'none');");
                    $objResponse->addScript("$('noplayer_{$sid}').setStyle('display', 'block');");
                    $objResponse->addScript("$('serverwindow_{$sid}').setStyle('height', '64px');");
                } else {
                    $objResponse->addScript("$('sinfo_{$sid}').setStyle('display', 'block');");
                    $objResponse->addScript("$('noplayer_{$sid}').setStyle('display', 'none');");
                    if (!defined('IN_HOME')) {
                        // remove childnodes
                        $objResponse->addScript('var toempty = document.getElementById("playerlist_' . $sid . '");
						var empty = toempty.cloneNode(false);
						toempty.parentNode.replaceChild(empty,toempty);');
                        //draw table headlines
                        $objResponse->addScript('var e = document.getElementById("playerlist_' . $sid . '");
						var tr = e.insertRow("-1");
							// Name Top TD
							var td = tr.insertCell("-1");
								td.setAttribute("width","45%");
								td.setAttribute("height","16");
								td.className = "listtable_top";
									var b = document.createElement("b");
									var txt = document.createTextNode("'.t('servers', 'Name').'");
									b.appendChild(txt);
								td.appendChild(b);
							// Score Top TD
							var td = tr.insertCell("-1");
								td.setAttribute("width","10%");
								td.setAttribute("height","16");
								td.className = "listtable_top";
									var b = document.createElement("b");
									var txt = document.createTextNode("'.t('servers', 'Score').'");
									b.appendChild(txt);
								td.appendChild(b);
							// Time Top TD
							var td = tr.insertCell("-1");
								td.setAttribute("height","16");
								td.className = "listtable_top";
									var b = document.createElement("b");
									var txt = document.createTextNode("'.t('servers', 'Time').'");
									b.appendChild(txt);
								td.appendChild(b);');
                        // add players
                        $playercount = 0;
                        foreach ($players AS $pid => $player) {
                            $player["name"] = str_replace('"', '\"', $player['Name']);
                            if(!$player['name']) {
                                continue;
                            }
                            $objResponse->addScript('var e = document.getElementById("playerlist_' . $sid . '");
                                var tr = e.insertRow("-1");
                                tr.className="tbl_out";
                                tr.onmouseout = function(){this.className="tbl_out"};
                                tr.onmouseover = function(){this.className="tbl_hover"};
                                tr.id = "player_s' . $sid . 'p' . $pid . '";
                                    // Name TD
                                    var td = tr.insertCell("-1");
                                        td.className = "listtable_1";
                                        var txt = document.createTextNode("' . $player["name"] . '");
                                        td.appendChild(txt);
                                    // Score TD
                                    var td = tr.insertCell("-1");
                                        td.className = "listtable_1";
                                        var txt = document.createTextNode("' . $player["Frags"] . '");
                                        td.appendChild(txt);
                                    // Time TD
                                    var td = tr.insertCell("-1");
                                        td.className = "listtable_1";
                                        var txt = document.createTextNode("' . SecondsToString(intval($player["Time"])) . '");
                                        td.appendChild(txt);
														');
                            if ($userbank->HasAccess(ADMIN_OWNER | ADMIN_ADD_BAN)) {
                                
                                $js = '
AddContextMenu(
    "#player_s'.$sid.'p'.$pid.'",
    "contextmenu",
    true,
    "'.t('servers', 'Player Commands').'",
    [
        {
            name: "'.t('servers', 'Kick').'",
            callback: function(){
                if(confirm("'.t('servers', 'Are you sure you want to kick player [[player]]?', array('[[player]]' => $player['name'])).'")) {
                    xajax_KickPlayer('.$sid.', "'.$player['name'].'");
                }
            }
        },
        {
            name: "'.t('servers', 'Ban').'",
            callback: function(){
                window.location = "index.php?m=bans&p=admin&action=pasteBan&sid='.$sid.'&pName='.$player['name'].'";
            }
        },
        {separator: true},
        {
            name: "'.t('servers', 'View Profile').'",
            callback: function(){
                ShowBox(
                    "'.t('servers', 'View Community Profile').'",
                    "'.t('servers', 'Generating Community Profile link for [[player], please wait...', array('[[player]]' => $player['name'])).'",
                    "blue",
                    "",
                    true
                );
                $("dialog-control").setStyle("display", "none");
                xajax_ViewCommunityProfile('.$sid.', "'.$player['name'].'");
            }
        },
        {
            name: "'.t('servers', 'Send Message').'",
            callback: function(){
                OpenMessageBox('.$sid.', "'.$player['name'].'", 1);
            }
        }
    ]
);
';
                                $objResponse->addScript($js);
                            }
                            $playercount++;
                        }
                    }
                    if ($playercount > 15) {
                        $height = 329 + 16 * ($playercount - 15) + 4 * ($playercount - 15) . "px";
                    } else {
                        $height = 329 . "px";
                    }
                    $objResponse->addScript("$('serverwindow_{$sid}').setStyle('height', '" . $height . "');");
                }
            }
        }else {
            if ($userbank->HasAccess(ADMIN_OWNER)) {
                $objResponse->addAssign(
                    "host_{$sid}",
                    "innerHTML",
                    "<b>".t('servers', 'Error connecting')."</b> (<i>" 
                            . $res[1] 
                            . ":" 
                            . $res[2] 
                            . "</i>) <small>"
                            . "<a href=\"http://sourcebans.net/node/25\" "
                                . "title=\""
                                .t('servers', 'Which ports does the SourceBans webpanel require to be open?')
                                ."\">".t('servers', 'Help')."</a></small>"
                );
            } else {
                $objResponse->addAssign("host_{$sid}", "innerHTML", "<b>".t('servers', 'Error connecting')."</b> (<i>" . $res[1] . ":" . $res[2] . "</i>)");
            }
            $objResponse->addAssign("players_{$sid}", "innerHTML", "N/A");
            $objResponse->addAssign("os_{$sid}", "innerHTML", "N/A");
            $objResponse->addAssign("vac_{$sid}", "innerHTML", "N/A");
            $objResponse->addAssign("map_{$sid}", "innerHTML", "N/A");
            if (!$inHome) {
                //$connect = "onclick = \"document.location = 'steam://connect/" . $res['ip'] . ":" . $res['port'] . "'\"";
                $objResponse->addScript("$('sinfo_{$sid}').setStyle('display', 'none');");
                $objResponse->addScript("$('noplayer_{$sid}').setStyle('display', 'block');");
                $objResponse->addScript("$('serverwindow_{$sid}').setStyle('height', '64px');");
                $objResponse->addScript("if($('sid_{$sid}'))$('sid_{$sid}').setStyle('color', '#adadad');");
            }
        }
        if ($tplsid != "" && $open != "" && $tplsid == $open) {
            $objResponse->addScript("InitAccordion('tr.opener', 'div.opener', 'mainwrapper', '" . $open . "');");
        }
        $objResponse->addScript("$('dialog-control').setStyle('display', 'block');");
        $objResponse->addScript("$('dialog-placement').setStyle('display', 'none');");
    } elseif ($type == "id") {
        if ($connected) {
            $objResponse->addAssign($obId, "innerHTML", trunc($info['HostName'], $trunchostname, false));
        } else {
            $objResponse->addAssign($obId, "innerHTML", "<b>".t('servers', 'Error connecting')."</b> (<i>" . $res[1] . ":" . $res[2] . "</i>)");
        }
    } else {
        if ($connected) {
            $objResponse->addAssign("ban_server_{$type}", "innerHTML", trunc($info['HostName'], $trunchostname, false));
        } else {
            $objResponse->addAssign("ban_server_{$type}", "innerHTML", "<b>".t('servers', 'Error connecting')."</b> (<i>" . $res[1] . ":" . $res[2] . "</i>)");
        }
    }
    return $objResponse;
}

function ServerHostProperty($sid, $obId, $obProp, $trunchostname) {
    $objResponse = new xajaxResponse();

    $sid = (int) $sid;
    $obId = htmlspecialchars($obId);
    $obProp = htmlspecialchars($obProp);
    $trunchostname = (int) $trunchostname;

    $res = $GLOBALS['db']->GetRow("SELECT ip, port FROM " . DB_PREFIX . "_servers WHERE sid = $sid");
    if (empty($res[0]) || empty($res[1])) {
        return $objResponse;
    }
    $info = array();
    
    $query = QueryInstance($res[0], $res[1]);
    if($query === false) {
        return $objResponse;
    }
    
    $info = $query->GetInfo();

    if (!empty($info['HostName'])) {
        $objResponse->addAssign("$obId", "$obProp", addslashes(trunc($info['hostname'], $trunchostname, false)));
    } else {
        $objResponse->addAssign("$obId", "$obProp", t('servers', 'Error connecting')." (" . $res[0] . ":" . $res[1] . ")");
    }
    return $objResponse;
}

function ServerHostPlayers_list($sid, $type = "servers", $obId = "") {
    $objResponse = new xajaxResponse();

    $sids = explode(";", $sid, -1);
    if (count($sids) < 1) {
        return $objResponse;
    }

    $ret = "";
    for ($i = 0; $i < count($sids); $i++) {
        $sid = (int) $sids[$i];

        $res = $GLOBALS['db']->GetRow("SELECT sid, ip, port FROM " . DB_PREFIX . "_servers WHERE sid = $sid");
        if (empty($res[1]) || empty($res[2])) {
            return $objResponse;
        }
        $info = array();
        
        $query = QueryInstance($res[1], $res[2]);
        if($query === false) {
            return $objResponse;
        }
        $info = $query->GetInfo();

        if (!empty($info['HostName'])) {
            $ret .= trunc($info['HostName'], 48, false) . "<br />";
        } else {
            $ret .= "<b>".t('servers', 'Error connecting')."</b> (<i>" . $res[1] . ":" . $res[2] . "</i>)<br />";
        }
    }

    if ($type == "id") {
        $objResponse->addAssign("$obId", "innerHTML", $ret);
    } else {
        $objResponse->addAssign("ban_server_{$type}", "innerHTML", $ret);
    }

    return $objResponse;
}

function ServerPlayers($sid) {
    $objResponse = new xajaxResponse();
    $sid = (int) $sid;

    $res = $GLOBALS['db']->GetRow("SELECT sid, ip, port FROM " . DB_PREFIX . "_servers WHERE sid = $sid");
    if (empty($res[1]) || empty($res[2])) {
        $objResponse->addAlert('IP or Port not set :o');
        return $objResponse;
    }
    $info = array();

    $query = QueryInstance($res[1], $res[2]);
    
    if($query === false) {
        $objResponse->addAlert(t('servers', 'Can not connect to server. See system log for details'));
        return $objResponse;
    }

    $info = $query->GetPlayers();
    if(!$info){
        $objResponse->addAlert(t('servers', 'Can not connect to server. See system log for details'));
        return $objResponse;
    }
    
    $html = "";
    foreach ($info AS $player) {
        $html .= '<tr> <td class="listtable_1">' . htmlentities($player['Name']) . '</td>
						<td class="listtable_1">' . (int) $player['Frags'] . '</td>
						<td class="listtable_1">' . $player['TimeF'] . '</td>
				  </tr>';
    }
    $objResponse->addAssign("player_detail_{$sid}", "innerHTML", $html);
    //$objResponse->addScript("document.getElementById('player_detail_$sid').innerHTML = 'hi';");
    $objResponse->addScript("setTimeout('xajax_ServerPlayers({$sid})', 5000);");
    $objResponse->addScript("$('opener_$sid').setProperty('onclick', '');");
    return $objResponse;
}

function KickPlayer($sid, $name) {
    $objResponse = new xajaxResponse();
    global $userbank, $username;
    $sid = (int) $sid;

    $objResponse->addScript("$('dialog-control').setStyle('display', 'block');");

    if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_ADD_BAN)) {
        $objResponse->redirect("index.php?p=login&n=no_access", 0);

        new CSystemLog(
            'w',
            t('log', 'Hacking Attempt'),
            t('log', '[[username]] tried to kick [[player]], but doesnt have access.', array(
                '[[username]]' => $username,
                '[[player]]' => RemoveCode($name)
            ))
        );
        
        return $objResponse;
    }
    
    //get the server data
    $data = $GLOBALS['db']->CacheGetRow(86400, 
        "SELECT
            `srv`.`ip` `ip`,
            `srv`.`port` `port`,
            `srv`.`rcon` `rcon`,
            `mod`.`modfolder` `game`
        FROM
            `".DB_PREFIX."_servers` `srv`
        LEFT JOIN
            `".DB_PREFIX."_mods` `mod`
        ON
            `mod`.`mid` = `srv`.`modid`
        WHERE `srv`.`sid` = '".intval($sid)."';");
    if (empty($data['rcon'])) {
        $msg = t(
            'servers',
            'Can not kick [[player]]. No RCON password!',
            array(
                '[[player]]' => RemoveCode($name)
            )
        );
        $objResponse->addScript("ShowBox('".t('sb-callback', 'Error')."', '{$msg}', 'red', '', true);");
        return $objResponse;
    }

    require_once INCLUDES_PATH . 'SourceQuery'.DIRECTORY_SEPARATOR.'SourceQuery.class.php';
    $query = new SourceQuery;
    try {
        $query->Connect($data['ip'], $data['port'], 1);
        $query->SetRconPassword($data['rcon']);
    } catch(Exception $e) {
        new CSystemLog(
            'e',
            t('log', 'Can not connect to server'),
            t('log', 'Can not connect to server. Error: [[errormessage]]', array(
                '[[errormessage]]' => $e->getMessage()
            ))
        );
         $objResponse->addScript("ShowBox('"
             .t('servers', 'Error')
             ."', '"
             .t('servers', 'Can not connect to server. See system log for details')
             ."', 'red', '', true);"
        );
        return $objResponse;
    }
    
    $status = $query->Rcon('status');
    
    $player = false;
    
    $players = getPlayersFromStatus($status, $data['game']);
    
    foreach($players as $p) {
        if($p['name'] == $name) {
            $player = $p;
            break;
        }
    }

    if ($player) {
        $steam = $player['steamid'];
        // check for immunity
        $admin = $GLOBALS['db']->GetRow(
            "SELECT
                a.immunity AS pimmune,
                g.immunity AS gimmune
            FROM
                `" . DB_PREFIX . "_admins` AS a
            LEFT JOIN
                `" . DB_PREFIX . "_srvgroups` AS g
            ON
                g.name = a.srv_group
            WHERE
                authid = '" . $steam . "'
            LIMIT 1;"
        );
        if ($admin && $admin['gimmune'] > $admin['pimmune']) {
            $immune = $admin['gimmune'];
        } elseif ($admin) {
            $immune = $admin['pimmune'];
        } else {
            $immune = 0;
        }

        if ($immune <= $userbank->GetProperty('srv_immunity')) {
            $requri = substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], ".php") + 4);
            $kickMsg = t('servers', 'You have been kicked by this server, check [[infolink]] for more info.', array(
                '[[infolink]]' => 'http://' . $_SERVER['HTTP_HOST'] . $requri
            ));
            
            if(steamidVersion($steam) == 3) {
                $steam = '"'.$steam.'"';
            }
            $query->Rcon('kickid '.$steam.' "'.$kickMsg.'"');
            
            new CSystemLog(
                'm',
                t('log', 'Player kicked'),
                t('log', '[[username]] kicked player [[player]] ([[steam]]) from [[server]].', array(
                    '[[username]]' => $username,
                    '[[player]]' => RemoveCode($name),
                    '[[steam]]' => $steam,
                    '[[server]]' => $data['ip'] . ":" . $data['port']
                ))
            );

            $objResponse->addScript("ShowBox('".t('servers', 'Player kicked')."', '".t('servers', 'Player [[player]] has been kicked from the server.', array('[[player]]' => RemoveCode($name)))."', 'green', 'index.php?p=servers');");
        } else {
            $objResponse->addScript("ShowBox('".t('sb-callback', 'Error')."', '".t('servers', 'Can not kick [[player]]. Player is immune!', array('[[player]]' => RemoveCode($name)))."', 'red', '', true);");
        }
    } else {
        $objResponse->addScript("ShowBox('".t('sb-callback', 'Error')."', '".t('servers', 'Can not kick [[player]]. Player not on the server anymore!', array('[[player]]' => RemoveCode($name)))."', 'red', '', true);");
    }
    return $objResponse;
}

function SetupEditServer($sid) {
    $objResponse = new xajaxResponse();
    $sid = (int) $sid;
    $server = $GLOBALS['db']->GetRow("SELECT * FROM " . DB_PREFIX . "_servers WHERE sid = $sid");

    // clear any old stuff
    $objResponse->addScript("$('address').value = ''");
    $objResponse->addScript("$('port').value = ''");
    $objResponse->addScript("$('rcon').value = ''");
    $objResponse->addScript("$('rcon2').value = ''");
    $objResponse->addScript("$('mod').value = '0'");
    $objResponse->addScript("$('serverg').value = '0'");


    // add new stuff
    $objResponse->addScript("$('address').value = '" . $server['ip'] . "'");
    $objResponse->addScript("$('port').value =  '" . $server['port'] . "'");
    $objResponse->addScript("$('rcon').value =  '" . $server['rcon'] . "'");
    $objResponse->addScript("$('rcon2').value =  '" . $server['rcon'] . "'");
    $objResponse->addScript("$('mod').value =  " . $server['modid']);
    $objResponse->addScript("$('serverg').value =  " . $server['gid']);

    $objResponse->addScript("$('insert_type').value =  " . $server['sid']);
    $objResponse->addScript("SwapPane(1);");
    return $objResponse;
}

function CheckPassword($aid, $pass) {
    $objResponse = new xajaxResponse();
    global $userbank;
    $aid = (int) $aid;
    if (!$userbank->CheckLogin($userbank->encrypt_password($pass), $aid)) {
        $objResponse->addScript("$('current.msg').setStyle('display', 'block');");
        $objResponse->addScript("$('current.msg').setHTML('".t('youraccount', 'Incorrect password.')."');");
        $objResponse->addScript("set_error(1);");
    } else {
        $objResponse->addScript("$('current.msg').setStyle('display', 'none');");
        $objResponse->addScript("set_error(0);");
    }
    return $objResponse;
}

function ChangePassword($aid, $pass) {
    global $userbank;
    $objResponse = new xajaxResponse();
    $aid = (int) $aid;

    if ($aid != $userbank->aid && !$userbank->HasAccess(ADMIN_OWNER | ADMIN_EDIT_ADMINS)) {
        $objResponse->redirect("index.php?p=login&n=no_access", 0);

        new CSystemLog(
            'w',
            t('log', 'Hacking Attempt'),
            t('log', '[[address]] tried to change a password that doesn\'t have permissions.', array(
                '[[address]]' => $_SERVER["REMOTE_ADDR"],
            ))
        );
        
        return $objResponse;
    }

    $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_admins` SET `password` = '" . $userbank->encrypt_password($pass) . "' WHERE `aid` = $aid");
    $admname = $GLOBALS['db']->GetRow("SELECT user FROM `" . DB_PREFIX . "_admins` WHERE aid = ?", array((int) $aid));
    $objResponse->addAlert(t('youraccount', 'Password changed successfully'));
    $objResponse->addRedirect("index.php?p=login", 0);
    
    new CSystemLog(
        'm',
        t('log', 'Password Changed'),
        t('log', 'Password changed for admin ([[admin]])', array(
            '[[admin]]' => $admname['user']
        ))
    );
    
    return $objResponse;
}

function EditAdminPerms($aid, $web_flags, $srv_flags) {
    if (empty($aid)) {
        return;
    }
    $aid = (int) $aid;
    $web_flags = (int) $web_flags;

    $objResponse = new xajaxResponse();
    global $userbank, $username;
    if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_EDIT_ADMINS)) {
        $objResponse->redirect("index.php?p=login&n=no_access", 0);

        new CSystemLog(
            'w',
            t('log', 'Hacking Attempt'),
            t('log', '[[username]] tried to edit admin permissions, but doesnt have access.', array(
                '[[username]]' => $username,
            ))
        );
        
        return $objResponse;
    }

    if (!$userbank->HasAccess(ADMIN_OWNER) && (int) $web_flags & ADMIN_OWNER) {
        $objResponse->redirect("index.php?p=login&n=no_access", 0);

        new CSystemLog(
            'w',
            t('log', 'Hacking Attempt'),
            t('log', '[[username]] tried to gain OWNER admin permissions, but doesnt have access.', array(
                '[[username]]' => $username,
            ))
        );
        
        return $objResponse;
    }

    // Users require a password and email to have web permissions
    $password = $GLOBALS['userbank']->GetProperty('password', $aid);
    $email = $GLOBALS['userbank']->GetProperty('email', $aid);
    if ($web_flags > 0 && (empty($password) || empty($email))) {
        
        $msg = t(
            'sb-callback',
            'Admins have to have a password and email set in order to get web permissions.<br /><a href="index.php?p=admin&c=admins&o=editdetails&id=[[adminid]]" title="Edit Admin Details">Set the details</a> first and try again.',
            array(
                '[[adminid]]' => $aid
            )
        );
        
        $objResponse->addScript("ShowBox('".t('sb-callback', 'Error')."', '{$msg}', 'red', '');");
        return $objResponse;
    }

    // Update web stuff
    $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_admins` SET `extraflags` = $web_flags WHERE `aid` = $aid");


    if (strstr($srv_flags, "#")) {
        $immunity = "0";
        $immunity = substr($srv_flags, strpos($srv_flags, "#") + 1);
        $srv_flags = substr($srv_flags, 0, strlen($srv_flags) - strlen($immunity) - 1);
    }
    $immunity = ($immunity > 0) ? $immunity : 0;
    // Update server stuff
    $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_admins` SET `srv_flags` = ?, `immunity` = ? WHERE `aid` = $aid", array($srv_flags, $immunity));

    if (isset($GLOBALS['config']['config.enableadminrehashing']) && $GLOBALS['config']['config.enableadminrehashing'] == 1) {
        // rehash the admins on the servers
        $serveraccessq = $GLOBALS['db']->GetAll("SELECT s.sid FROM `" . DB_PREFIX . "_servers` s
												LEFT JOIN `" . DB_PREFIX . "_admins_servers_groups` asg ON asg.admin_id = '" . (int) $aid . "'
												LEFT JOIN `" . DB_PREFIX . "_servers_groups` sg ON sg.group_id = asg.srv_group_id
												WHERE ((asg.server_id != '-1' AND asg.srv_group_id = '-1')
												OR (asg.srv_group_id != '-1' AND asg.server_id = '-1'))
												AND (s.sid IN(asg.server_id) OR s.sid IN(sg.server_id)) AND s.enabled = 1");
        $allservers = array();
        foreach ($serveraccessq as $access) {
            if (!in_array($access['sid'], $allservers)) {
                $allservers[] = $access['sid'];
            }
        }
        $objResponse->addScript("ShowRehashBox('" . implode(",", $allservers) . "', '".t('sb-callback', 'Permissions updated')."', '".t('sb-callback', 'The user`s permissions have been updated successfully')."', 'green', 'index.php?p=admin&c=admins');TabToReload();");
    } else {
        $objResponse->addScript("ShowBox('".t('sb-callback', 'Permissions updated')."', '".t('sb-callback', 'The user`s permissions have been updated successfully')."', 'green', 'index.php?p=admin&c=admins');TabToReload();");
    }
    $admname = $GLOBALS['db']->GetRow("SELECT user FROM `" . DB_PREFIX . "_admins` WHERE aid = ?", array((int) $aid));
    new CSystemLog("m", "Permissions Changed", "Permissions have been changed for (" . $admname['user'] . ")");
    
    new CSystemLog(
        'm',
        t('log', 'Permissions Changed'),
        t('log', 'Permissions have been changed for ([[admin]])', array(
            '[[admin]]' => $admname['user']
        ))
    );
    
    return $objResponse;
}

function EditGroup($gid, $web_flags, $srv_flags, $type, $name, $overrides, $newOverride) {
    $objResponse = new xajaxResponse();
    global $userbank, $username;
    if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_EDIT_GROUPS)) {
        $objResponse->redirect("index.php?p=login&n=no_access", 0);

        new CSystemLog(
            'w',
            t('log', 'Hacking Attempt'),
            t('log', '[[username]] tried to edit group details, but doesnt have access.', array(
                '[[username]]' => $username,
            ))
        );
        
        return $objResponse;
    }

    if (empty($name)) {
        $objResponse->redirect("index.php?p=login&n=no_access", 0);

        new CSystemLog(
            'w',
            t('log', 'Hacking Attempt'),
            t('log', '[[username]] tried to set group\'s name to nothing. This isn\'t possible with the normal form.', array(
                '[[username]]' => $username,
            ))
        );
        
        return $objResponse;
    }

    $gid = (int) $gid;
    $name = RemoveCode($name);
    $web_flags = (int) $web_flags;
    if ($type == "web" || $type == "server") {
    // Update web stuff
        $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_groups` SET `flags` = ?, `name` = ? WHERE `gid` = $gid", array($web_flags, $name));
    }

    if ($type == "srv") {
        $gname = $GLOBALS['db']->GetRow("SELECT name FROM " . DB_PREFIX . "_srvgroups WHERE id = $gid");

        if (strstr($srv_flags, "#")) {
            $immunity = 0;
            $immunity = substr($srv_flags, strpos($srv_flags, "#") + 1);
            $srv_flags = substr($srv_flags, 0, strlen($srv_flags) - strlen($immunity) - 1);
        }
        $immunity = ($immunity > 0) ? $immunity : 0;

        // Update server stuff
        $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_srvgroups` SET `flags` = ?, `name` = ?, `immunity` = ? WHERE `id` = $gid", array($srv_flags, $name, $immunity));

        $oldname = $GLOBALS['db']->GetAll("SELECT aid FROM " . DB_PREFIX . "_admins WHERE srv_group = ?", array($gname['name']));
        foreach ($oldname as $o) {
            $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_admins` SET `srv_group` = ? WHERE `aid` = '" . (int) $o['aid'] . "'", array($name));
        }

        // Update group overrides
        if (!empty($overrides)) {
            foreach ($overrides as $override) {
                // Skip invalid stuff?!
                if ($override['type'] != "command" && $override['type'] != "group") {
                    continue;
                }

                $id = (int) $override['id'];
                // Wants to delete this override?
                if (empty($override['name'])) {
                    $GLOBALS['db']->Execute("DELETE FROM `" . DB_PREFIX . "_srvgroups_overrides` WHERE id = ?;", array($id));
                    continue;
                }

                // Check for duplicates
                $chk = $GLOBALS['db']->GetAll("SELECT * FROM `" . DB_PREFIX . "_srvgroups_overrides` WHERE name = ? AND type = ? AND group_id = ? AND id != ?", array($override['name'], $override['type'], $gid, $id));
                if (!empty($chk)) {
                    
                    $msg = t(
                        'sb-callback',
                        'There already is an override with name [[name]] from the selected type..',
                        array(
                            '[[name]]' => htmlspecialchars(addslashes($override['name']))
                        )
                    );
                    
                    $objResponse->addScript("ShowBox('".t('sb-callback', 'Error')."', '{$msg}', 'red', '', true);");
                    return $objResponse;
                }

                // Edit the override
                $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_srvgroups_overrides` SET name = ?, type = ?, access = ? WHERE id = ?;", array($override['name'], $override['type'], $override['access'], $id));
            }
        }

        // Add a new override
        if (!empty($newOverride)) {
            if (($newOverride['type'] == "command" || $newOverride['type'] == "group") && !empty($newOverride['name'])) {
                // Check for duplicates
                $chk = $GLOBALS['db']->GetAll("SELECT * FROM `" . DB_PREFIX . "_srvgroups_overrides` WHERE name = ? AND type = ? AND group_id = ?", array($newOverride['name'], $newOverride['type'], $gid));
                if (!empty($chk)) {
                    $msg = t(
                        'sb-callback',
                        'There already is an override with name [[name]] from the selected type..',
                        array(
                            '[[name]]' => htmlspecialchars(addslashes($override['name']))
                        )
                    );
                    
                    $objResponse->addScript("ShowBox('".t('sb-callback', 'Error')."', '{$msg}', 'red', '', true);");
                    return $objResponse;
                }

                // Insert the new override
                $GLOBALS['db']->Execute("INSERT INTO `" . DB_PREFIX . "_srvgroups_overrides` (group_id, type, name, access) VALUES (?, ?, ?, ?);", array($gid, $newOverride['type'], $newOverride['name'], $newOverride['access']));
            }
        }

        if (isset($GLOBALS['config']['config.enableadminrehashing']) && $GLOBALS['config']['config.enableadminrehashing'] == 1) {
            // rehash the settings out of the database on all servers
            $serveraccessq = $GLOBALS['db']->GetAll("SELECT sid FROM " . DB_PREFIX . "_servers WHERE enabled = 1;");
            $allservers = array();
            foreach ($serveraccessq as $access) {
                if (!in_array($access['sid'], $allservers)) {
                    $allservers[] = $access['sid'];
                }
            }
            $objResponse->addScript("ShowRehashBox('" . implode(",", $allservers) . "', '".t('sb-callback', 'Group updated')."', '".t('sb-callback', 'The group has been updated successfully')."', 'green', 'index.php?p=admin&c=groups');TabToReload();");
        } else {
            $objResponse->addScript("ShowBox('".t('sb-callback', 'Group updated')."', '".t('sb-callback', 'The group has been updated successfully')."', 'green', 'index.php?p=admin&c=groups');TabToReload();");
        }

        new CSystemLog(
            'm',
            t('log', 'Group Updated'),
            t('log', 'Group ([[group]]) has been updated', array(
                '[[group]]' => $name
            ))
        );
        
        return $objResponse;
    }

    $objResponse->addScript("ShowBox('".t('sb-callback', 'Group updated')."', '".t('sb-callback', 'The group has been updated successfully')."', 'green', 'index.php?p=admin&c=groups');TabToReload();");
    new CSystemLog(
        'm',
        t('log', 'Group Updated'),
        t('log', 'Group ([[group]]) has been updated', array(
            '[[group]]' => $name
        ))
    );
    
    
    return $objResponse;
}

function CheckVersion() {
    $objResponse = new xajaxResponse();
    $relver = @file_get_contents("http://www.sourcebans.net/public/versionchecker/?type=rel");

    if (defined('SB_SVN')) {
        $relsvn = @file_get_contents("http://www.sourcebans.net/public/versionchecker/?type=svn");
    }

    if (version_compare($relver, SB_VERSION) > 0) {
        $versmsg = "<span style='color:#aa0000;'><strong>".t('sb-callback', 'A new release is available.')."</strong></span>";
    } else {
        $versmsg = "<span style='color:#00aa00;'><strong>".t('sb-callback', 'You have the latest release.')."</strong></span>";
    }

    $msg = $versmsg;
    if (strlen($relver) > 8 || $relver == "") {
        $relver = "<span style='color:#aa0000;'>Error</span>";
        $msg = "<span style='color:#aa0000;'><strong>".t('sb-callback', 'Error retrieving latest release.')."</strong></span>";
    }
    $objResponse->addAssign("relver", "innerHTML", $relver);

    if (defined('SB_SVN')) {
        if (intval($relsvn) > GetSVNRev()) {
            $svnmsg = "<span style='color:#aa0000;'><strong>".t('sb-callback', 'A new SVN revision is available.')."</strong></span>";
        } else {
            $svnmsg = "<span style='color:#00aa00;'><strong>".t('sb-callback', 'You have the latest SVN revision.')."</strong></span>";
        }

        if (strlen($relsvn) > 8 || $relsvn == "") {
            $relsvn = "<span style='color:#aa0000;'>".t('sb-callback', 'Error')."</span>";
            $svnmsg = "<span style='color:#aa0000;'><strong>".t('sb-callback', 'Error retrieving latest svn revision.')."</strong></span>";
        }
        $msg .= "<br />" . $svnmsg;
        $objResponse->addAssign("svnrev", "innerHTML", $relsvn);
    }

    $objResponse->addAssign("versionmsg", "innerHTML", $msg);
    return $objResponse;
}

function SelTheme($theme) {
    $objResponse = new xajaxResponse();
    global $userbank, $username;
    if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_WEB_SETTINGS)) {
        $objResponse->redirect("index.php?p=login&n=no_access", 0);
        new CSystemLog(
            'w',
            t('log', 'Hacking Attempt'),
            t('log', '[[username]] tried to execute SelTheme() function, but doesnt have access.', array(
                '[[username]]' => $username,
            ))
        );
        
        return $objResponse;
    }

    $theme = rawurldecode($theme);
    $theme = str_replace(array('../', '..\\', chr(0)), '', $theme);
    $theme = basename($theme);

    if ($theme[0] == '.' || !in_array($theme, scandir(SB_THEMES)) || !is_dir(SB_THEMES . $theme) || !file_exists(SB_THEMES . $theme . "/theme.conf.php")) {
        $objResponse->addAlert(t('sb-callback', 'Invalid theme selected.'));
        return $objResponse;
    }

    include(SB_THEMES . $theme . "/theme.conf.php");

    if (!defined('theme_screenshot')) {
        $objResponse->addAlert(t('sb-callback', 'Bad theme selected.'));
        return $objResponse;
    }

    $objResponse->addAssign("current-theme-screenshot", "innerHTML", '<img width="250px" height="170px" src="themes/' . $theme . '/' . strip_tags(theme_screenshot) . '">');
    $objResponse->addAssign("theme.name", "innerHTML", theme_name);
    $objResponse->addAssign("theme.auth", "innerHTML", theme_author);
    $objResponse->addAssign("theme.vers", "innerHTML", theme_version);
    $objResponse->addAssign("theme.link", "innerHTML", '<a href="' . theme_link . '" target="_new">' . theme_link . '</a>');
    $objResponse->addAssign("theme.apply", "innerHTML", "<input type='button' onclick=\"javascript:xajax_ApplyTheme('" . $theme . "')\" name='btnapply' class='btn ok' onmouseover='ButtonOver(\"btnapply\")' onmouseout='ButtonOver(\"btnapply\")' id='btnapply' value='".t('sb-callback', 'Apply Theme')."' />");

    return $objResponse;
}

function ApplyTheme($theme) {
    $objResponse = new xajaxResponse();
    global $userbank, $username;
    if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_WEB_SETTINGS)) {
        $objResponse->redirect("index.php?p=login&n=no_access", 0);

        new CSystemLog(
            'w',
            t('log', 'Hacking Attempt'),
            t('log', '[[username]] tried to change the theme to [[theme]], but doesnt have access.', array(
                '[[username]]' => $username,
                '[[theme]]' => htmlspecialchars(addslashes($theme))
            ))
        );
        
        return $objResponse;
    }

    $theme = rawurldecode($theme);
    $theme = str_replace(array('../', '..\\', chr(0)), '', $theme);
    $theme = basename($theme);

    if ($theme[0] == '.' || !in_array($theme, scandir(SB_THEMES)) || !is_dir(SB_THEMES . $theme) || !file_exists(SB_THEMES . $theme . "/theme.conf.php")) {
        $objResponse->addAlert(t('sb-callback', 'Invalid theme selected.'));
        return $objResponse;
    }

    include(SB_THEMES . $theme . "/theme.conf.php");

    if (!defined('theme_screenshot')) {
        $objResponse->addAlert(t('sb-callback', 'Bad theme selected.'));
        return $objResponse;
    }

    $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_settings` SET `value` = ? WHERE `setting` = 'config.theme'", array($theme));
    $objResponse->addScript('window.location.reload( false );');
    return $objResponse;
}

function ClearCache() {
    $objResponse = new xajaxResponse();
    global $userbank, $username;
    if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_WEB_SETTINGS)) {
        $objResponse->redirect("index.php?p=login&n=no_access", 0);

        new CSystemLog(
            'w',
            t('log', 'Hacking Attempt'),
            t('log', '[[username]] tried to clear the cache, but doesnt have access.', array(
                '[[username]]' => $username
            ))
        );
        
        return $objResponse;
    }

    $cachedir = dir(SB_THEMES_COMPILE . DIRECTORY_SEPARATOR);
    while (($entry = $cachedir->read()) !== false) {
        if (is_file($cachedir->path . $entry)) {
            unlink($cachedir->path . $entry);
        }
    }
    $cachedir->close();
    $GLOBALS['db']->CacheFlush();
    $objResponse->addScript("$('clearcache.msg').innerHTML = '<font color=\"green\" size=\"1\">".t('sb-callback', 'Cache cleared.')."</font>';");

    return $objResponse;
}

function RefreshServer($sid) {
    $objResponse = new xajaxResponse();
    $sid = (int) $sid;
    session_start();
    $data = $GLOBALS['db']->GetRow("SELECT ip, port FROM `" . DB_PREFIX . "_servers` WHERE sid = ?;", array($sid));
    if (isset($_SESSION['getInfo.' . $data['ip'] . '.' . $data['port']]) && is_array($_SESSION['getInfo.' . $data['ip'] . '.' . $data['port']])) {
        unset($_SESSION['getInfo.' . $data['ip'] . '.' . $data['port']]);
    }
    $objResponse->addScript("xajax_ServerHostPlayers('" . $sid . "');");
    return $objResponse;
}

function RehashAdmins($server, $do = 0) {
    $objResponse = new xajaxResponse();
    global $userbank, $username;
    $do = (int) $do;
    if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_EDIT_ADMINS | ADMIN_EDIT_GROUPS | ADMIN_ADD_ADMINS)) {
        $objResponse->redirect("index.php?p=login&n=no_access", 0);

        new CSystemLog(
            'w',
            t('log', 'Hacking Attempt'),
            t('log', '[[username]] tried to rehash admins, but doesnt have access.', array(
                '[[username]]' => $username
            ))
        );
        
        return $objResponse;
    }
    $servers = explode(",", $server);
    if (sizeof($servers) > 0) {
        if (sizeof($servers) - 1 > $do) {
            $objResponse->addScriptCall("xajax_RehashAdmins", $server, $do + 1);
        }

        $serv = $GLOBALS['db']->GetRow("SELECT ip, port, rcon FROM " . DB_PREFIX . "_servers WHERE sid = '" . (int) $servers[$do] . "';");
        if (empty($serv['rcon'])) {
            $objResponse->addAppend("rehashDiv", "innerHTML", "" . $serv['ip'] . ":" . $serv['port'] . " (" . ($do + 1) . "/" . sizeof($servers) . ") <font color='red'>".t('admin/servers', 'failed: No rcon password set')."</font>.<br />");
            if ($do >= sizeof($servers) - 1) {
                $objResponse->addAppend("rehashDiv", "innerHTML", "<b>".t('admin/servers', 'Done')."</b>");
                $objResponse->addScript("$('dialog-control').setStyle('display', 'block');");
            }
            return $objResponse;
        }

        require_once INCLUDES_PATH . 'SourceQuery'.DIRECTORY_SEPARATOR.'SourceQuery.class.php';
        $query = new SourceQuery;
        
        try {
            $query->Connect($serv['ip'], $serv['port'], 1);
        } catch(Exception $e) {
            $objResponse->addAppend("rehashDiv", "innerHTML", "" . $serv['ip'] . ":" . $serv['port'] . " (" . ($do + 1) . "/" . sizeof($servers) . ") <font color='red'>".t('sb-callback', 'failed: Can not connect')."</font>.<br />");
            if ($do >= sizeof($servers) - 1) {
                $objResponse->addAppend("rehashDiv", "innerHTML", "<b>".t('admin/servers', 'Done')."</b>");
                $objResponse->addScript("$('dialog-control').setStyle('display', 'block');");
            }
            return $objResponse;
        }
        
        try {
            $query->SetRconPassword($serv['rcon']);
        } catch(Exception $e) {
            $GLOBALS['db']->Execute("UPDATE " . DB_PREFIX . "_servers SET rcon = '' WHERE sid = '" . $serv['sid'] . "';");
            $objResponse->addAppend("rehashDiv", "innerHTML", "" . $serv['ip'] . ":" . $serv['port'] . " (" . ($do + 1) . "/" . sizeof($servers) . ") <font color='red'>".t('sb-callback', 'failed: Wrong rcon password')."</font>.<br />");
            if ($do >= sizeof($servers) - 1) {
                $objResponse->addAppend("rehashDiv", "innerHTML", "<b>".t('admin/servers', 'Done')."</b>");
                $objResponse->addScript("$('dialog-control').setStyle('display', 'block');");
            }
            return $objResponse;
        }

        $query->Rcon("sm_rehash");

        $objResponse->addAppend("rehashDiv", "innerHTML", "" . $serv['ip'] . ":" . $serv['port'] . " (" . ($do + 1) . "/" . sizeof($servers) . ") <font color='green'>".t('admin/servers', 'successful')."</font>.<br />");
        if ($do >= sizeof($servers) - 1) {
            $objResponse->addAppend("rehashDiv", "innerHTML", "<b>".t('admin/servers', 'Done')."</b>");
            $objResponse->addScript("$('dialog-control').setStyle('display', 'block');");
        }
    } else {
        $objResponse->addAppend("rehashDiv", "innerHTML", t('admin/servers', 'No servers to check.'));
        $objResponse->addScript("$('dialog-control').setStyle('display', 'block');");
    }
    return $objResponse;
}

function GetGroups($friendid) {
    set_time_limit(0);
    $objResponse = new xajaxResponse();
    if ($GLOBALS['config']['config.enablegroupbanning'] == 0 || !is_numeric($friendid)) {
        return $objResponse;
    }
    global $userbank, $username;
    if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_ADD_BAN)) {
        $objResponse->redirect("index.php?p=login&n=no_access", 0);

        new CSystemLog(
            'w',
            t('log', 'Hacking Attempt'),
            t('log', '[[username]] tried to list groups of "[[group]]", but doesnt have access.', array(
                '[[username]]' => $username,
                '[[group]]' => htmlspecialchars(addslashes(trim($friendid)))
            ))
        );
        
        return $objResponse;
    }
    // check if we're getting redirected, if so there is $result["Location"] (the player uses custom id)  else just use the friendid. !We can't get the xml with the friendid url if the player has a custom one!
    $result = get_headers("http://steamcommunity.com/profiles/" . $friendid . "/", 1);
    $raw = file_get_contents((!empty($result["Location"]) ? $result["Location"] : "http://steamcommunity.com/profiles/" . $friendid . "/") . "?xml=1");
    preg_match("/<privacyState>([^\]]*)<\/privacyState>/", $raw, $status);
    if (($status && $status[1] != "public") || strstr($raw, "<groups>")) {
        $raw = str_replace("&", "", $raw);
        $raw = strip_31_ascii($raw);
        $raw = utf8_encode($raw);
        $xml = simplexml_load_string($raw); // parse xml
        $result = $xml->xpath('/profile/groups/group'); // go to the group nodes
        $i = 0;
        while (list(, $node) = each($result)) {
            // Steam only provides the details of the first 3 groups of a players profile. We need to fetch the individual groups seperately to get the correct information.
            if (empty($node->groupName)) {
                $memberlistxml = file_get_contents("http://steamcommunity.com/gid/" . $node->groupID64 . "/memberslistxml/?xml=1");
                $memberlistxml = str_replace("&", "", $memberlistxml);
                $memberlistxml = strip_31_ascii($memberlistxml);
                $memberlistxml = utf8_encode($memberlistxml);
                $groupxml = simplexml_load_string($memberlistxml); // parse xml
                $node = $groupxml->xpath('/memberList/groupDetails');
                $node = $node[0];
            }

            // Checkbox & Groupname table cols
            $objResponse->addScript('var e = document.getElementById("steamGroupsTable");
                var tr = e.insertRow("-1");
                    var td = tr.insertCell("-1");
                        td.className = "listtable_1";
                        td.style.padding = "0px";
                        td.style.width = "3px";
                            var input = document.createElement("input");
                            input.setAttribute("type","checkbox");
                            input.setAttribute("id","chkb_' . $i . '");
                            input.setAttribute("value","' . $node->groupURL . '");
                        td.appendChild(input);
                    var td = tr.insertCell("-1");
                        td.className = "listtable_1";
                        var a = document.createElement("a");
                            a.href = "http://steamcommunity.com/groups/' . $node->groupURL . '";
                            a.setAttribute("target","_blank");
                                var txt = document.createTextNode("' . utf8_decode($node->groupName) . '");
                            a.appendChild(txt);
                        td.appendChild(a);
                            var txt = document.createTextNode(" (");
                        td.appendChild(txt);
                            var span = document.createElement("span");
                            span.setAttribute("id","membcnt_' . $i . '");
                            span.setAttribute("value","' . $node->memberCount . '");
                                var txt3 = document.createTextNode("' . $node->memberCount . '");
                            span.appendChild(txt3);
                        td.appendChild(span);
                            var txt2 = document.createTextNode(" '.t('admin/bans', 'Members').')");
                        td.appendChild(txt2);
                    ');
            $i++;
        }
    } else {
        $msg = t(
            'sb-callback',
            'There was an error retrieving the group data. <br>Maybe the player isn\'t member of any group or his profile is private?<br><a href="http://steamcommunity.com/profiles/[[friendid]]/" title="Community profile" target="_blank">Community profile</a>',
            array(
                '[[friendid]]' => $friendid
            )
        );
        $objResponse->addScript("ShowBox('".t('sb-callback', 'Error')."', '{$msg}', 'red', 'index.php?p=banlist', true);");
        $objResponse->addScript("$('steamGroupsText').innerHTML = '<i>".t('sb-callback', 'No groups...')."</i>';");
        return $objResponse;
    }
    $objResponse->addScript("$('steamGroupsText').setStyle('display', 'none');");
    $objResponse->addScript("$('steamGroups').setStyle('display', 'block');");
    return $objResponse;
}

function ViewCommunityProfile($sid, $name) {
    $objResponse = new xajaxResponse();
    global $userbank, $username;
    if (!$userbank->is_admin()) {
        $objResponse->redirect("index.php?p=login&n=no_access", 0);

        new CSystemLog(
            'w',
            t('log', 'Hacking Attempt'),
            t('log', '[[username]] tried to view profile of "[[name]]", but doesnt have access.', array(
                '[[username]]' => $username,
                '[[friendid]]' => htmlspecialchars($name)
            ))
        );
        
        return $objResponse;
    }
    $sid = (int) $sid;

    $data = $GLOBALS['db']->GetRow(
        "SELECT
            `srv`.`ip` `ip`,
            `srv`.`port` `port`,
            `srv`.`rcon` `rcon`,
            `mod`.`modfolder` `game`
        FROM
            `".DB_PREFIX."_servers` `srv`
        LEFT JOIN
            `".DB_PREFIX."_mods` `mod`
        ON
            `mod`.`mid` = `srv`.`modid`
        WHERE `srv`.`sid` = '".intval($sid)."';");
    if (empty($data['rcon'])) {
        $objResponse->addScript("ShowBox('"
                .t('servers', 'Error')
                ."', '"
                .t(
                    'servers',
                    'Can not get playerinfo for [[player]]. No RCON password!',
                    array(
                        '[[player]]' => addslashes(htmlspecialchars($name))
                    )
                )
                ."', 'red', '', true);");
        return $objResponse;
    }
    
    require_once INCLUDES_PATH . 'SourceQuery'.DIRECTORY_SEPARATOR.'SourceQuery.class.php';
    $query = new SourceQuery;

    try {
        $query->Connect($data['ip'], $data['port'], 1);
    } catch(Exception $e) {
        return $objResponse;
    }

    try {
        $query->SetRconPassword($data['rcon']);
    } catch(Exception $e) {
        $GLOBALS['db']->Execute("UPDATE " . DB_PREFIX . "_servers SET rcon = '' WHERE sid = '" . $sid . "';");
        $objResponse->addScript("ShowBox('".t('servers', 'Error')."', '".t('servers', 'Can not get playerinfo for [[player]]. Wrong RCON password!', array('[[player]]' => addslashes(htmlspecialchars($name))))."', 'red', '', true);");
        return $objResponse;
    }

    $players = getPlayersFromStatus($query->Rcon('status'), $data['game']);
    $steam = false;
    foreach($players as $player) {
        if($player['name'] == $name) {
            $steam = $player['steamid'];
        }
    }
    if ($steam) {
        $dialogMsg = t(
            'servers',
            'Generating Community Profile link for [[player]], please wait...',
            array('[[player]]' => addslashes(htmlspecialchars($name)))
        );
        $objResponse->addScript("$('dialog-control').setStyle('display', 'block');$('dialog-content-text').innerHTML = '"
                .$dialogMsg
                ."<br /><font color=\"green\">"
                .t('servers', 'Done.')
                ."</font><br /><br /><b><a href=\"http://www.steamcommunity.com/profiles/" . SteamIDToFriendID($steam) . "/\" title=\"" . addslashes(htmlspecialchars($name)) . "\'s Profile\" target=\"_blank\">".t('servers', 'Watch the profile')."</a>.</b>';");
        //$objResponse->addScript("window.open('http://www.steamcommunity.com/profiles/" . SteamIDToFriendID($steam) . "/', 'Community_" . $steam . "');");
    } else {
        $objResponse->addScript("ShowBox('".t('servers', 'Error')."', '".t('servers', 'Can not get playerinfo for [[player]]. Player not on the server anymore!', array('[[player]]' => addslashes(htmlspecialchars($name)))) ."', 'red', '', true);");
    }
    return $objResponse;
}

function SendMessage($sid, $name, $message) {
    $objResponse = new xajaxResponse();
    global $userbank, $username;
    if (!$userbank->is_admin()) {
        $objResponse->redirect("index.php?p=login&n=no_access", 0);
        new CSystemLog(
            'w',
            t('log', 'Hacking Attempt'),
            t('log', '[[username]] tried to send ingame message to  "[[player]]" (Message: "[[message]]"), but doesnt have access.', array(
                '[[username]]' => $username,
                '[[friendid]]' => addslashes(htmlspecialchars($name)),
                '[[message]]' => RemoveCode($message)
            ))
        );
        
        return $objResponse;
    }
    $sid = (int) $sid;
    //get the server data
    $data = $GLOBALS['db']->GetRow("SELECT ip, port, rcon FROM " . DB_PREFIX . "_servers WHERE sid = '" . $sid . "';");
    if (empty($data['rcon'])) {
        $msg = t(
            'admin/servers',
            'Can not send message to "[[player]]". No RCON password!',
            array(
                '[[player]]' => addslashes(htmlspecialchars($name))
            )
        );
        $objResponse->addScript("ShowBox('".t('sb-callback', 'Error')."', '{$msg}', 'red', '', true);");
        return $objResponse;
    }
    
    require_once INCLUDES_PATH . 'SourceQuery'.DIRECTORY_SEPARATOR.'SourceQuery.class.php';
    $query = new SourceQuery;
    try {
        $query->Connect($data['ip'], $data['port'], 1);
        $query->SetRconPassword($data['rcon']);
    } catch(Exception $e) {
        $msg = t(
            'admin/servers',
            'Can not send message to "[[player]]". Wrong RCON password!',
            array(
                '[[player]]' => addslashes(htmlspecialchars($name))
            )
        );
        $objResponse->addScript("ShowBox('".t('sb-callback', 'Error')."', '{$msg}', 'red', '', true);");
        return $objResponse;

    }
    
    
    $query->Rcon('sm_psay "' . $name . '" "' . addslashes($message) . '"');

    new CSystemLog(
        'm',
        t('log', 'Message sent to player'),
        t('log', 'The following message was sent to [[player]] on server [[server]]. Message: [[message]]', array(
            '[[player]]' => addslashes(htmlspecialchars($name)),
            '[[server]]' => $data['ip'] . ":" . $data['port'],
            '[[message]]' => RemoveCode($message)
        ))
    );
    
    $objResponse->addScript("ShowBox('".t('admin/servers', 'Message Sent')."', '".t('admin/servers', 'The message has been sent to player [[player]] successfully!', array('[[player]]' => addslashes(htmlspecialchars($name))))."', 'green', '', true);$('dialog-control').setStyle('display', 'block');");
    return $objResponse;
}
