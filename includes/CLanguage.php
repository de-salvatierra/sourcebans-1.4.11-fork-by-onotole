<?php

class CLanguage {
    
    /**
     *
     * @var string Текущий язык пользователя
     */
    private $language;
    
    /**
     *
     * @var array Имеющиеся языки
     */
    private $languages;
    
    /**
     *
     * @var array Массив со всеми подключенными файлами текущей сессии
     */
    private $_loadedFiles = array();
    
    /**
     * 
     */
    public function __construct() {
        $this->language = $this->getLanguage();
    }
    
    /**
     * 
     * @return array Массив с кодами языка
     */
    public function getLanguages() {
        if(is_array($this->languages)) {
            return $this->languages;
        }
        $langs = array();
        $langs['en'] = 'English';
        foreach(glob(SB_LANGUAGES_PATH . '*') as $dir) {
            if(is_file($dir . DIRECTORY_SEPARATOR . 'language.php')) {
                $langInfo = include($dir . DIRECTORY_SEPARATOR . 'language.php');
                $langs[basename($dir)] = $langInfo['langName'];
            }
        }
        $this->languages = $langs;
        return $langs;
    }

    public function canUserChange() {
        return isset($GLOBALS['config']['config.enableclientlanguage']) && $GLOBALS['config']['config.enableclientlanguage'] == 1;
    }
    
    public function setClientLanguage($lang) {
        if(!$this->canUserChange()) {
            new CSystemLog(
                'w',
                t('log', 'Language change'),
                t('log', 'User tried a change client language, but this function is disabled')
            );
            return false;
        }
        if($lang == $this->getLanguage()) {
            return true;
        }
        $phpSelf = filter_input(INPUT_SERVER, 'PHP_SELF');
        $baseurl =  dirname($phpSelf);
        $expire = strtotime('+1 MONTH');
        return setcookie('sb_language', $lang, $expire, $baseurl);
    }
    
    /**
     * 
     * @return string Текущий язык пользователя
     */
    public function getLanguage() {
        if($this->language) {
            return $this->language;
        }
        if(!empty($GLOBALS['config']['config.language'])) {
            $lang = $GLOBALS['config']['config.language'];
        }
        
        if(isset($GLOBALS['config']['config.enableclientlanguage']) && $GLOBALS['config']['config.enableclientlanguage'] == '1') {
            $cookie = filter_input(INPUT_COOKIE, 'sb_language');
            if($cookie) {
                $lang = $cookie;
            }
        }

        if(empty($lang)) {
            $lang = 'en';
        }
        
        if(!array_key_exists($lang, $this->getLanguages())) {
            exit('ERROR! WRONG LANGUAGE CODE!!! ('.$lang.')');
        }
        $this->language = $lang;
        return $lang;
    }
    
    /**
     * Переводит строку
     * @param string $section Секция сообщений (Путь к файлу, начиная от папки текущего языка)
     * <br>
     * Например, для Русского языка секция admin/bans значит, что будет использован файл
     * <br>
     * <pre>languages/ru/admin/bans.php</pre>
     * <br>
     * А секция servers означает, что будет использован файл languages/ru/servers.php
     * @param string $message Сообщение, которое будет переведено
     * @param array $params [Optional] Массив с параметрами перевода.
     * Если нужно передать переменную, то можно передать массив параметров
     * [[param_name]] => $some_var
     * И в нужном месте в строке $message вставить шоткод [[param_name]]
     * @return string
     */
    public function translate($section, $message, $params) {
        if($this->language != 'en') {
            $messages = $this->getMessages($section);

            if($messages !== false && !empty($messages[$message])) {
                $message = $messages[$message];
            }
        }
        if(!empty($params)) {
            return str_replace(array_keys($params), array_values($params), $message);
        }
        return $message;
    }
    
    /**
     * Получает полный путь к файлу с переводами
     * @param string $section Секция перевода
     * @return string
     * @throws Exception
     */
    private function getFilePathBySection($section) {
        if (preg_match('/modules\/(\w+)\/(\w+)/', $section, $match)) {
            $path = SB_MODULES
                    . $match[1]
                    . DIRECTORY_SEPARATOR
                    . 'languages'
                    . DIRECTORY_SEPARATOR
                    . $this->language
                    . DIRECTORY_SEPARATOR
                    . $match[2] . '.php';
        } else {
            $path = SB_LANGUAGES_PATH . $this->language . DIRECTORY_SEPARATOR . $section . '.php';
        }
        if(!file_exists($path)) {
            return false;
        }
        return $path;
    }
    
    private function getMessages($section) {
        if (empty($this->_loadedFiles[$section])) {
            $path = $this->getFilePathBySection($section);
            if($path === false) {
                return false;
            }
            $this->_loadedFiles[$section] = include_once $path;
        }
        return $this->_loadedFiles[$section];
    }
    
    public function getFiles() {
        return $this->_loadedFiles;
    }
    
    public static function ajaxTest() {
        return 'fsfsdf';
    }
}
