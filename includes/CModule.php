<?php

/**
 * Класс, управляющий модулями SourceBans.
 * @property string $title Заголовок страницы
 * @property string $pageTitle Заголовок сайта
 * @property array $breadCrumbs Хлебные крошки
 * @property ADODB_mysqli $db;
 * @property Smarty $theme;
 */

class CModule {
    
    /**
     * @var array массив с магическими свойствами класса
     */
    private $_params = array();
    
    /**
     * @var string Путь к модулю
     */
    private $_modulePath;
    
    /**
     * @var string Имя текущего модуля
     */
    private $_moduleName;
    
    /**
     * @var ADODB_mysqli
     */
    private $_db;
    
    /**
     * @var array массив со всеми модулями
     */
    private static $_allModules;

    /**
	 * Магический PHP метод.
     * Этот метод магически вызывается при попытке присвоения значения несуществующему свойству.
     * Ищет и вызывает сеттер и, если он есть, записывает результат в $this->_params
	 * @param string $name Имя свойства
	 * @param mixed $value Значение свойства
	 */
    public function __set($name, $value) {
        $setter = 'set' . ucfirst($name);
        if (method_exists($this, $setter)) {
            $this->$setter($value);
        } else {
            $this->_params[$name] = $value;
        }
    }
    
    /**
     * Магический PHP метод.
	 * This method is overridden so that $this->_params's elements can be accessed like properties.
     * Этот метод вызывается при попытке обращения к несуществующему свойству.
     * Ищет и вызывает метод с префиксом get и суффиксом с именем вызываемого свойства
     * @param string $name Имя свойства
     * @return результат геттера или null в случае ошибки
     */
    public function __get($name) {
        $getter = 'get' . ucfirst($name);
        if (method_exists($this, $getter)) {
            return $this->$getter();
        } elseif(isset($this->_params[$name])) {
            return $this->_params[$name];
        } else {
            return null;
        }
    }
    
    /**
	 * Checks whether there is a property of this class or property is not null
	 * @param string $name the property name
	 * @return boolean whether the property value is null
	 */
    public function __isset($name) {
        $getter = 'get' . ucfirst($name);
        if(isset($this->_params[$name])) {
            return true;
        } elseif (method_exists($this, $getter)) {
            return $this->$getter() !== null;
        } else {
            return false;
        }
    }
    
    /**
	 * Устанавливает свойству значение null.
	 * @param string $name Имя метода
	 */
    public function __unset($name) {
        $setter = 'set' . ucfirst($name);
        $getter = 'get' . ucfirst($name);
        if(isset($this->_params[$name])) {
            unset($this->_params[$name]);
        } elseif (method_exists($this, $setter)) {
            $this->$setter(null);
        } elseif(method_exists($this, $getter)) {
            throw new Exception('Property "'.$name.'" of class "' . get_class($this) . '" is readonly');
        }
    }

    /**
     * Магический PHP метод. Ищет статический метод в определенном модуле и вызывает его.
     * @param string $name Имя метода. Формируется следующим образом:
     * Первая часть - имя модуля в нижнем регистре, в котором будет происходить поиск статического метода.
     * Вторая часть - Имя статического метода в классе модуля. Начинается с большой буквы. А в классе модуля должен начинаться
     * с маленькой буквы. Пример:
     * Вызывается CModule::bansBansForDashboard();
     * В данном примере - bans - имя модуля в нижнем регистре.
     * BansForDashboard - Статический метод в модуле. Первая буква имени метода будет переведена в нижний регистр.
     * Будет вызван метод BansModule::bansForDashboard()
     * @param array $arguments Массив аргументов, которые будут переданы методу.
     * @return mixed|null Результат вызываемого метода, или null в случае ошибки
     */
    public static function __callStatic($name, $arguments) {
        $check = preg_match('/([a-z]+)([A-Z].+)/', $name, $match);
        if($check) {
            $moduleName = $match[1];
            $methodName = lcfirst($match[2]);
            $className = ucfirst($moduleName) . 'Module';
            
            if(!self::hasModule($moduleName, false) || !method_exists($className, $methodName)) {
                return null;
            }
            
            return call_user_func_array(array($className, $methodName), $arguments);
        }
        return null;
    }
    
    /**
     * Генерирует и возвращает контент
     * @param string $page Имя вызываемой страницы из запроса ($_GET['p'])
     * @return string
     */
    public function getContent($page = null) {
        if($page === null) {
            $page = 'index';
        }
        $modulePath = $this->getModulePath();
        $file = $modulePath
                . DIRECTORY_SEPARATOR 
                . 'pages' 
                . DIRECTORY_SEPARATOR;
        
        // For admin pages
        if($page == 'admin') {
            // Проверка на админа
            CheckAdminAccess(ALL_WEB);
            
            global $theme;
            $theme->template_dir .= DIRECTORY_SEPARATOR . 'admin';
            
            $act = filter_input(INPUT_GET, 'c');
            $file .= 'admin' . DIRECTORY_SEPARATOR;
            if($act) {
                $page = $act;
            } else {
                $page = 'index';
            }
        }
        
        $file .= $page . '.php';
        
        if(!is_file($file)) {
            return pageNotFound();
        }
        
        global $userbank;
        
        if(!$this->isInstalled() && !$userbank->HasAccess(ALL_WEB)) {
            return pageNotFound();
        }
        
        $this->registerAjax();
        ob_start();
        include $file;
        return ob_get_clean();
    }
    
    /**
     * @global Smarty $theme
     * @return Smarty
     */
    public function getTheme() {
        global $theme;
        return $theme;
    }
    
    /**
     * @return ADODB_mysqli
     */
    public function getDb() {
        if(!$this->_db) {
            $this->_db = $GLOBALS['db'];
        }
        return $this->_db;
    }
    
    /**
     * @return string Имя текущего модуля в нижнем регистре
     */
    public function getModuleName() {
        if (!$this->_moduleName) {
            $this->_moduleName = strtolower(str_replace('Module', '', get_class($this)));
        }
        return $this->_moduleName;
    }
    
    /**
     * Возвращает установлен ли модуль
     * @return boolean
     */
    public function isInstalled() {
        return true;
    }
    
    /**
     * @return string Путь к папке текущего модуля
     */
    protected function getModulePath() {
        if (!$this->_modulePath) {
            $this->_modulePath = SB_MODULES . $this->getModuleName();
        }
        return $this->_modulePath;
    }
    
    
    /**
     * Регистрирует методы текущего класса или классов-потомков для AJAX запросов.
     * Имя метода должно начинаться с префикса "xajax_".
     * Для примера: 
     * В классе модуля "testmodule" создадим метод "xajax_testFunstion()".
     * И в JavaScript будет доступна функция "xajax_testFunstion()"
     * @global xajax $xajax
     */
    private function registerAjax() {
        global $xajax;
        $methods = get_class_methods($this);
        foreach ($methods as $method) {
            if(substr($method, 0, 6) == 'xajax_') {
                $jsFunction = str_replace('xajax_', '', $method);
                $xajax->registerFunction(array($jsFunction, $this, $method));
            }
        }
    }
    
    /**
     * Получает все модули и возвращает массив в следующем формате:
     * <pre>
     * Array
     *  (
     *      [0] => Array
     *      (
     *          [basePath] => 
     *          [baseUrl] => 
     *          [mainImage] => 
     *          [name] => 
     *          [adminPage] => 
     *          [info] => Array
     *              (
     *                  [author] => 
     *                  [homePage] => 
     *                  [email] => 
     *              )
     *          )
     *      )
     * )
     * </pre>
     * @return array
     */
    public static function getAllModules() {
        if(self::$_allModules) {
            return self::$_allModules;
        }
        $modules = array();
        foreach(glob(SB_MODULES . '*') as $m) {
            if(!is_dir($m)) {
                continue;
            }
            $baseUrl =  str_replace(ROOT, '', $m);
            
            // For Windows
            if(strpos($m, '\\') !== false) {
                $baseUrl = str_replace('\\', '/');
            }
            if(is_file($m . DIRECTORY_SEPARATOR . 'pages' . DIRECTORY_SEPARATOR . 'admin/index.php')) {
                $adminPage = 'admin';
            } else {
                $adminPage = 'index';
            }
            $module = array(
                'basePath' => $m,
                'baseUrl' => $baseUrl,
                'mainImage' => './images/module.png',
                'name' => basename($m),
                'adminPage' => $adminPage,
                'info' => array(
                    'author' => '',
                    'homePage' => '',
                    'email' => ''
                )
            );
            $info = $m . DIRECTORY_SEPARATOR . 'moduleInfo.php';
            if(is_file($info)) {
                $module['info'] = include $info;
            }
            if(is_file($m . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'main.png')) {
                $module['mainImage'] = $baseUrl . '/images/main.png';
            }
            $modules[] = $module;
        }
        self::$_allModules = $modules;
        return self::$_allModules;
    }
    
    /**
     * Возвращает ссылки на админку каждого модуля для страницы админцентра
     * @return array
     */
    public static function getAdminMenu() {
        $return = array();
        $modules = self::getAllModules();
        foreach($modules as $module) {
            $menuFile = $module['basePath'] 
                    . DIRECTORY_SEPARATOR
                    . 'includes'
                    . DIRECTORY_SEPARATOR 
                    . 'adminmenu.php';
            if (is_file($menuFile)) {
                $return = array_merge($return, include $menuFile);
            }
        }
        return $return;
    }
    
    /**
     * Проверяет существование модуля
     * @param string $moduleName Имя модуля
     * @param boolean $installed Проверить установлен модуль или нет
     * @return boolean
     */
    public static function hasModule($moduleName, $installed = true) {
        $moduleName = strtolower($moduleName);
        $className = ucfirst($moduleName) . 'Module';
        
        $moduleFile = SB_MODULES . $moduleName . DIRECTORY_SEPARATOR . $className . '.php';
        if(!is_file($moduleFile)) {
            return false;
        }
        include_once $moduleFile;
        
        if(!$installed) {
            return true;
        }
        $module = new $className;
        $isInstalled = $module->isInstalled();
        return $isInstalled;
    }
}
