<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty {help_icon title="gaben" message="hello"} function plugin
 *
 * Type:     function<br>
 * Name:     help tip<br>
 * Purpose:  show help tip
 * @link http://www.sourcebans.net
 * @author  SourceBans Development Team
 * @param array
 * @param Smarty
 * @return string
 */
function smarty_function_help_icon($params, &$smarty)
{
    if(isset($params['translate'])) {
        $p = array();
        if(count($params) > 3) {
            foreach($params as $key => $val) {
                if($key != 'title' && $key != 'message' && $key != 'translate') {
                    $p["[[{$key}]]"] = $val;
                }
            }
        }
        $params['title'] = t($params['translate'], $params['title']);
        $params['message'] = t($params['translate'], $params['message'], $p);
    }
    return HelpIcon($params['title'], $params['message']);
}
