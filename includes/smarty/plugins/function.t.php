<?php
function smarty_function_t($params, &$smarty)
{
    $paramsArray = array();
    if(count($params) > 2) {
        foreach($params as $key => $val) {
            if($key != 'section' && $key != 'message') {
                $paramsArray["[[{$key}]]"] = $val;
            }
        }
    }
    return t($params['section'], $params['message'], $paramsArray);
}
