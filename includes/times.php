<?php
return array(
    0 => t('times', 'Permanent'),
    t('times', 'minutes') => array(
        1 => t('times', '1 minute'),
        5 => t('times', '5 minutes'),
        10 => t('times', '10 minutes'),
        15 => t('times', '15 minutes'),
        30 => t('times', '30 minutes'),
        45 => t('times', '45 minutes'),
    ),
    t('times', 'hours') => array(
        60 => t('times', '1 hour'),
        120 => t('times', '2 hours'),
        180 => t('times', '3 hours'),
        240 => t('times', '4 hours'),
        480 => t('times', '8 hours'),
        720 => t('times', '12 hours'),
    ),
    t('times', 'days') => array(
        1440 => t('times', '1 day'),
        2880 => t('times', '2 days'),
        4320 => t('times', '3 days'),
        5760 => t('times', '4 days'),
        7200 => t('times', '5 days'),
        8640 => t('times', '6 days'),
    ),
    t('times', 'weeks') => array(
        10080 => t('times', '1 week'),
        20160 => t('times', '2 weeks'),
        30240 => t('times', '3 weeks'),
    ),
    t('times', 'months') => array(
        43200 => t('times', '1 month'),
        86400 => t('times', '2 months'),
        129600 => t('times', '3 months'),
        259200 => t('times', '6 months'),
        518400 => t('times', '12 months'),
    ),
);
