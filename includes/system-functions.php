<?php
/* @var $GLOBALS['db'] ADODB_mysqli */
/**
 * =============================================================================
 * Main functions for system
 *
 * @author SteamFriends Development Team
 * @version 1.0.0
 * @copyright SourceBans (C)2007 SteamFriends.com.  All rights reserved.
 * @package SourceBans
 * @link http://www.sourcebans.net
 *
 * @version $Id: system-functions.php 268 2009-06-20 01:39:57Z peace-maker $
 * =============================================================================
 */

if(!defined("IN_SB")){echo "You should not be here. Only follow links!";die();}
/**
* Extended substr function. If it finds mbstring extension it will use, else
* it will use old substr() function
*
* @param string $string String that need to be fixed
* @param integer $start Start extracting from
* @param integer $length Extract number of characters
* @return string
*/
function substr_utf($string, $start = 0, $length = null) {
    $start = (integer) $start >= 0 ? (integer) $start : 0;
    if (is_null($length)) {
        $length = strlen_utf($string) - $start;
    }
    return substr($string, $start, $length);
}

/**
* Equivalent to htmlspecialchars(), but allows &#[0-9]+ (for unicode)
* This function was taken from punBB codebase <http://www.punbb.org/>
*
* @param string $str
* @return string
*/
function clean($str) {
	$str = preg_replace('/&(?!#[0-9]+;)/s', '&amp;', $str);
	$str = str_replace(array('<', '>', '"'), array('&lt;', '&gt;', '&quot;'), $str);
	return $str;
}

/**
* Check if selected email has valid email format
*
* @param string $user_email Email address
* @return boolean
*/
function is_valid_email($user_email) {
    return (bool)filter_var($user_email, FILTER_VALIDATE_EMAIL);
}

/**
 * Returns the full location that the website is running in
 *
 * @return string location of SourceBans
 */
function GetLocation()
{
	return substr($_SERVER['SCRIPT_FILENAME'], 0, strlen($base)-strlen("index.php"));
}

/**
 * Displays the header of SourceBans
 *
 * @return noreturn
 */
function BuildPageHeader()
{
	include TEMPLATES_PATH . "header.php";
}

/**
 * Displays the sub-nav menu of SourceBans
 *
 * @return noreturn
 */
function BuildSubMenu()
{
	global $theme;
	$theme->left_delimiter = '<!--{';
	$theme->right_delimiter = '}-->';
	$theme->display('submenu.tpl');
	$theme->left_delimiter = '{';
	$theme->right_delimiter = '}';
}

/**
 * Displays the content header
 *
 * @return noreturn
 */
function BuildContHeader()
{
	global $theme, $userbank;
	if(isset($_GET['p']) && $_GET['p'] == "admin" && !$userbank->is_admin()) {
		echo t('system-functions', 'You dont have admin. Be gone!');
		RedirectJS('index.php?p=login');
		PageDie();
	}

	if(!isset($_GET['s']) && isset($GLOBALS['title'])) {
		$page = "<b>".$GLOBALS['title']."</b>";
	}

	$theme->assign('main_title', isset($page)?$page:'');
	$theme->display('content_header.tpl');
}


/**
 * Adds a tab to the page
 *
 * @param string $title The title of the tab
 * @param string $utl The link of the tab
 * @param boolean $active Is the tab active?
 * @return noreturn
 */

function AddTab($title, $url, $desc, $active=null)
{
    if(substr(filter_input(INPUT_SERVER, 'REQUEST_URI'), 1) == $url && $active === null) {
        $active = true;
    }
    $tabs = array();
	$tabs['title'] = $title;
	$tabs['url'] = $url;
	$tabs['desc'] = $desc;
    $tabs['active'] = $active;

	include TEMPLATES_PATH . "tab.php";
}

/**
 * Displays the pagetabs
 *
 * @return noreturn
 */
function BuildPageTabs()
{
    $mainmenu = getLinks();
    foreach($mainmenu as $item) {
        AddTab($item['label'], $item['url'], $item['title']);
    }

    // BUILD THE SUB-MENU's FOR ADMIN PAGES
    $submenu = new CTabsMenu();

    $adminmenu = include INCLUDES_PATH . 'adminmenu.php';
    
    $adminSub = CModule::getAdminMenu();
    
    if(!empty($adminSub)) {
        $adminmenu = array_merge($adminmenu, $adminSub);
    }
    
    foreach ($adminmenu as $item) {
        if(!isset($item['visible']) || $item['visible'] === true) {
            $submenu->addMenuItem($item['label'], 0, "", $item['url'], true);
        }
    }

    SubMenu($submenu->getMenuArray());
}

/**
 * Rewrites the breadcrumb html
 *
 * @return noreturn
 */
function BuildBreadcrumbs($breadcrumb = false)
{
    if($breadcrumb) {
        $text = breadCrumbs($breadcrumb);
    } else {
        $text = "<b>" . t('mainmenu', 'Dashboard') . "</b>";
    }
    echo "\n<script type=\"text/javascript\">\n\t$('breadcrumb').setHTML('{$text}');\n</script>\n";

}

function breadCrumbs($bc) {
    $return = '<a href="index.php">'.t('breadcrumbs', 'Home').'</a> &raquo; ';
    
    if(is_string($bc)) {
        $return .= "<b>" . RemoveCode($bc) . "</b>";
    } elseif(is_array($bc)) {
        foreach($bc as $b) {
            if(is_array($b)) {
                
                if(!empty($b['items'])) {
                    $return .= breadCrumbs($b['items']);
                } else {
                    $return .= '<a href="'.$b['url'].'">'.  RemoveCode($b['label']) .'</a> &raquo; ';
                }
            } else {
                $return .= "<b>" . RemoveCode($b) . "</b>";
            }
        }
    }
    return $return;
}

/**
 * Creates an anchor tag, and adds tooltip code if needed
 *
 * @param string $title The title of the tooltip/text to link
 * @param string $url The link
 * @param string $tooltip The tooltip message
 * @param string $target The new links target
 * @return noreturn
 */
function CreateLink($title, $url, $tooltip="", $target="_self", $wide=false)
{
	if ($wide) {
        $class = "perm";
    } else {
        $class = "tip";
    }
    if(strlen($tooltip) == 0) {
		echo '<a href="' . $url . '" target="' . $target . '">' . $title .' </a>';
	} else {
		echo '<a href="' . $url . '" class="' . $class .'" title="' .  $title . ' :: ' .  $tooltip . '" target="' . $target . '">' . $title .' </a>';
	}
}

/**
 * Creates an anchor tag, and adds tooltip code if needed
 *
 * @param string $title The title of the tooltip/text to link
 * @param string $url The link
 * @param string $tooltip The tooltip message
 * @param string $target The new links target
 * @return URL
 */
function CreateLinkR($title, $url, $tooltip="", $target="_self", $wide=false, $onclick="")
{
	if ($wide) {
        $class = "perm";
    } else {
        $class = "tip";
    }
    if(strlen($tooltip) == 0) {
		return '<a href="' . $url . '" onclick="' . $onclick . '" target="' . $target . '">' . $title .' </a>';
	} else {
		return '<a href="' . $url . '" class="' . $class .'" title="' .  $title . ' :: ' .  $tooltip . '" target="' . $target . '">' . $title .' </a>';
	}
}

function HelpIcon($title, $text)
{
	return '<img border="0" align="absbottom" src="themes/' . SB_THEME .'/images/admin/help.png" class="tip" title="' .  $title . ' :: ' .  $text . '">&nbsp;&nbsp;';
}

/**
 * Allows the title of the page to change wherever the code is being executed from
 *
 * @param string $title The new title
 * @return noreturn
 */
function RewritePageTitle($title)
{
	$GLOBALS['title'] = RemoveCode($title);
}

/**
 * Build sub-menu
 *
 * @param array $el The array of elements for the menu
 * @return noreturn
 */
function SubMenu($el)
{
	$output = "";
	$first = true;
	foreach($el AS $e) {
        preg_match('/.*?&c=(.*)/', html_entity_decode($e['url']), $matches);
        if (!empty($matches[1])) {
            $c = $matches[1];
        }

        $output .= "<a class=\"nav_link".($first?" first":"").(isset($_GET['c'])&&$_GET['c']==$c?" active":"")."\" href=\"" . $e['url'] . "\">" . $e['title']. "</a>";
		$first = false;
	}
	$GLOBALS['NavRewrite'] = $output;
}

/**
 * Converts a flag bitmask into a string
 *
 * @param integer $mask The mask to convert
 * @return string
 */
function BitToString($mask, $masktype=0, $head=true)
{
	$string = "";
    if ($head) {
        $string .= '<span style="font-size:10px;color:#1b75d1;">'.t('webperms', 'Web Permissions').'</span><br>';
    }
    if($mask == 0) {
        $string .= '<i>'.t('webperms', 'None').'</i>';
        return $string;
    }
    if (($mask & ADMIN_LIST_ADMINS) != 0 || ($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'View admins').'<br />';
    }
    if (($mask & ADMIN_ADD_ADMINS) != 0 || ($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'Add admins').'<br />';
    }
    if (($mask & ADMIN_EDIT_ADMINS) != 0 || ($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'Edit admins').'<br />';
    }
    if (($mask & ADMIN_DELETE_ADMINS) != 0 || ($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'Delete admins').'<br />';
    }

    if (($mask & ADMIN_LIST_SERVERS) != 0 || ($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'View servers').'<br />';
    }
    if (($mask & ADMIN_ADD_SERVER) != 0 || ($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'Add servers').'<br />';
    }
    if (($mask & ADMIN_EDIT_SERVERS) != 0 || ($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'Edit servers').'<br />';
    }
    if (($mask & ADMIN_DELETE_SERVERS) != 0 || ($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'Delete servers').'<br />';
    }

    if (($mask & ADMIN_ADD_BAN) != 0 || ($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'Add bans').'<br />';
    }
    if (($mask & ADMIN_EDIT_OWN_BANS) != 0 && ($mask & ADMIN_EDIT_ALL_BANS) == 0) {
        $string .='&bull; '.t('webperms', 'Edit own bans').'<br />';
    }
    if (($mask & ADMIN_EDIT_GROUP_BANS) != 0 && ($mask & ADMIN_EDIT_ALL_BANS) == 0) {
        $string .= '&bull; '.t('webperms', 'Edit groups bans').'<br />';
    }
    if (($mask & ADMIN_EDIT_ALL_BANS) != 0 || ($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'Edit all bans').'<br />';
    }
    if (($mask & ADMIN_BAN_PROTESTS) != 0 || ($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'Ban protests').'<br />';
    }
    if (($mask & ADMIN_BAN_SUBMISSIONS) != 0 || ($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'Ban submissions').'<br />';
    }

    if (($mask & ADMIN_UNBAN_OWN_BANS) != 0 && ($mask & ADMIN_UNBAN) == 0) {
        $string .= '&bull; '.t('webperms', 'Unban own bans').'<br />';
    }
    if (($mask & ADMIN_UNBAN_GROUP_BANS) != 0 && ($mask & ADMIN_UNBAN) == 0) {
        $string .= '&bull; '.t('webperms', 'Unban group bans').'<br />';
    }
    if (($mask & ADMIN_UNBAN) != 0 || ($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'Unban all bans').'<br />';
    }
    if (($mask & ADMIN_DELETE_BAN) != 0 || ($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'Delete All bans').'<br />';
    }
    if (($mask & ADMIN_BAN_IMPORT) != 0 || ($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'Import bans').'<br />';
    }

    if (($mask & ADMIN_LIST_GROUPS) != 0 || ($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'List groups').'<br />';
    }
    if (($mask & ADMIN_ADD_GROUP) != 0 || ($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'Add groups').'<br />';
    }
    if (($mask & ADMIN_EDIT_GROUPS) != 0 || ($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'Edit groups').'<br />';
    }
    if (($mask & ADMIN_DELETE_GROUPS) != 0 || ($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'Delete groups').'<br />';
    }

    if (($mask & ADMIN_NOTIFY_SUB) != 0 || ($mask & ADMIN_NOTIFY_SUB) != 0) {
        $string .= '&bull; '.t('webperms', 'Submission email notifying').'<br />';
    }
    if (($mask & ADMIN_NOTIFY_PROTEST) != 0 || ($mask & ADMIN_NOTIFY_PROTEST) != 0) {
        $string .= '&bull; '.t('webperms', 'Protest email notifying').'<br />';
    }

    if (($mask & ADMIN_WEB_SETTINGS) != 0 || ($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'Web settings').'<br />';
    }

    if (($mask & ADMIN_LIST_MODS) != 0 || ($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'View mods').'<br />';
    }
    if (($mask & ADMIN_ADD_MODS) != 0 || ($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'Add mods').'<br />';
    }
    if (($mask & ADMIN_EDIT_MODS) != 0 || ($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'Edit mods').'<br />';
    }
    if (($mask & ADMIN_DELETE_MODS) != 0 || ($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'Delete mods').'<br />';
    }

    if (($mask & ADMIN_OWNER) != 0) {
        $string .= '&bull; '.t('webperms', 'Owner').'<br />';
    }

    return $string;
}


function SmFlagsToSb($flagstring, $head=true)
{

	$string = '';
	if ($head) {
        $string .= '<span style="font-size:10px;color:#1b75d1;">'.t('srvperms', 'Server Permissions').'</span><br>';
    }
    if(empty($flagstring)) {
        $string .= '<i>'.t('srvperms', 'None').'</i>';
        return $string;
    }
	if ((strstr($flagstring, 'a') || strstr($flagstring, 'z'))) {
        $string .= '&bull; [a] '.t('srvperms', 'Reserved slot').'<br />';
    }
    if ((strstr($flagstring, 'b') || strstr($flagstring, 'z'))) {
        $string .= '&bull; [b] '.t('srvperms', 'Generic admin').'<br />';
    }
    if ((strstr($flagstring, 'c') || strstr($flagstring, 'z'))) {
        $string .= '&bull; [c] '.t('srvperms', 'Kick').'<br />';
    }
    if ((strstr($flagstring, 'd') || strstr($flagstring, 'z'))) {
        $string .= '&bull; [d] '.t('srvperms', 'Ban').'<br />';
    }
    if ((strstr($flagstring, 'e') || strstr($flagstring, 'z'))) {
        $string .= '&bull; [e] '.t('srvperms', 'Unban').'<br />';
    }
    if ((strstr($flagstring, 'f') || strstr($flagstring, 'z'))) {
        $string .= '&bull; [f] '.t('srvperms', 'Slay').'<br />';
    }
    if ((strstr($flagstring, 'g') || strstr($flagstring, 'z'))) {
        $string .= '&bull; [g] '.t('srvperms', 'Map change').'<br />';
    }
    if ((strstr($flagstring, 'h') || strstr($flagstring, 'z'))) {
        $string .= '&bull; [h] '.t('srvperms', 'Change cvars').'<br />';
    }
    if ((strstr($flagstring, 'i') || strstr($flagstring, 'z'))) {
        $string .= '&bull; [i] '.t('srvperms', 'Run configs').'<br />';
    }
    if ((strstr($flagstring, 'j') || strstr($flagstring, 'z'))) {
        $string .= '&bull; [j] '.t('srvperms', 'Admin chat').'<br />';
    }
    if ((strstr($flagstring, 'k') || strstr($flagstring, 'z'))) {
        $string .='&bull; [k] '.t('srvperms', 'Start votes').'<br />';
    }
    if ((strstr($flagstring, 'l') || strstr($flagstring, 'z'))) {
        $string .='&bull; [l] '.t('srvperms', 'Password server').'<br />';
    }
    if ((strstr($flagstring, 'm') || strstr($flagstring, 'z'))) {
        $string .='&bull; [m] '.t('srvperms', 'RCON').'<br />';
    }
    if ((strstr($flagstring, 'n') || strstr($flagstring, 'z'))) {
        $string .='&bull; [n] '.t('srvperms', 'Enable Cheats').'<br />';
    }
    if ((strstr($flagstring, 'z'))) {
        $string .='&bull; [z] '.t('srvperms', 'Full Admin').'<br />';
    }

    if ((strstr($flagstring, 'o') || strstr($flagstring, 'z'))) {
        $string .='&bull; [o] '.t('srvperms', 'Custom flag 1').'<br />';
    }
    if ((strstr($flagstring, 'p') || strstr($flagstring, 'z'))) {
        $string .='&bull; [p] '.t('srvperms', 'Custom flag 2').'<br />';
    }
    if ((strstr($flagstring, 'q') || strstr($flagstring, 'z'))) {
        $string .='&bull; [q] '.t('srvperms', 'Custom flag 3').'<br />';
    }
    if ((strstr($flagstring, 'r') || strstr($flagstring, 'z'))) {
        $string .='&bull; [r] '.t('srvperms', 'Custom flag 4').'<br />';
    }
    if ((strstr($flagstring, 's') || strstr($flagstring, 'z'))) {
        $string .='&bull; [s] '.t('srvperms', 'Custom flag 5').'<br />';
    }
    if ((strstr($flagstring, 't') || strstr($flagstring, 'z'))) {
        $string .='&bull; [t] '.t('srvperms', 'Custom flag 6').'<br />';
    }


    //if(($mask & SM_DEF_IMMUNITY) != 0)
	//{
	//	$flagstring .="&bull; Default immunity<br />";
	//}
	//if(($mask & SM_GLOBAL_IMMUNITY) != 0)
	//{
	//	$flagstring .="&bull; Global immunity<br />";
	//}
	return $string;

}

function PrintArray($array)
{
	echo "<pre>";
	echo print_r($array, true);
	echo "</pre>";
}

function NextGid()
{
	$gid = $GLOBALS['db']->GetRow("SELECT MAX(gid) AS next_gid FROM `" . DB_PREFIX . "_groups`");
	return ($gid['next_gid']+1);
}
function NextSGid()
{
	$gid = $GLOBALS['db']->GetRow("SELECT MAX(id) AS next_id FROM `" . DB_PREFIX . "_srvgroups`");
	return ($gid['next_id']+1);
}
function NextSid()
{
	$sid = $GLOBALS['db']->GetRow("SELECT MAX(sid) AS next_sid FROM `" . DB_PREFIX . "_servers`");
	return ($sid['next_sid']+1);
}
function NextAid()
{
	$aid = $GLOBALS['db']->GetRow("SELECT MAX(aid) AS next_aid FROM `" . DB_PREFIX . "_admins`");
	return ($aid['next_aid']+1);
}

function trunc($text, $len, $byword=true)
{
	if (mb_strlen($text, 'UTF-8') <= $len) {
        return $text;
    }
    $text = $text." ";
    $text = mb_substr($text,0,$len, 'UTF-8');
    if ($byword) {
        $text = mb_substr($text, 0, mb_strrpos($text, ' ', 0, 'UTF-8'), 'UTF-8');
    }
    $text = $text."...";
    return $text;
}

function StripQuotes($str)
{
	$str = str_replace("'", "", $str);
	$str = str_replace('"', "", $str);
	return $str;
}

function CreateRedBox($title, $content)
{
	$text = '<div id="msg-red-debug" style="">
	<i><img src="./images/warning.png" alt="Warning" /></i>
	<b>' . $title .'</b>
	<br />
	' . $content . '</i>
    </div>';

	echo $text;
}
function CreateGreenBox($title, $contnet)
{
	$text = '<div id="msg-green-dbg" style="">
	<i><img src="./images/yay.png" alt="Yay!" /></i>
	<b>' . $title .'</b>
	<br />
	' . $contnet . '</i>
    </div>';

	echo $text;
}

function CreateQuote()
{
	global $userbank;
	$quote = array(
		array("Buy a new PC!", "Viper"),
		array("I'm not lazy! I just utilize technical resources!", "Brizad"),
		array("I need to mow the lawn", "sslice"),
		array("Like A Glove!", "Viper"),
		array("Your a Noob and You Know It!", "Viper"),
		array("Get your ass ingame", "Viper"),
		array("Mother F***ing Peices of Sh**", "Viper"),
		array("Shut up Bam", "[Everyone]"),
		array("Hi OllyBunch", "Viper"),
		array("Procrastination is like masturbation. Sure it feels good, but in the end you're only F***ing yourself!", "[Unknown]"),
		array("Rave's momma so fat she sat on the beach and Greenpeace threw her in", "SteamFriend"),
		array("Im just getting a beer", "Faith"),
		array("To be honest " . ($userbank->is_logged_in()?$userbank->getProperty('user'):'...') . ", I DONT CARE!", "Viper"),
		array("Yams", "teame06"),
		array("built in cheat 1.6 - my friend told me theres a cheat where u can buy a car door and run around and it makes u invincible....", "gdogg"),
		array("i just join conversation when i see a chance to tell people they might be wrong, then i quickly leave, LIKE A BAT", "BAILOPAN"),
		array("Lets just blame it on FlyingMongoose", "[Everyone]"),
		array("Don't step on that boom... mine...", "Recon"),
		array("Looks through sniper scope... Sit ;)", "Recon"),
		array("That plugin looks like something you found in a junk yard.", "Recon"),
		array("That's exactly what I asked you not to do.", "Recon"),
		array("Why are you wasting your time looking at this?", "Recon"),
		array("You must have better things to do with your time", "Recon"),
		array("I pity da fool", "Mr. T"),
		array("you grew a 3rd head?", "Tsunami"),
		array("I dont think you want to know...", "devicenull"),
		array("Sheep sex isn't baaaaaa...aad", "Brizad"),
		array("Oh wow, he's got skillz spelled with an 's'", "Brizad"),
		array("I'll get to it this weekend... I promise", "Brizad"),
		array("People do crazy things all the time... Like eat a Arby's", "Marge Simpson"),
		array("I wish my lawn was emo, so it would cut itself", "SirTiger"),
		array("Oh no! I've overflowed my balls!", "Olly"),
	);
	$num = rand(0, sizeof($quote)-1);
	return '"' . $quote[$num][0] . '" - <i>' . $quote[$num][1] . '</i>';
}

function CheckAdminAccess($mask)
{
	global $userbank;
	if(!$userbank->HasAccess($mask)) {
		RedirectJS("index.php?p=login&n=no_access");
		die();
	}
}

function RedirectJS($url)
{
	echo '<script>window.location = "' . $url .'";</script>';
}

function RemoveCode($text)
{
	return htmlspecialchars(strip_tags($text));
}

function SecondsToString($sec, $textual=true, $withSeconds = true)
{
	if($textual) {
		$div = array( 2592000, 86400, 3600, 60, 1 );
        if($withSeconds) {
            $desc = array(
                t('date', 'mo'),
                t('date', 'd'),
                t('date', 'hr'),
                t('date', 'min'),
                t('date', 'sec'),
            );
        } else {
            $desc = array(
                t('date', 'mo'),
                t('date', 'd'),
                t('date', 'hr'),
                t('date', 'min'),
            );
        }
		$ret = null;
		foreach($div as $index => $value) {
			$quotent = floor($sec / $value); //greatest whole integer
			if($quotent > 0 && !empty($desc[$index])) {
                $ret .= "{$quotent} {$desc[$index]}, ";
				$sec %= $value;
			}
		}
		return substr($ret,0,-2);
	} else {
		$hours = floor ($sec / 3600);
		$sec -= $hours * 3600;
		$mins = floor ($sec / 60);
		$secs = $sec % 60;
        return "{$hours}:{$mins}:{$secs}";
	}
}

function FetchIp($ip)
{
    unset($_SESSION['CountryFetchHndl']);
	$ip = sprintf('%u', ip2long($ip));
	if(!isset($_SESSION['CountryFetchHndl']) || !is_resource($_SESSION['CountryFetchHndl'])) {
		$handle = fopen(INCLUDES_PATH.'IpToCountry.csv', "r");
		$_SESSION['CountryFetchHndl'] = $handle;
	} else {
		$handle = $_SESSION['CountryFetchHndl'];
		rewind($handle);
	}

	if (!$handle) {
        return "zz";
    }

    while (($ipdata = fgetcsv($handle, 4096)) !== FALSE) {
		// If line is comment or IP is out of range
		if ($ipdata[0][0] == '#' || $ip < $ipdata[0] || $ip > $ipdata[1]) {
            continue;
        }

        if (empty($ipdata[4])) {
            return "zz";
        }
        return $ipdata[4];
	}

	return "zz";
}

function PageDie()
{
	include TEMPLATES_PATH.'footer.php';
	die();
}

function GetMapImage($map)
{
	if (file_exists(SB_MAP_LOCATION . $map . ".jpg")) {
        return "images/maps/" . $map . ".jpg";
    } else {
        return "images/maps/nomap.jpg";
    }
}

function CheckExt($filename, $ext)
{
	$filename = str_replace(chr(0), '', $filename);
	$path_info = pathinfo($filename);
    $fileExt = strtolower($path_info['extension']);
    if(is_array($ext)) {
        $ext = array_map(function($ext){
            return strtolower($ext);
        }, $ext);
        return in_array($fileExt, $ext);
    }
	return $fileExt == strtolower($ext);
}

function ShowBox($title, $msg, $color, $redir="", $noclose=false)
{
	echo "<script>ShowBox('$title', '$msg', '$color', '$redir', $noclose);</script>";
}
function ShowBox_ajx($title, $msg, $color, $redir="", $noclose=false, &$response)
{
	$response->AddScript("ShowBox('$title', '$msg', '$color', '$redir', $noclose);");
}

function PruneBans()
{
	global $userbank;

	$res = $GLOBALS['db']->Execute('UPDATE `'.DB_PREFIX.'_bans` SET `RemovedBy` = 0, `RemoveType` = \'E\', `RemovedOn` = UNIX_TIMESTAMP() WHERE `length` != 0 and `ends` < UNIX_TIMESTAMP() and `RemoveType` IS NULL');
	$GLOBALS['db']->Execute("UPDATE `".DB_PREFIX."_protests` SET archiv = '3', archivedby = ".($userbank->GetAid()<0?0:$userbank->GetAid())." WHERE archiv = '0' AND bid IN((SELECT bid FROM `".DB_PREFIX."_bans` WHERE `RemoveType` = 'E'))");
	$GLOBALS['db']->Execute('UPDATE `'.DB_PREFIX.'_submissions` SET archiv = \'3\', archivedby = '.($userbank->GetAid()<0?0:$userbank->GetAid()).' WHERE archiv = \'0\' AND (SteamId IN((SELECT authid FROM `'.DB_PREFIX.'_bans` WHERE `type` = 0 AND `RemoveType` IS NULL)) OR sip IN((SELECT ip FROM `'.DB_PREFIX.'_bans` WHERE `type` = 1 AND `RemoveType` IS NULL)))');
    return $res?true:false;
}


function GetSVNRev()
{
	preg_match('/\\$Rev:[\\s]+([\\d]+)/', SB_REV, $rev, PREG_OFFSET_CAPTURE);
	return (int)$rev[1][0];
}


// Function by Luman (http://snipplr.com/users/luman)
function array_qsort(&$array, $column=0, $order=SORT_ASC, $first=0, $last= -2)
{
    // $array  - the array to be sorted
    // $column - index (column) on which to sort
    //          can be a string if using an associative array
    // $order  - SORT_ASC (default) for ascending or SORT_DESC for descending
    // $first  - start index (row) for partial array sort
    // $last  - stop  index (row) for partial array sort
    // $keys  - array of key values for hash array sort

    $keys = array_keys($array);
    if ($last == -2) {
        $last = count($array) - 1;
    }
    if($last > $first) {
        $alpha = $first;
        $omega = $last;
        $key_alpha = $keys[$alpha];
        $key_omega = $keys[$omega];
        $guess = $array[$key_alpha][$column];
        while($omega >= $alpha) {
            if($order == SORT_ASC) {
                while($array[$key_alpha][$column] < $guess) {
                    $alpha++;
                    $key_alpha = $keys[$alpha];
                }
                while($array[$key_omega][$column] > $guess) {
                    $omega--;
                    $key_omega = $keys[$omega];
                }
            } else {
                while($array[$key_alpha][$column] > $guess) {$alpha++; $key_alpha = $keys[$alpha]; }
                while($array[$key_omega][$column] < $guess) {$omega--; $key_omega = $keys[$omega]; }
            }
            if ($alpha > $omega) {
                break;
            }
            $temporary = $array[$key_alpha];
            $array[$key_alpha] = $array[$key_omega];
            $alpha++;
            $key_alpha = $keys[$alpha];
            $array[$key_omega] = $temporary;
            $omega--;
            if ($omega > 0) {
                $key_omega = $keys[$omega];
            }
        }
        array_qsort ($array, $column, $order, $first, $omega);
        array_qsort ($array, $column, $order, $alpha, $last);
    }
}


function getDirectorySize($path)
{
	$totalsize = 0;
	$totalcount = 0;
	$dircount = 0;
	if ($handle = opendir ($path)) {
		while (false !== ($file = readdir($handle))) {
			$nextpath = $path . '/' . $file;
			if ($file != '.' && $file != '..' && !is_link ($nextpath)) {
				if (is_dir ($nextpath)) {
					$dircount++;
					$result = getDirectorySize($nextpath);
					$totalsize += $result['size'];
					$totalcount += $result['count'];
					$dircount += $result['dircount'];
				} elseif (is_file ($nextpath)) {
					$totalsize += filesize ($nextpath);
					$totalcount++;
				}
			}
		}
	}
	closedir ($handle);
	$total['size'] = $totalsize;
	$total['count'] = $totalcount;
	$total['dircount'] = $dircount;
	return $total;
}

function sizeFormat($value)
{
    $units=array('B','KB','MB','GB','TB');
    for ($i = 0; 1024 <= $value; $i++) {
        $value = $value / 1024;
    }
    return round($value, 2).$units[$i];
}

function check_email($email) {
    return is_valid_email($email);
}

//function to check for multiple steamids on one server.
// param $steamids needs to be an array of steamids.
//returns array('STEAM_ID_1' => array('name' => $name, 'steam' => $steam, 'ip' => $ip, 'time' => $time, 'ping' => $ping), 'STEAM_ID_2' => array()....)
function checkMultiplePlayers($sid)
{
	$serv = $GLOBALS['db']->CacheGetRow(86400,
        "SELECT
            `srv`.`ip` `ip`,
            `srv`.`port` `port`,
            `srv`.`rcon` `rcon`,
            `mod`.`modfolder` `game`
        FROM
            `".DB_PREFIX."_servers` `srv`
        LEFT JOIN
            `".DB_PREFIX."_mods` `mod`
        ON
            `mod`.`mid` = `srv`.`modid`
        WHERE `srv`.`sid` = '".intval($sid)."';");
	if(empty($serv['rcon'])) {
		return array();
	}

    $status = SendRconSilent('status', $sid);
    if($status === false) {
		return array();
	}
    $players = getPlayersFromStatus($status, $serv['game']);
    return $players;
}

/**
 * Parse response of `status` command
 * @param string $game Game type (A "modfolder" param in game MOD)
 * @return array|null
 */
function getParseInfoByGame($game) {
    $patterns = array(
        'cstrike' => array(
            'pattern' => '/\#\s+([0-9]{1,5})\s+' // ID
                . '"(.*)"\s+'// Nick
                . '(STEAM_[0-9]:[0-9]:[0-9]{5,15})\s+' // SteamID
                . '([0-9\:]+)\s+' // Time
                . '([0-9]{1,5})\s+' // Ping
                . '([0-9]{1,5})\s+' // Loss
                . '(\w+)\s+' //State
                . '(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}):/',
            'indexes' => array(
                'name' => 2,
                'steamid' => 3,
                'time' => 4,
                'ping' => 5,
                'ip' => 8
            )
        ),
        'csgo' => array(
            'pattern' => '/\#\s+([0-9]{1,5})\s+' // ID
                . '([0-9]{1,5})\s+' // ID
                . '"(.*)"\s+'// Nick
                . '(STEAM_[0-9]:[0-9]:[0-9]{5,15})\s+' // SteamID
                . '(.{2,11})\s+' // Time
                . '([0-9]{1,5})\s+' // Ping
                . '([0-9]{1,5})\s+' // Loss
                . '(\w+)\s+' //State
                . '(\d+)\s+' // Rate
                . '(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}):/', // IP
            'indexes' => array(
                'name' => 3,
                'steamid' => 4,
                'time' => 5,
                'ping' => 6,
                'ip' => 10
            )
        ),
    );
    
    return isset($patterns[$game]) ? $patterns[$game] : null;
}

/**
 * Получает игроков сервера из команды status
 * @param string $status Результат команды status
 * @param string $game Тип игры
 * @return array массив с игроками, с SteamID игроков в качестве индексов
 */
function getPlayersFromStatus($status, $game) {
    $status = explode(PHP_EOL, trim($status));
    $players = array();
    $data = getParseInfoByGame($game);
    if($data === null) {
        return array();
    }
    
    foreach($status as $str) {
        $playersMatch = preg_match($data['pattern'], $str, $playerMatch);
        if($playersMatch) {
            $players[$playerMatch[$data['indexes']['steamid']]] = array(
                'name' => $playerMatch[$data['indexes']['name']],
                'steamid' => $playerMatch[$data['indexes']['steamid']],
                'time' => $playerMatch[$data['indexes']['time']],
                'ping' => $playerMatch[$data['indexes']['ping']],
                'ip' => $playerMatch[$data['indexes']['ip']],
            );
        }
    }
    return $players;
}


function SBDate($format, $timestamp="")
{
    if(version_compare(PHP_VERSION, "5") != -1) {
        if($GLOBALS['config']['config.summertime'] == "1") {
            $str = date("r", $timestamp);
            $date = new DateTime($str);
            $date->modify("+1 hour");
            return $date->format($format);
        } else if (empty($timestamp)) {
            return date($format);
        }
    } else {
        if($GLOBALS['config']['config.summertime'] == "1") {
            $summertime = 3600;
        } else {
            $summertime = 0;
        }
        if(empty($timestamp)) {
            $timestamp = time() + SB_TIMEZONE*3600 + $summertime;
        } else {
            $timestamp = $timestamp + SB_TIMEZONE*3600 + $summertime;
        }
    }
	return date($format, $timestamp);
}

/**
* Converts a SteamID to a FriendID
*
* @param string $authid the steamid to convert
* @return string
*/
function SteamIDToFriendID($authid)
{
	$friendid = $GLOBALS['db']->CacheGetRow(86400, "SELECT CAST(MID('".$authid."', 9, 1) AS UNSIGNED) + CAST('76561197960265728' AS UNSIGNED) + CAST(MID('".$authid."', 11, 10) * 2 AS UNSIGNED) AS friend_id");
	return $friendid["friend_id"];
}

/**
* Converts a FriendID to a SteamID
*
* @param string $friendid the friendid to convert
* @return string
*/
function FriendIDToSteamID($friendid)
{

	$steamid = $GLOBALS['db']->CacheGetRow(86400, "SELECT CONCAT(\"STEAM_0:\", (CAST('".$friendid."' AS UNSIGNED) - CAST('76561197960265728' AS UNSIGNED)) % 2, \":\", CAST(((CAST('".$friendid."' AS UNSIGNED) - CAST('76561197960265728' AS UNSIGNED)) - ((CAST('".$friendid."' AS UNSIGNED) - CAST('76561197960265728' AS UNSIGNED)) % 2)) / 2 AS UNSIGNED)) AS steam_id;");
	return $steamid['steam_id'];
}

/**
* Gets the friendid from a custom user id
*
* @param string $comid the customid to get the friendid for
* @return string
*/
function GetFriendIDFromCommunityID($comid)
{
	$raw = @file_get_contents("http://steamcommunity.com/id/".$comid."/?xml=1");
	preg_match("/<privacyState>([^\]]*)<\/privacyState>/", $raw, $status);
	if(($status && $status[1] != "public") || strstr($raw, "</profile>")) {
		$raw = str_replace("&", "", $raw);
		$raw = strip_31_ascii($raw);
		$raw = utf8_encode($raw);
		$xml = simplexml_load_string($raw);
		$result = $xml->xpath('/profile/steamID64');
		return (string)$result[0];
	}
	return false;
}
function GetCommunityName($steamid)
{
	$friendid = SteamIDToFriendID($steamid);
	$result = get_headers("http://steamcommunity.com/profiles/".$friendid."/", 1);
	$raw = file_get_contents(($result["Location"]!=""?$result["Location"]:"http://steamcommunity.com/profiles/".$friendid."/")."?xml=1");
	if(strstr($raw, "</profile>")) {
		$raw = str_replace("&", "", $raw);
        $raw = strip_31_ascii($raw);
		$raw = utf8_encode($raw);
		$xml = simplexml_load_string($raw);
		$result = $xml->xpath('/profile/steamID');
		return (string)$result[0];
	}
	return "";
}

function SendRconSilent($command, $sid)
{
	$serv = $GLOBALS['db']->CacheGetRow(86400, "SELECT ip, port, rcon FROM ".DB_PREFIX."_servers WHERE sid = '".$sid."';");
	if(empty($serv['rcon'])) {
		return false;
	}
    
    require_once INCLUDES_PATH . 'SourceQuery'.DIRECTORY_SEPARATOR.'SourceQuery.class.php';
    $query = new SourceQuery;
    try {
        $query->Connect($serv['ip'], $serv['port'], 1);
        $query->SetRconPassword($serv['rcon']);
        $ret = $query->Rcon($command);
    } catch(Exception $e) {
        new CSystemLog(
            'w',
            t('log', 'No connection to the server'),
            t('log', 'No connection to the server.<br>Server: [[ip]]:[[port]]<br>Errormessage: [[error]]', array(
                '[[ip]]' => $serv['ip'],
                '[[port]]' => $serv['port'],
                '[[error]]' => $e->getMessage(),
            ))
        );
        return false;
    }
    return $ret;
}

/* Function to check if a needle is inside a 2 layered recursive array
* like the one from ADODB->GetAll
* @param string $needle The string to search for
* @param array $array The array to search in
* @return boolean
*/
function in_array_dim($needle, $array)
{
	foreach($array as $secarray) {
		foreach($secarray as $part) {
			if ($part == $needle) {
                return true;
            }
        }
	}
	return false;
}

// Strip all undisplayable chars from a string. e.g.  or 
function strip_31_ascii($string)
{
	for ($i = 0; $i < 32; $i++) {
        $string = str_replace(chr($i), "", $string);
    }
    return $string;
}

function t($section, $message, $params = array()) {
    global $language;
    return $language->translate($section, $message, $params);
}

/**
 * 
 * @param string $ip
 * @param int $port
 * @return \SourceQuery|boolean
 */
function QueryInstance($ip, $port) {
    require_once INCLUDES_PATH . 'SourceQuery'.DIRECTORY_SEPARATOR.'SourceQuery.class.php';
    $query = new SourceQuery;
    try {
        $query->Connect($ip, $port, 1);
    } catch(Exception $e) {
        new CSystemLog(
            'w',
            t('log', 'No connection to the server'),
            t('log', 'No connection to the server.<br>Server: [[ip]]:[[port]]<br>Errormessage: [[error]]', array(
                '[[ip]]' => $ip,
                '[[port]]' => $port,
                '[[error]]' => $e->getMessage(),
            ))
        );
        return false;
    }
    return $query;
}

function getLinks($inSettings = false) {
    global $userbank;
    if(!$userbank->is_logged_in()) {
        $where = " AND `hideGuests` = 0";
    } elseif($inSettings) {
        $where = " AND `url` NOT REGEXP 'http:\/\/'";
    } else {
        $where = "";
    }
    
    $links = $GLOBALS['db']->CacheGetAll(86400, "SELECT * FROM `".DB_PREFIX."_links` WHERE `active` = 1{$where} ORDER BY `position` ASC, `id` ASC");
    foreach($links as $key => $link) {
        $links[$key]['label'] = t('mainmenu', $link['label']);
        $links[$key]['title'] = t('mainmenu', $link['title']);
    }
    return $links;
}

function pageNotFound() {
    RewritePageTitle(t('layout','Error'));
    header('HTTP/1.0 404 Not Found');
    return t('layout','<h1>The requested page was not found</h1>');
}

function getMainPagePath() {
    $mainpage = $GLOBALS['db']->CacheGetRow(86400, "SELECT * FROM `".DB_PREFIX."_links` WHERE `id` = " . intval($GLOBALS['config']['config.defaultpage']));
    if($mainpage) {
        RewritePageTitle(RemoveCode($mainpage['label']));
        preg_match('/^index\.php\?p=([a-z]+)$/', $mainpage['url'], $match);
        $page = TEMPLATES_PATH . "/page.{$match[1]}.php";
        if(!is_file($page)) {
            $page = TEMPLATES_PATH . "/page.home.php";
        }
    } else {
        RewritePageTitle(t('mainmenu', 'Dashboard'));
        $page = TEMPLATES_PATH . "/page.home.php";
    }
    
    return $page;
}

function steamidVersion($steam) {
    if(is_numeric($steam)) {
        return 64;
    }
    if(strtolower(substr($steam, 0, 5)) == 'steam') {
        return 1;
    }
    if($steam{0} == '[') {
        return 3;
    }
    return false;
}