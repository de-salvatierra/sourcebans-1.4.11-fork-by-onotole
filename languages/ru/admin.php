<?php
return array(
    'You dont have admin. Be gone!' => 'У Вас недостаточно прав',
    'Ban Deleted' => 'Бан удален',
    'The ban has been deleted from SourceBans' => 'Бан был успешно удален из SourceBans',
    'Player Unbanned' => 'Игрок разбанен',
    'The Player has been unbanned from SourceBans' => 'Игрок был успешно разбанен',
    
    'Never' => 'Никогда',
    'Forever' => 'Навсегда',
    'Expired' => 'Истек',
    
    // Titles (ALL ADMIN PAGES HERE!!)
    'Administration' => 'Админцентр',
    
    'Group Management' => 'Управление группами',
    
    'Edit Groups' => 'Редактировать группы',
    
    'Admin Management' => 'Управление админами',
    'Edit Admin Groups' => 'Редактировать группы админа',
    'Edit Admin Details' => 'Редактировать детали админа',
    'Edit Admin Permissions' => 'Редактировать разрешения админа',
    'Edit Server Access' => 'Редактировать доступ к серверу',
    
    'Server Management' => 'Управление серверами',
    'Edit Server' => 'Редактировать сервер',
    'Server RCON' => 'RCON консоль',
    'Database Config' => 'Настройки для databases.cfg',
    'Server Admins' => 'Администраторы сервера',
    
    'Manage Mods' => 'Управление модами',
    'Edit Mod Details' => 'Редактировать мод',
    
    'Bans' => 'Баны',
    'Edit Ban Details' => 'Редактировать детали бана',
    'Email' => 'E-mail',
    
    'SourceBans Settings' => 'Настройки SourceBans',
    
    'Manage links' => 'Ссылки сайта',
    
    // admin.uploaddemo.php
    'You don\'t have access to this!' => 'У вас недостаточно прав для данной операции!',
    'Demo Uploaded' => 'Демо загружено',
    'The file must have the following extension: [[extensions]]' => 'Файл должен иметь одно из следующих расширений: [[extensions]]',
    // page_uploadfile.tpl
    'Upload File : SourceBans' => 'SourceBans : Загрузить файл',
    'Plese select the file to upload. The file must either be "[[formats]]" file format' => 'Выберите файл для загрузки.<br>Файл должен иметь один из следующих форматов:<br>[[formats]]',
    'Upload' => 'Загрузить',
    'Upload Demo' => 'Загрузить демку',
    // page.uploadicon.php
    'The file must have the following extension: [[extensions]]' => 'Загружаемый файл может иметь только следующие расширения: [[extensions]]',
    'Upload Icon' => 'Загрузить иконку',
    // page.uploadmapimg.php
    'File must be jpg filetype.' => 'Расширение файла может быть только jpeg',
    'Upload Mapimage' => 'Загрузить картинку',
    
    // page_admin.tpl
    'Please select an option to administer.' => 'Выберите пункт администрирования',
    'Admin Settings' => 'Настройки админов',
    'Server Settings' => 'Управление серверами',
    'Edit Bans' => 'Изменить баны',
    'Bans' => 'Управление банами',
    'Edit Groups' => 'Группы',
    'Group Settings' => 'Настройки групп',
    'SourceBans Settings' => 'Настройки SourceBans',
    'Webpanel Settings' => 'Настройки панели',
    'Mods' => 'Моды',
    'Manage Mods' => 'Управление модами',
    'Version Information' => 'Версия',
    'Ban Information' => 'Информация о банах',
    'Admin Information' => 'Информация об админах',
    'Latest release' => 'Последний релиз',
    'Please Wait...' => 'Ждите...',
    'Total admins' => 'Всего админов',
    'Total bans' => 'Всего банов',
    'Latest SVN' => 'Последний SVN',
    'Connection blocks' => 'Заблокировано игроков',
    'Total demo size' => 'Общий размер демо',
    'Server Information' => 'Серверы',
    'Protest Information' => 'Протесты',
    'Submission Information' => 'Предложения',
    'Total Servers' => 'Всего серверов',
    'Total protests' => 'Всего протестов',
    'Total submissions' => 'Всего предложений',
    'Archived protests' => 'Протестов в архиве',
    'Archived submissions' => 'Предложений в архиве',
);