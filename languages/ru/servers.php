<?php
return array(
    'Name' => 'Ник',
    'Score' => 'Счет',
    'Time' => 'Время',
    'Player Commands' => 'Действия',
    'Kick' => 'Кикнуть',
    'Ban' => 'Забанить',
    'View Profile' => 'Профиль',
    'Send Message' => 'Сообщение',
    'Error connecting' => 'Ошибка соединения',
    'Which ports does the SourceBans webpanel require to be open?' => 'Проверьте, открыты ли порты (UDP 27000-27999)',
    'Help' => 'Помощь',
    'Can not kick [[player]]. No RCON password!' => 'Невозможно кикнуть игрока "[[player]]". Не установлен RCON пароль',
    'Can not ban [[player]]. No RCON password!' => 'Невозможно забанить игрока "[[player]]". Не установлен RCON пароль',
    'Can not kick [[player]]. Wrong RCON password!' => 'Невозможно кикнуть игрока "[[player]]". Неверный RCON пароль',
    'Can not kick [[player]]. Player is immune!' => 'Невозможно кикнуть игрока "[[player]]". У него иммунитет',
    'Can not get player info for [[player]], Player is not on the server ([[server]]) anymore!' => 'Невозможно кикнуть игрока "[[player]]". Его нет на сервере ([[server]])',
    
    
    
    
    
    
    'Can not connect to server. See system log for details' => 'Нет соединения с сервером. Подробности смотрите в системном логе',
    'You have been kicked by this server, check [[infolink]] for more info.' => 'Вы были кикнуты с сервера. Подробности по ссылке: [[infolink]]',
    'Can not get playerinfo for [[player]]. No RCON password!' => 'Ошибка получения информации об игроке [[player]]. Не указан RCON пароль',
    'Can not get playerinfo for [[player]]. Wrong RCON password!' => 'Ошибка получения информации об игроке [[player]]. Невернвй RCON пароль',
    'Can not get playerinfo for [[player]]. Player not on the server anymore!' => 'Ошибка получения информации об игроке [[player]]. Игрока нет на сервере',
    'Generating Community Profile link for [[player]], please wait...' => 'Генерируется ссылка на профиль сообщества Steam игрока [[player]]. Подождите...',
    'Done.' => 'Готово',
    'Watch the profile' => 'Ссылка на профиль',
    'Player [[player]] has been kicked from the server.' => 'Игрок [[player]] кикнут с сервера',
    'Player kicked' => 'Игрок кикнут',
    
    // Title
    
    
    // page_servers.tpl
    'Hint: Rightclick on a player to open a context menu with options to kick, ban or contact the player directly.' => 'Подсказка: кликните правой кнопкой на игроке, чтобы кикнуть его, забанить или написать ему сообщение',
    'MOD' => 'МОД',
    'Servers List' => 'Список серверов',
    'OS' => 'ОС',
    'VAC' => '',
    'N/A' => '',
    'Hostname' => 'Название',
    'Players' => 'Игроки',
    'Map' => 'Карта',
    'Querying Server Data...' => 'Получение информации...',
    'IP:Port - [[ip]]:[[port]]' => 'IP:Порт - [[ip]]:[[port]]',
    'Connect' => 'Подключиться',
    'Refresh' => 'Обновить',
    'No players in the server' => 'На сервере нет игроков',
    'Reloading..' => 'Обновление...',
    '<b>Refreshing the Serverdata...</b><br><i>Please Wait!</i>' => '<b>Обновение информации с сервера...</b><br><i>Подалуйста, подождите!</i>',
    
    // AJAX
    'Send Message' => 'Отправить сообщение',
    'Please type the message you want to send to selected player<br />You need to have basechat.smx enabled as we use <i>&lt;sm_psay&gt;</i>.' => 'Введите сообщение, которое вы хотите отправить выбранному игроку.<br />На сервере должен быть установлен плагин basechat.smx, чтобы использовать команду <i>&lt;sm_psay&gt;</i>.',
    'Cancel' => 'Отмена',
    'Please type your message.' => 'Введите текст сообщения',
    'Are you sure you want to kick player [[player]]?' => 'Кикнуть игрока [[player]]?',
    'View Community Profile' => 'Профиль сообщества Steam',
    'Generating Community Profile link for [[player]], please wait...' => 'Генерируется ссылка на профиль сообщества Steam игрока [[player]]. Подождите...',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
);
