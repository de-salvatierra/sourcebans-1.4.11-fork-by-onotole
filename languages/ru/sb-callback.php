<?php
return array(
    
    'Error' => 'Ошибка',
    
    
    'Error: You cannot delete the owner.' => 'Ошибка: нельзя удалить главного админа',
    'Admin Deleted' => 'Админ удален',
    'The selected admin has been deleted from the database' => 'Выбранный админ был успешно удален из базы',
    'There was an error removing the admin from the database, please check the logs' => 'Произошла ошибка при удалении админа. Смотрите логи',
    'Server Group Name' => 'Имя группы серверов',
    'Please type the name of the new group you wish to create.' => 'Введите имя новой группы серверов',
    'Group Name' => 'Имя группы',
    'You must type a name for the admin.' => 'Введите имя админа',
    'An admin name can not contain a " \' ".' => 'Имя админа не может содержать точку',
    'An admin with this name already exists' => 'Админ с таким именем уже существует',
    'Admin [[admin]] already uses this Steam ID.' => 'Админ с таким SteamID уже есть в базе ([[admin]])',
    'You must type an e-mail address.' => 'Введите E-mail адрес',
    'This email address is already being used by [[admin]]' => 'E-mail адрес уже используется админом [[admin]]',
    'You must type a password.' => 'Введите пароль',
    'Your password must be at-least [[minpass]] characters long.' => 'Пароль должен содержать минимум [[minpass]] символов',
    'You must confirm the password' => 'Введите подтверждение пароля',
    'Your passwords don\'t match' => 'Пароли не совпадают',
    'You must type a server password or uncheck the box.' => 'Введите серверный пароль, или снимите эту галочку',
    'You must choose a group.' => 'Выберите группу',
    'Group name cannot contain a \',\'' => 'Имя группы не может содержать запятой',
    'You need to type a name for the new group.' => 'Введите имя новой группы',
    'There are some errors in your input. Please correct them.' => 'Обнаружены ошибки. Проверьте введенные данные',
    'The admin has been added successfully' => 'Админ успешно добавлен',
    'Admin Added' => 'Админ добавлен',
    'User NOT Added' => 'Пользователь не добавлен',
    'The admin failed to be added to the database. Check the logs for any SQL errors.' => 'Произошла ошибка при добавлении админа. Смотрите логи',
    'Player kicked' => 'Игрок кикнут',
    'Can\'t kick [[player]]. Player not on the server anymore!' => 'Невозможно кикнуть игрока [[player]]: игрок ушел с сервера',
    'Can\'t kick [[player]]. Player is immune!' => 'Невозможно кикнуть игрока: [[player]]. У него иммунитет',
    'Player [[player]] has been kicked from the server.' => 'Игрок [[player]] был кикнуть с сервера',
    'No RCON password for server [[server]]!' => 'Для сервера [[server]] не установлен RCON пароль',
    'Can\'t get player info for [[player]], Player is not on the server ([[server]]) anymore!' => 'Невозможно получить информацию об игроке [[player]]. Его нет на сервере [[server]]',
    'Wrong RCON password for server [[server]]!' => 'Неверный RCON пароль для сервера [[server]]',
    
    
    
    'Permissions updated' => 'Права доступа обновлены',
    'Admins have to have a password and email set in order to get web permissions.<br /><a href="index.php?p=admin&c=admins&o=editdetails&id=[[adminid]]" title="Edit Admin Details">Set the details</a> first and try again.' => 'Администраторы должны иметь пароль и адрес электронной почты для того, чтобы иметь веб-разрешения.<br /><a href="index.php?p=admin&c=admins&o=editdetails&id=[[adminid]]" title="Редактировать детали админа">Отредактируйте админа</a> перед тем, как задать ему веб разрешения.',
    'The user`s permissions have been updated successfully' => 'Права доступа для админа были успешно обновлены',
    'There already is an override with name [[name]] from the selected type..' => '',
    'Group updated' => 'Группа обновлена',
    'The group has been updated successfully' => 'Группа была успешно отредактирована',
    'There is no email to send to supplied.' => 'Нет электронной почты для отправки сообщения',
    'A new release is available.' => 'Доступна новая версия',
    'Error retrieving latest release.' => 'Ошибка получения версии',
    'You have the latest release.' => 'Вы используете последнюю версию',
    'A new SVN revision is available.' => 'Доступная новая SVN версия',
    'You have the latest SVN revision.' => 'Вы используете последнюю SVN версию',
    'Error retrieving latest svn revision.' => 'Ошибка получения SVN версии',
    'Invalid theme selected.' => 'Выбрана неверная тема',
    'Bad theme selected.' => 'Выбрана неверная тема',
    'Apply Theme' => 'Применить тему',
    'Cache cleared.' => 'Кэш очищен',
    'failed: Can\'t connect' => 'ошибка: Нет соединения',
    'failed: Wrong rcon password' => 'ошибка: Неверный RCON пароль',
    'Error parsing the group url.' => 'Ошибка разбора URL группы',
    
    
    
    
    'There was an error retrieving the group data. <br>Maybe the player isn\'t member of any group or his profile is private?<br><a href="http://steamcommunity.com/profiles/[[friendid]]/" title="Community profile" target="_blank">Community profile</a>' => 'Ошибка получения информации о группе. Возможно игок не является участником этой группы, или его профиль скрыт.<br /><a href="http://steamcommunity.com/profiles/[[friendid]]/" title="Профиль сообщества" target="_blank">Профиль сообщества</a>',
    'No groups...' => 'Нет групп',
    'Banned [[banned]]/[[total]] friends of "[[name]]".<br>[[bannedbefore]] were banned already.' => 'Забанено [[banned]] из [[total]] друзей игрока "[[name]]".<br>[[bannedbefore]] было забанено ранее.',
    'Can\'t get playerinfo for [[player]]. No RCON password!' => 'Невозможно получить информацию об игроке [[player]]. Не задан RCON пароль',
    'Can\'t get playerinfo for [[player]]. Wrong RCON password!' => 'Невозможно получить информацию об игроке [[player]]. RCON пароль неверный',
    'Can\'t send message to "[[player]]". No RCON password!' => 'Ошибка отправки сообщения игроку "[[player]]". Не задан RCON пароль',
    'Can\'t send message to "[[player]]". Wrong RCON password!' => 'Ошибка отправки сообщения игроку "[[player]]". RCON пароль неверный',
    'Message Sent' => 'Сообщение отправлено',
    'The message has been sent to player [[player]] successfully!' => 'Сообщение было успешно отправлено игроку [[player]]',
    
    'Group Deleted' => 'Группа удалена',
    'The selected group has been deleted from the database' => 'Выбранная группа была удалена из базы',
);
