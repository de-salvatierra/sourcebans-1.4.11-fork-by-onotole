<?php
return array(
    // Admincenter
    'Admin' => 'Админцентр',
    'Admin management' => 'Управление админами',
    'Server management' => 'Управление серверами',
    'Ban management' => 'Управление банами',
    'Group Management' => 'Управление группами',
    'SourceBans settings' => 'Настройки SourceBans',
    'Mod management' => 'Управление модами',
    'Links management' => 'Управление Ссылками',
    
    // Main
    'Home' => 'Главная',
    'Banlist' => 'Банлист',
    'Login form' => 'Форма входа',
    'Restore password' => 'Сброс пароля',
    'Protest a ban' => 'Опротестовать бан',
    'Servers list' => 'Серверы',
    'Submit a ban' => 'Предложить бан',
    'Profile' => 'Профиль',
    '' => '',
);
