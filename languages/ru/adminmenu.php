<?php
return array(
    'Admins' => 'Админы',
    'Bans' => 'Баны',
    'Servers' => 'Серверы',
    'Groups' => 'Группы',
    'Settings' => 'Настройки',
    'Mods' => 'Моды',
);
