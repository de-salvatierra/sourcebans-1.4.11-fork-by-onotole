<?php

return array(
    // Edit server
    // admin.edit.server.php
    'Error' => 'Ошибка',
    
    'Are you sure you want to delete the server?' => 'Удалить сервер?',
    
    
    
    // Server DB settings
    
    
    'failed: No rcon password set' => 'ошибка: Не задан RCON пароль',
    'Done' => 'Выполнено',
    'successful' => 'успешно',
    'No servers to check.' => 'Нет серверов',
    'Can not send message to "[[player]]". No RCON password!' => 'Ошибка отправления сообщения игроку "[[player]]". Не установлен RCON пароль',
    'Can not send message to "[[player]]". Wrong RCON password!' => 'Ошибка отправления сообщения игроку "[[player]]". Неверный RCON пароль',
    'Message Sent' => 'Сообщение отправлено',
    'The message has been sent to player [[player]] successfully!' => 'Сообщение для игрока [[player]] успешно отправлено',
);
