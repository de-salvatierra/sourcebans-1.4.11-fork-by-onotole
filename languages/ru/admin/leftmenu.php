<?php
return array(
    'List groups' => 'Группы',
    'Add a group' => 'Добавить группу',
    'Back' => 'Назад',
    'List admins' => 'Админы',
    'Add new admin' => 'Добавить',
    'Overrides' => '',
    
    'List servers' => 'Серверы',
    'Add new server' => 'Добавить сервер',
    
    'Add a ban' => 'Добавить бан',
    'Group ban' => 'Забанить группу',
    'Ban protests' => 'Протесты',
    'Ban submissions' => 'Предложения',
    'Import bans' => 'Импорт банов',
    'Ban list' => 'Список банов',
    
    'MODs' => 'Моды',
    'Add new MOD' => 'Добавить',
    
    'Main Settings' => 'Главные настройки',
    'Features' => 'Особенности',
    'Themes' => 'Темы',
    'System Log' => 'Системный лог',
    'Email settings' => 'Настройки E-mail',
);
