<?php

// ###################[ Bans ]##################################################################
CheckAdminAccess(
    ADMIN_OWNER
        |
    ADMIN_ADD_BAN
        |
    ADMIN_EDIT_OWN_BANS
        |
    ADMIN_EDIT_GROUP_BANS
        |
    ADMIN_EDIT_ALL_BANS
        |
    ADMIN_BAN_PROTESTS
        |
    ADMIN_BAN_SUBMISSIONS
);

$sbViewParams['breadCrumbs'][] = t('breadcrumbs', 'Ban management');

if(!isset($_GET['o'])) {
    // ====================[ ADMIN SIDE MENU START ] ===================
    $banTabMenu = new CTabsMenu();
    if($userbank->HasAccess(ADMIN_OWNER|ADMIN_ADD_BAN ) ) {
        $banTabMenu->addMenuItem(t('admin/leftmenu', "Add a ban"),0);
        if ($GLOBALS['config']['config.enablegroupbanning'] == 1) {
            $banTabMenu->addMenuItem(t('admin/leftmenu', "Group ban"), 4);
        }
    }
    if ($userbank->HasAccess(ADMIN_OWNER | ADMIN_BAN_PROTESTS)) {
        $banTabMenu->addMenuItem(t('admin/leftmenu', "Ban protests"), 1);
    }
    if ($userbank->HasAccess(ADMIN_OWNER | ADMIN_BAN_SUBMISSIONS)) {
        $banTabMenu->addMenuItem(t('admin/leftmenu', "Ban submissions"), 2);
    }
    if ($userbank->HasAccess(ADMIN_OWNER | ADMIN_BAN_IMPORT)) {
        $banTabMenu->addMenuItem(t('admin/leftmenu', "Import bans"), 3);
    }
    $banTabMenu->addMenuItem(t('admin/leftmenu', "Ban list"), 5, "", "index.php?m=bans",true);
    $banTabMenu->outputMenu();

    // ====================[ ADMIN SIDE MENU END ] ===================

    include __DIR__ . DIRECTORY_SEPARATOR . "admin.bans.php";

    if (isset($_GET['mode']) && $_GET['mode'] == "delete") {
        echo "<script>ShowBox('".t('admin', 'Ban Deleted')."', '".t('admin', 'The ban has been deleted from SourceBans')."', 'green', '', true);</script>";
    } elseif (isset($_GET['mode']) && $_GET['mode'] == "unban") {
        echo "<script>ShowBox('".t('admin', 'Player Unbanned')."', '".t('admin', 'The Player has been unbanned from SourceBans')."', 'green', '', true);</script>";
    }

    $sbViewParams['pageTitle'] = t('admin', 'Bans');
    $sbViewParams['title'] = t('admin', 'Bans');
} elseif($_GET['o'] == 'edit') {
    $banTabMenu = new CTabsMenu();
    $banTabMenu->addMenuItem(t('admin/leftmenu', 'Back'), 0,"", "javascript:history.go(-1);", true);
    $banTabMenu->outputMenu();			

    include __DIR__ . DIRECTORY_SEPARATOR . "admin.edit.ban.php";
    $sbViewParams['pageTitle'] = t('admin', 'Edit Ban Details');
    $sbViewParams['title'] = t('admin', 'Edit Ban Details');
} elseif($_GET['o'] == 'email') {
    $banTabMenu = new CTabsMenu();
    $banTabMenu->addMenuItem(t('admin/leftmenu', 'Back'), 0, "", "javascript:history.go(-1);", true);
    $banTabMenu->outputMenu();					

    include __DIR__ . DIRECTORY_SEPARATOR . "admin.email.php";
    $sbViewParams['pageTitle'] = t('admin', 'Email');
    $sbViewParams['title'] = t('admin', 'Email');
}