<?php

return array(
    'Error' => 'Ошибка',
    'This page is disabled. You should not be here.' => 'Страница отключена',
    'Successful' => 'Успешно',
    'Your protest has been sent.' => 'Ваш протест был успешно отправлен на рассмотрение',
    
    'Before you proceed make sure you first check our banlist and search it by clicking <a href="index.php?p=banlist">here</a> if you are listed and for what reason.<br>If you do find yourself listed on the banlist and find the reason for this to be untrue you can write a protest.' => 'Перед добавлением протеста, будьте уверены, что бан, который вы хотите опротестовать, существует.<br> Перейдите по <a href="index.php?p=banlist">этой ссылке</a>, чтобы найти бан',
    
    // Title (Heading browser tabs)
    'Protest' => 'Добавить протест бана',
    'Protest a ban' => 'Опротестовать бан',
    
    'Name' => 'Ваш ник',
    'Steam ID' => 'SteamID',
    'IP Address' => 'IP адрес',
    'Your Details' => 'Ваша информация',
    'Ban Type' => 'Тип бана',
    'Your SteamID' => 'Ваш SteamID',
    'Your IP' => 'Ваш IP',
    'Reason why you should be unbanned' => 'Причина, по которой вас необходимо разбанить',
    'Be as descriptive as possible' => 'Будьте максимально информативным',
    'Your Email' => 'Ваш E-mail',
    '[[mandatory]] = Mandatory Field' => '[[mandatory]] - обязательные поля',
    'Submit' => 'Отправить',
    'What happens after you posted your protest?' => 'Что произойдет после того, как вы отправите ваш протест?',
    'The admins will get notified of your protest. They will then review if the ban is conclusive. After reviewing you will get a reply, which usally means within 24 hours.' => 'Наши администраторы сразу же получат уведомление о поступившем протесте. Затем Вашу заявку будут рассматривать, чтобы убедиться в ее достоверности. Затем администраторами принимается решение принять заявку или нет. Если заявка принимается админами, то игрок сразу же попадает в банлист. И вам будет отправлено уведомление о принятии решения о вашей заявке',
    '<b>Note:</b> Sending emails with threats to our admins, scolding or shouting will not get you unbanned and in fact we will delete your protest right away!' => '<b>Внимание:</b> Сообщения с угрозами, криками и бранью будут сразу же проигнорированы, и заявка будет удалена',
    'Protest a ban' => 'Опротестовать бан',
    'Please type a valid STEAM ID.' => 'Введите правильный SteamID',
    'That Steam ID is not banned!' => 'Этот SteamID не забанен',
    'A protest is already pending for this Steam ID.' => 'На этот SteamID уже есть заявки протеста',
    'Please type a valid IP.' => 'Введите правильный IP адрес',
    'That IP is not banned!' => 'Этот IP адрес не забанен',
    'A protest is already pending for this IP.' => 'На этот IP адрес уже есть заявки протеста',
    'You must include a player name' => 'Введите ник игрока',
    'You must include comments' => 'Введите несколько слов комментария к заявке',
    'You must include a valid email address' => 'Введите правильный E-mail адрес',
    
    // Mail
    'Hello [[username]]' => 'Привет, [[username]]',
    'A new ban protest has been posted on your SourceBans page.' => 'Новый протест бана добавлен в SourceBans',
    "Player: [[player]] ([[steamid]])\nBanned by: [[bannedby]]\nMessage: [[message]]" => "Игрок: [[player]] ([[steamid]])\nЗабанен админом: [[bannedby]]\nСообщение: [[message]]",
    'Click the link below to view the current ban protests.' => 'Перейдите по ссылке ниже для просмотра текущих протестов',
    '[SourceBans] Ban Protest Added' => '[SourceBans] Добавлен протест бана',
);
