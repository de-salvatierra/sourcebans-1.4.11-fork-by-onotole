<?php

class BansModule extends CModule {
    
    /**
     * Метод вызывается магически через Cmodule::_callStatic
     * @return array Массив с данными банов и блокировок
     */
    public static function bansForDashboard() {
        $res = $GLOBALS['db']->Execute("SELECT count(name) FROM " . DB_PREFIX . "_banlog");
        $totalstopped = (int) $res->fields[0];

        $res = $GLOBALS['db']->Execute(
            "SELECT
                `bl`.`name`,
                `time`,
                `bl`.`sid`,
                `bl`.`bid`,
                `b`.`type`,
                `b`.`authid`,
                `b`.`ip`
            FROM
                `" . DB_PREFIX . "_banlog` AS `bl`
            LEFT JOIN
                `" . DB_PREFIX . "_bans` AS `b`
            ON
                `b`.`bid` = `bl`.`bid`
            ORDER BY `time` DESC
            LIMIT 10"
        );

        $stopped = array();
        $blcount = 0;
        while (!$res->EOF) {
            $info = array();
            $info['date'] = SBDate(DATE_FORMAT, $res->fields[1]);
            $info['name'] = stripslashes($res->fields[0]);
            $info['short_name'] = trunc($info['name'], 40, false);
            $info['auth'] = $res->fields['authid'];
            $info['ip'] = $res->fields['ip'];
            $info['server'] = "block_" . $res->fields['sid'] . "_{$blcount}";
            if(isset($GLOBALS['config']['banlist.hidesteamid']) && $GLOBALS['config']['banlist.hidesteamid'] == "1" && !$userbank->is_admin()) {
                $info['search_link'] = "";
            } else {
                if ($res->fields['type'] == 1) {
                    $info['search_link'] = "index.php?m=bans&advSearch=" . $info['ip'] . "&advType=ip&Submit";
                } else {
                    $info['search_link'] = "index.php?m=bans&advSearch=" . $info['auth'] . "&advType=steamid&Submit";
                }
                $info['search_link'] = "<br /><div align=middle><a href=" . $info['search_link'] . ">".t('home', 'Click here for ban details.')."</a></div>";
            }
            $info['link_url'] = "window.location = '" . $info['search_link'] . "';";
            $info['name'] = htmlspecialchars(addslashes($info['name']), ENT_QUOTES, 'UTF-8');

            $showBoxTitle = t('home', 'Blocked player: [[playername]]', array('[[playername]]' => $info['name']));

            $showBoxMsg  = t('home', '[[playername]] tried to enter', array('[[playername]]' => $info['name']));
            $showBoxMsg2 = t('home', 'at [[date]]', array('[[date]]' => $info['date']));

            $info['popup'] = "ShowBox('" . $showBoxTitle . "', '" . $showBoxMsg . "<br />' + document.getElementById('". $info['server'] . "').title + '<br />" . $showBoxMsg2 
                    . $info['search_link'] . "', "
                . "'red', "
                . "'', "
                . "true"
                . ");";

            array_push($stopped, $info);
            $res->MoveNext();
            ++$blcount;
        }

        $res = $GLOBALS['db']->Execute("SELECT count(bid) FROM " . DB_PREFIX . "_bans");
        $BanCount = (int) $res->fields[0];

        $res = $GLOBALS['db']->Execute(
            "SELECT
                bid,
                ba.ip,
                ba.authid,
                ba.name,
                created,
                ends,
                length,
                reason,
                ba.aid,
                ba.sid,
                ad.user,
                CONCAT(se.ip,':',se.port),
                se.sid,
                mo.icon,
                ba.RemoveType,
                ba.type
            FROM
                " . DB_PREFIX . "_bans AS ba 
            LEFT JOIN
                " . DB_PREFIX . "_admins AS ad
            ON
                ba.aid = ad.aid
            LEFT JOIN
                " . DB_PREFIX . "_servers AS se
            ON
                se.sid = ba.sid
            LEFT JOIN
                " . DB_PREFIX . "_mods AS mo
            ON
                mo.mid = se.modid
            ORDER BY created DESC
            LIMIT 10"
        );
        $bans = array();
        while (!$res->EOF) {
            $info = array();
            $info['name'] = stripslashes($res->fields[3]);
            $info['created'] = SBDate(DATE_FORMAT, $res->fields['created']);
            if($res->fields[6] == 0) {
                $info['length'] = t('home', 'Permanent');
            } else {
                $ltemp = explode(",", SecondsToString(intval($res->fields[6])));
                $info['length'] = $ltemp[0];
            }

            $info['icon'] = empty($res->fields[13]) ? 'web.png' : $res->fields[13];
            $info['authid'] = $res->fields[2];
            $info['ip'] = $res->fields[1];
            if ($res->fields[15] == 1) {
                $info['search_link'] = "index.php?m=bans&advSearch=" . $info['ip'] . "&advType=ip&Submit";
            } else {
                $info['search_link'] = "index.php?m=bans&advSearch=" . $info['authid'] . "&advType=steamid&Submit";
            }
            $info['link_url'] = "window.location = '" . $info['search_link'] . "';";
            $info['short_name'] = trunc($info['name'], 25, false);

            if ($res->fields[14] == 'D' || $res->fields[14] == 'U' || $res->fields[14] == 'E' || ($res->fields[6] && $res->fields[5] < time())) {
                $info['unbanned'] = true;

                if ($res->fields[14] == 'D') {
                    $info['ub_reason'] = t('home', 'D');
                } elseif ($res->fields[14] == 'U') {
                    $info['ub_reason'] = t('home', 'U');
                } else {
                    $info['ub_reason'] = t('home', 'E');
                }
            } else {
                $info['unbanned'] = false;
            }

            array_push($bans, $info);
            $res->MoveNext();
        }
        
        return array(
            'players_blocked' => $stopped,
            'total_blocked' => $totalstopped,
            'players_banned' => $bans,
            'total_bans' => $BanCount
        );
    }
    
    public function xajax_PasteBan($sid, $name, $type = 0) {
        $objResponse = new xajaxResponse();
        global $userbank;

        $sid = (int) $sid;
        $type = (int) $type;
        if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_ADD_BAN)) {
            $objResponse->redirect("index.php?p=login&n=no_access", 0);

            new CSystemLog(
                'w',
                t('log', 'Hacking Attempt'),
                t('log', '[[username]] tried paste a ban, but doesn\'t have access.', array(
                    '[[username]]' => $userbank->GetProperty("user"),
                ))
            );

            return $objResponse;
        }

        //get the server data
        $data = $GLOBALS['db']->GetRow(
            "SELECT
                `srv`.`ip` `ip`,
                `srv`.`port` `port`,
                `srv`.`rcon` `rcon`,
                `mod`.`modfolder` `game`
            FROM
                `".DB_PREFIX."_servers` `srv`
            LEFT JOIN
                `".DB_PREFIX."_mods` `mod`
            ON
                `mod`.`mid` = `srv`.`modid`
            WHERE `srv`.`sid` = '".intval($sid)."';");
        if (empty($data['rcon'])) {
            $msg = t(
                'servers',
                'Can not ban [[player]]. No RCON password!',
                array(
                    '[[player]]' => RemoveCode($name)
                )
            );
            $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Error')."', '{$msg}', 'red', '', true);");
            return $objResponse;
        }

        require_once INCLUDES_PATH . 'SourceQuery'.DIRECTORY_SEPARATOR.'SourceQuery.class.php';
        $query = new SourceQuery;
        try {
            $query->Connect($data['ip'], $data['port'], 1);
            $query->SetRconPassword($data['rcon']);
        } catch(Exception $e) {
            new CSystemLog(
                'e',
                t('log', 'Can not connect to server'),
                t('log', 'Can not connect to server. Error: [[errormessage]]', array(
                    '[[errormessage]]' => $e->getMessage()
                ))
            );
             $objResponse->addScript("ShowBox('"
                 .t('servers', 'Error')
                 ."', '"
                 .t('servers', 'Can not connect to server. See system log for details')
                 ."', 'red', '', true);"
            );
            return $objResponse;
        }

        $status = $query->Rcon('status');

        $player = false;

        $players = getPlayersFromStatus($status, $data['game']);

        foreach($players as $p) {
            if($p['name'] == $name) {
                $player = $p;
                break;
            }
        }

        if ($player) {
            $steam = $player['steamid'];
            $name = $player['name'];
            $ip = $player['ip'];
            $objResponse->addScript("$('nickname').value = '" . addslashes($name) . "'");
            if ($type == 1) {
                $objResponse->addScript("$('type').options[1].selected = true");
            }
            $objResponse->addScript("$('steam').value = '" . $steam . "'");
            $objResponse->addScript("$('ip').value = '" . $ip . "'");
        } else {
            $msg = t(
                'servers',
                'Can not get player info for [[player]], Player is not on the server ([[server]]) anymore!',
                array(
                    '[[player]]' => addslashes(htmlspecialchars($name)),
                    '[[server]]' => $data['ip'] . ":" . $data['port']
                )
            );
            $objResponse->addScript("ShowBox('".t('servers', 'Error')."', '{$msg}', 'red', '', true);");
            $objResponse->addScript("$('dialog-control').setStyle('display', 'block');");
            return $objResponse;
        }
        $objResponse->addScript("SwapPane(0);");
        $objResponse->addScript("$('dialog-control').setStyle('display', 'block');");
        $objResponse->addScript("$('dialog-placement').setStyle('display', 'none');");
        return $objResponse;
    }
    
    public function xajax_PrepareReban($bid) {
        $objResponse = new xajaxResponse();
        $bid = (int) $bid;

        $ban = $GLOBALS['db']->GetRow("SELECT type, ip, authid, name, length, reason FROM " . DB_PREFIX . "_bans WHERE bid = '" . $bid . "';");
        $demo = $GLOBALS['db']->GetRow("SELECT * FROM " . DB_PREFIX . "_demos WHERE demid = '" . $bid . "' AND demtype = \"B\";");
        // clear any old stuff
        $objResponse->addScript("$('nickname').value = ''");
        $objResponse->addScript("$('ip').value = ''");
        $objResponse->addScript("$('fromsub').value = ''");
        $objResponse->addScript("$('steam').value = ''");
        $objResponse->addScript("$('txtReason').value = ''");
        $objResponse->addAssign("demo.msg", "innerHTML", "");
        $objResponse->addAssign("txtReason", "innerHTML", "");

        // add new stuff
        $objResponse->addScript("$('nickname').value = '" . $ban['name'] . "'");
        $objResponse->addScript("$('steam').value = '" . $ban['authid'] . "'");
        $objResponse->addScript("$('ip').value = '" . $ban['ip'] . "'");
        $objResponse->addScriptCall("selectLengthTypeReason", $ban['length'], $ban['type'], addslashes($ban['reason']));

        if ($demo) {
            $objResponse->addAssign("demo.msg", "innerHTML", $demo['origname']);
            $objResponse->addScript("demo('" . $demo['filename'] . "', '" . $demo['origname'] . "');");
        }
        $objResponse->addScript("SwapPane(0);");
        return $objResponse;
    }


    public function xajax_BanFriends($friendid, $name) {
        set_time_limit(0);
        $objResponse = new xajaxResponse();
        if ($GLOBALS['config']['config.enablefriendsbanning'] == 0 || !is_numeric($friendid)) {
            return $objResponse;
        }
        global $userbank;
        if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_ADD_BAN)) {
            $objResponse->redirect("index.php?p=login&n=no_access", 0);

            new CSystemLog(
                'w',
                t('log', 'Hacking Attempt'),
                t('log', '[[username]] tried to ban friends of "[[friendid]]", but doesnt have access.', array(
                    '[[username]]' => $userbank->GetProperty("user"),
                    '[[friendid]]' => RemoveCode($friendid)
                ))
            );

            return $objResponse;
        }

        $bans = $GLOBALS['db']->GetAll("SELECT CAST(MID(authid, 9, 1) AS UNSIGNED) + CAST('76561197960265728' AS UNSIGNED) + CAST(MID(authid, 11, 10) * 2 AS UNSIGNED) AS community_id FROM " . DB_PREFIX . "_bans WHERE RemoveType IS NULL;");
        foreach ($bans as $ban) {
            $already[] = $ban["community_id"];
        }
        $doc = new DOMDocument();
        $result = get_headers("http://steamcommunity.com/profiles/" . $friendid . "/", 1);
        $raw = @file_get_contents(($result["Location"] != "" ? $result["Location"] : "http://steamcommunity.com/profiles/" . $friendid . "/") . "friends"); // get the friends page
        @$doc->loadHTML($raw);
        $divs = $doc->getElementsByTagName('div');
        foreach ($divs as $div) {
            if ($div->getAttribute('id') == "memberList") {
                $memberdiv = $div;
                break;
            }
        }

        $total = 0;
        $bannedbefore = 0;
        $error = 0;
        $links = $doc->getElementsByTagName('a');
        foreach ($links as $link) {
            if (strstr($link->getAttribute('href'), "http://steamcommunity.com/id/") || strstr($link->getAttribute('href'), "http://steamcommunity.com/profiles/")) {
                $total++;
                $url = parse_url($link->getAttribute('href'), PHP_URL_PATH);
                $url = explode("/", $url);
                if (in_array($url[2], $already)) {
                    $bannedbefore++;
                    continue;
                }
                if (strstr($link->getAttribute('href'), "http://steamcommunity.com/id/")) {
                    // we don't have the friendid as this player is using a custom id :S need to get the friendid
                    if ($tfriend = GetFriendIDFromCommunityID($url[2])) {
                        if (in_array($tfriend, $already)) {
                            $bannedbefore++;
                            continue;
                        }
                        $cust = $url[2];
                        $steamid = FriendIDToSteamID($tfriend);
                        $urltag = $tfriend;
                    } else {
                        $error++;
                        continue;
                    }
                } else {
                    // just a normal friendid profile =)
                    $cust = NULL;
                    $steamid = FriendIDToSteamID($url[2]);
                    $urltag = $url[2];
                }

                // get the name
                $friendName = $link->parentNode->childNodes->item(5)->childNodes->item(0)->nodeValue;
                $friendName = str_replace("&#13;", "", $friendName);
                $friendName = trim($friendName);

                $pre = $GLOBALS['db']->Prepare("INSERT INTO " . DB_PREFIX . "_bans(created,type,ip,authid,name,ends,length,reason,aid,adminIp ) VALUES
                                        (UNIX_TIMESTAMP(),?,?,?,?,UNIX_TIMESTAMP(),?,?,?,?)");
                $GLOBALS['db']->Execute($pre, array(0,
                    "",
                    $steamid,
                    utf8_decode($friendName),
                    0,
                    t('modules/bans/main', 'Steam Community Friend Ban ([[playername]])', array('[[playername]]' => RemoveCode($name))),
                    $userbank->GetAid(),
                    $_SERVER['REMOTE_ADDR']));
            }
        }
        if ($total == 0) {
            $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Error retrieving friends')."', '".t('modules/bans/admin', 'There was an error retrieving the friend list. Check if the profile isn\'t private or if he hasn\'t got any friends! retrieving friends')."', 'red', 'index.php?p=banlist', true);");
            $objResponse->addScript("$('dialog-control').setStyle('display', 'block');");
            return $objResponse;
        }

        $msg = t(
            'modules/bans/admin',
            'Banned [[banned]]/[[total]] friends of "[[name]]".<br>[[bannedbefore]] were banned already.',
            array(
                '[[banned]]' => $total - $bannedbefore - $error,
                '[[total]]' => $total,
                '[[name]]' => htmlspecialchars($name),
                '[[bannedbefore]]' => $bannedbefore
            )
        );

        $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Friends banned successfully')."', '{$msg}', 'green', 'index.php?p=banlist', true);");
        $objResponse->addScript("$('dialog-control').setStyle('display', 'block');");
        new CSystemLog(
            "m",
            "Friends Banned",
            "Banned " 
                . ($total - $bannedbefore - $error) 
                . "/" 
                . $total 
                . " friends of \'" 
                . htmlspecialchars($name) 
                . "\'.<br>" 
                . $bannedbefore 
                . " were banned already.<br>" 
                . $error 
                . " failed."
        );

        new CSystemLog(
            'm',
            t('log', 'Friends Banned'),
            t('log', 'Banned [[banned]]/[[total]] friends of "[[name]]".<br>[[bannedbefore]] were banned already.', array(
                '[[banned]]' => $total - $bannedbefore - $error,
                '[[total]]' => $total,
                '[[name]]' => htmlspecialchars($name),
                '[[bannedbefore]]' => $bannedbefore
            ))
        );

        return $objResponse;
    }


    public function xajax_AddBan($nickname, $type, $steam, $ip, $length, $dfile, $dname, $reason, $fromsub) {
        $objResponse = new xajaxResponse();
        global $userbank;
        if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_ADD_BAN)) {
            $objResponse->redirect("index.php?p=login&n=no_access", 0);

            new CSystemLog(
                'w',
                t('log', 'Hacking Attempt'),
                t('log', '[[username]] tried to add a ban, but doesnt have access.', array(
                    '[[username]]' => $userbank->GetProperty("user"),
                ))
            );

            return $objResponse;
        }

        $steam = trim($steam);

        $error = 0;
        // If they didnt type a steamid
        if (empty($steam) && $type == 0) {
            $error++;
            $objResponse->addAssign("steam.msg", "innerHTML", t('modules/bans/admin', 'You must type a Steam ID or Community ID'));
            $objResponse->addScript("$('steam.msg').setStyle('display', 'block');");
        } else if (($type == 0 && !is_numeric($steam) && !validate_steam($steam)) || (is_numeric($steam) && (strlen($steam) < 15 || !validate_steam($steam = FriendIDToSteamID($steam))))) {
            $error++;
            $objResponse->addAssign("steam.msg", "innerHTML", t('modules/bans/admin', 'Please enter a valid Steam ID or Community ID'));
            $objResponse->addScript("$('steam.msg').setStyle('display', 'block');");
        } else if (empty($ip) && $type == 1) {
            $error++;
            $objResponse->addAssign("ip.msg", "innerHTML", t('modules/bans/admin', 'You must type an IP'));
            $objResponse->addScript("$('ip.msg').setStyle('display', 'block');");
        } else if ($type == 1 && !validate_ip($ip)) {
            $error++;
            $objResponse->addAssign("ip.msg", "innerHTML", t('modules/bans/admin', 'You must type a valid IP'));
            $objResponse->addScript("$('ip.msg').setStyle('display', 'block');");
        } else {
            $objResponse->addAssign("steam.msg", "innerHTML", "");
            $objResponse->addScript("$('steam.msg').setStyle('display', 'none');");
            $objResponse->addAssign("ip.msg", "innerHTML", "");
            $objResponse->addScript("$('ip.msg').setStyle('display', 'none');");
        }

        if ($error > 0) {
            return $objResponse;
        }

        $nickname = RemoveCode($nickname);
        $ip = preg_replace('#[^\d\.]#', '', $ip); //strip ip of all but numbers and dots
        $dname = RemoveCode($dname);
        $reason = RemoveCode($reason);
        if (!$length) {
            $len = 0;
        } else {
            $len = $length * 60;
        }

        // prune any old bans
        PruneBans();
        if ((int) $type == 0) {
            // Check if the new steamid is already banned
            $chk = $GLOBALS['db']->GetRow("SELECT count(bid) AS count FROM " . DB_PREFIX . "_bans WHERE authid = ? AND (length = 0 OR ends > UNIX_TIMESTAMP()) AND RemovedBy IS NULL AND type = '0'", array($steam));

            if (intval($chk[0]) > 0) {
                $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Error')."', '".t('modules/bans/admin', 'SteamID: [[steam]] is already banned.', array('[[steam]]' => $steam))."', 'red', '');");
                return $objResponse;
            }

            // Check if player is immune
            $admchk = $userbank->GetAllAdmins();
            foreach ($admchk as $admin) {
                if ($admin['authid'] == $steam && $userbank->GetProperty('srv_immunity') < $admin['srv_immunity']) {
                    $msg = t(
                        'modules/bans/admin',
                        'SteamID: Admin [[admin]] ([[steam]]) is immune.',
                        array(
                            '[[admin]]' => $admin['user'],
                            '[[steam]]' => $steam
                        )
                    );
                    $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Error')."', '{$msg}', 'red', '');");
                    return $objResponse;
                }
            }
        }
        if ((int) $type == 1) {
            $chk = $GLOBALS['db']->GetRow("SELECT count(bid) AS count FROM " . DB_PREFIX . "_bans WHERE ip = ? AND (length = 0 OR ends > UNIX_TIMESTAMP()) AND RemovedBy IS NULL AND type = '1'", array($ip));

            if (intval($chk[0]) > 0) {
                $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Error')."', '".t('modules/bans/admin', 'IP: [[ip]] is already banned', array('[[ip]]' => $ip)).".', 'red', '');");
                return $objResponse;
            }
        }

        $pre = $GLOBALS['db']->Prepare("INSERT INTO " . DB_PREFIX . "_bans(created,type,ip,authid,name,ends,length,reason,aid,adminIp ) VALUES
                                        (UNIX_TIMESTAMP(),?,?,?,?,(UNIX_TIMESTAMP() + ?),?,?,?,?)");
        $GLOBALS['db']->Execute($pre, array($type,
            $ip,
            $steam,
            $nickname,
            $length * 60,
            $len,
            $reason,
            $userbank->GetAid(),
            $_SERVER['REMOTE_ADDR']));
        $subid = $GLOBALS['db']->Insert_ID();

        if ($dname && $dfile && ctype_alnum($dfile)) {
            $GLOBALS['db']->Execute("INSERT INTO " . DB_PREFIX . "_demos(demid,demtype,filename,origname)
                                 VALUES(?,'B', ?, ?)", array((int) $subid, $dfile, $dname));
        }
        if ($fromsub) {
            $submail = $GLOBALS['db']->Execute("SELECT name, email FROM " . DB_PREFIX . "_submissions WHERE subid = '" . (int) $fromsub . "'");
            // Send an email when ban is accepted
            $requri = substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], ".php") + 4);

            $message = t('modules/bans/admin', "Hello,\n");
            $message .= t('modules/bans/admin', "Your ban submission was accepted by our admins.\n");
            $message .= t('modules/bans/admin', "Thank you for your support!\n");
            $message .= t('modules/bans/admin', "Click the link below to view the current ban list.\n\n");
            $message .= "http://" . $_SERVER['HTTP_HOST'] . $requri . "?p=banlist";

            $subject = t('modules/bans/admin', '[SourceBans] Ban Added');

            $userbank->sendMail($submail->fields['email'], $subject, $message);
            $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_submissions` SET archiv = '2', archivedby = '" . $userbank->GetAid() . "' WHERE subid = '" . (int) $fromsub . "'");
        }

        $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_submissions` SET archiv = '3', archivedby = '" . $userbank->GetAid() . "' WHERE SteamId = ?;", array($steam));

        $kickit = isset($GLOBALS['config']['config.enablekickit']) && $GLOBALS['config']['config.enablekickit'] == "1";
        if ($kickit) {

            $bantype = intval($type) == 0 ? $steam : $ip;

            $objResponse->addScript("ShowKickBox('" . ((int) $type == 0 ? $steam : $ip) . "', '" . (int) $type . "');");
            $objResponse->addScript("ShowBox(
                    '".t('modules/bans/admin', 'Ban Added')."',
                    '".t('modules/bans/admin', 'The ban has been successfully added')."'
                        +'<br><iframe id=\"srvkicker\" frameborder=\"0\" width=\"100%\" src=\"pages/admin.kickit.php?check=".$bantype."&type=".intval($type)."\"></iframe>',
                    'green',
                    'index.php?m=bans&p=admin',
                    true
                );
            ");
        } else {
            $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Ban Added')."', '".t('modules/bans/admin', 'The ban has been successfully added')."', 'green', 'index.php?m=bans&p=admin');");
        }

        $objResponse->addScript("TabToReload();");

        new CSystemLog(
            'm',
            t('log', 'Ban Added'),
            t('log', 'Ban against ([[banid]]) has been added, reason: [[reason]], length: [[length]]', array(
                '[[banid]]' => (int) $type == 0 ? $steam : $ip,
                '[[reason]]' => $reason,
                '[[length]]' => $length,
            )),
            true,
            $kickit
        );

        return $objResponse;
    }
    
    public function xajax_GroupBan($groupuri, $isgrpurl = "no", $queue = "no", $reason = "", $last = "") {
        $objResponse = new xajaxResponse();
        if ($GLOBALS['config']['config.enablegroupbanning'] == 0) {
            return $objResponse;
        }
        global $userbank;
        if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_ADD_BAN)) {
            $objResponse->redirect("index.php?p=login&n=no_access", 0);

            new CSystemLog(
                'w',
                t('log', 'Hacking Attempt'),
                t('log', '[[username]] tried to initiate a groupban "[[group]]".', array(
                    '[[username]]' => $userbank->GetProperty("user"),
                    '[[group]]' => htmlspecialchars(addslashes(trim($groupuri)))
                ))
            );

            return $objResponse;
        }
        if ($isgrpurl == "yes") {
            $grpname = $groupuri;
        } else {
            $url = parse_url($groupuri, PHP_URL_PATH);
            $url = explode("/", $url);
            $grpname = $url[2];
        }
        if (empty($grpname)) {
            $objResponse->addAssign("groupurl.msg", "innerHTML", t('modules/bans/admin', 'Error parsing the group url'));
            $objResponse->addScript("$('groupurl.msg').setStyle('display', 'block');");
            return $objResponse;
        } else {
            $objResponse->addScript("$('groupurl.msg').setStyle('display', 'none');");
        }

        if ($queue == "yes") {
            $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Please Wait...')."', '".t('modules/bans/admin', 'Banning all members of the selected groups... <br>Please Wait...<br>Notice: This can last 15mins or longer, depending on the amount of members of the groups!')."', 'info', '', true);");
        } else {
            $msg = t(
                'modules/bans/admin',
                'Banning all members of [[groupname]]...<br>Please Wait...<br>Notice: This can last 15mins or longer, depending on the amount of members of the group!',
                array(
                    '[[groupname]]' => $grpname
                )
            );
            $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Please Wait...')."', '{$msg}', 'info', '', true);");
        }
        $objResponse->addScript("$('dialog-control').setStyle('display', 'none');");
        $objResponse->addScriptCall("xajax_BanMemberOfGroup", $grpname, $queue, htmlspecialchars(addslashes($reason)), $last);
        return $objResponse;
    }
    
    public function xajax_BanMemberOfGroup($grpurl, $queue, $reason, $last) {
        set_time_limit(0);
        $objResponse = new xajaxResponse();
        if ($GLOBALS['config']['config.enablegroupbanning'] == 0) {
            return $objResponse;
        }
        global $userbank;
        if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_ADD_BAN)) {
            $objResponse->redirect("index.php?p=login&n=no_access", 0);

            new CSystemLog(
                'w',
                t('log', 'Hacking Attempt'),
                t('log', '[[username]] tried to ban group "[[group]]", but doesnt have access.', array(
                    '[[username]]' => $userbank->GetProperty("user"),
                    '[[group]]' => htmlspecialchars(addslashes(trim($grpurl)))
                ))
            );

            return $objResponse;
        }
        $bans = $GLOBALS['db']->GetAll("SELECT CAST(MID(authid, 9, 1) AS UNSIGNED) + CAST('76561197960265728' AS UNSIGNED) + CAST(MID(authid, 11, 10) * 2 AS UNSIGNED) AS community_id FROM " . DB_PREFIX . "_bans WHERE RemoveType IS NULL;");
        foreach ($bans as $ban) {
            $already[] = $ban["community_id"];
        }
        $doc = new DOMDocument();
        // This could be changed to use the memberlistxml
        // https://partner.steamgames.com/documentation/community_data
        // http://steamcommunity.com/groups/<GroupName>/memberslistxml/?xml=1
        // but we'd need to open every single profile of every member to get the name..
        $raw = file_get_contents("http://steamcommunity.com/groups/" . $grpurl . "/members"); // get the members page
        @$doc->loadHTML($raw); // load it into a handy object so we can maintain it
        // the memberlist is paginated, so we need to check the number of pages
        $pagetag = $doc->getElementsByTagName('div');
        foreach ($pagetag as $pageclass) {
            if ($pageclass->getAttribute('class') == "pageLinks") { //search for the pageLinks div
                $pageclasselmt = $pageclass;
                break;
            }
        }
        
        if(empty($pageclasselmt)) {
            $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Groups banned successfully')."', '".t('modules/bans/admin', 'The selected Groups were banned successfully. For detailed info check below.')."', 'green', '', true);");
            $objResponse->addScript("$('dialog-control').setStyle('display', 'block');");
            return $objResponse;
        }
        
        $pagelinks = $pageclasselmt->getElementsByTagName('a'); // get all page links
        $pagenumbers = array();
        $pagenumbers[] = 1; // add at least one page for the loop. if the group doesn't have 50 members -> no paginating
        foreach ($pagelinks as $pagelink) {
            $pagenumber = str_replace("?p=", "", $pagelink->childNodes->item(0)->nodeValue); // remove the get variable stuff so we only have the pagenumber
            if (strpos($pagenumber, ">") === false) { // don't want the "next" button ;)
                $pagenumbers[] = $pagenumber;
            }
        }
        //$members = array();
        $total = 0;
        $bannedbefore = 0;
        $error = 0;
        for ($i = 1; $i <= max($pagenumbers); $i++) { // loop through all the pages
            if ($i != 1) { // if we are on page 1 we don't need to reget the content as we did above already.
                $raw = file_get_contents("http://steamcommunity.com/groups/" . $grpurl . "/members?p=" . $i); // open the memberpage
                @$doc->loadHTML($raw);
            }
            $tags = $doc->getElementsByTagName('a');
            foreach ($tags as $tag) {
                // search for the member profile links
                if ((strstr($tag->getAttribute('href'), "http://steamcommunity.com/id/") || strstr($tag->getAttribute('href'), "http://steamcommunity.com/profiles/")) && $tag->hasChildNodes() && $tag->childNodes->length == 1 && $tag->childNodes->item(0)->nodeValue != "") {
                    $total++;
                    $url = parse_url($tag->getAttribute('href'), PHP_URL_PATH);
                    $url = explode("/", $url);
                    if (in_array($url[2], $already)) {
                        $bannedbefore++;
                        continue;
                    }
                    if (strstr($tag->getAttribute('href'), "http://steamcommunity.com/id/")) {
                        // we don't have the friendid as this player is using a custom id :S need to get the friendid
                        if ($tfriend = GetFriendIDFromCommunityID($url[2])) {
                            if (in_array($tfriend, $already)) {
                                $bannedbefore++;
                                continue;
                            }
                            $cust = $url[2];
                            $steamid = FriendIDToSteamID($tfriend);
                            $urltag = $tfriend;
                        } else {
                            $error++;
                            continue;
                        }
                    } else {
                        // just a normal friendid profile =)
                        $cust = NULL;
                        $steamid = FriendIDToSteamID($url[2]);
                        $urltag = $url[2];
                    }
                    $pre = $GLOBALS['db']->Prepare("INSERT INTO " . DB_PREFIX . "_bans(created,type,ip,authid,name,ends,length,reason,aid,adminIp ) VALUES
                                        (UNIX_TIMESTAMP(),?,?,?,?,UNIX_TIMESTAMP(),?,?,?,?)");
                    $banReason = t(
                        'modules/bans/admin',
                        'Steam Community Group Ban ([[groupUrl]]) [[banReason]]',
                        array(
                            '[[groupUrl]]' => $grpurl,
                            '[[banReason]]' => $reason
                        )
                    );
                    $GLOBALS['db']->Execute($pre, array(0,
                        "",
                        $steamid,
                        utf8_decode($tag->childNodes->item(0)->nodeValue),
                        0,
                        $banReason,
                        $userbank->GetAid(),
                        $_SERVER['REMOTE_ADDR']));
                }
            }
        }
        if ($queue == "yes") {
            $msg = t(
                'modules/bans/admin',
                'Banned [[totalbanned]]/[[total]] players of group "[[group]]". [[bannedbefore]] were banned already. [[errors]] failed.',
                array(
                    '[[totalbanned]]' => $total - $bannedbefore - $error,
                    '[[total]]' => $total,
                    '[[group]]' => $grpurl,
                    '[[bannedbefore]]' => $bannedbefore,
                    '[[errors]]' => $error
                )
            );
            $objResponse->addAppend("steamGroupStatus", "innerHTML", "<p>{$msg}</p>");
            if ($grpurl == $last) {
                $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Groups banned successfully')."', '".t('modules/bans/admin', 'The selected Groups were banned successfully. For detailed info check below.')."', 'green', '', true);");
                $objResponse->addScript("$('dialog-control').setStyle('display', 'block');");
            }
        } else {
            $msg = t(
                'modules/bans/admin',
                'Banned [[totalbanned]]/[[total]] players of group "[[group]]". [[bannedbefore]] were banned already. [[errors]] failed.',
                array(
                    '[[totalbanned]]' => $total - $bannedbefore - $error,
                    '[[total]]' => $total,
                    '[[group]]' => $grpurl,
                    '[[bannedbefore]]' => $bannedbefore,
                    '[[errors]]' => $error
                )
            );
            $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Groups banned successfully')."', '{$msg}', 'green', '', true);");
            $objResponse->addScript("$('dialog-control').setStyle('display', 'block');");
        }

        new CSystemLog(
            'm',
            t('log', 'Group Banned'),
            t('log', 'Banned [[totalbanned]]/[[total]] players of group "[[group]]". [[bannedbefore]] were banned already. [[errors]] failed.', array(
                '[[totalbanned]]' => $total - $bannedbefore - $error,
                '[[total]]' => $total,
                '[[group]]' => $grpurl,
                '[[bannedbefore]]' => $bannedbefore,
                '[[errors]]' => $error
            ))
        );

        return $objResponse;
    }
    
    public function xajax_AddComment($bid, $ctype, $ctext, $page) {
        $objResponse = new xajaxResponse();
        global $userbank;
        if (!$userbank->is_admin()) {
            $objResponse->redirect("index.php?p=login&n=no_access", 0);

            new CSystemLog(
                'w',
                t('log', 'Hacking Attempt'),
                t('log', '[[username]] tried to add a comment, but doesnt have access.', array(
                    '[[username]]' => $userbank->GetProperty("user")
                ))
            );

            return $objResponse;
        }

        $bid = (int) $bid;
        $page = (int) $page;

        $pagelink = "";
        if ($page != -1) {
            $pagelink = "&page=" . $page;
        }

        if ($ctype == "B") {
            $redir = "?m=bans" . $pagelink;
        } elseif ($ctype == "S") {
            $redir = "?m=bans&p=admin#^2";
        } elseif ($ctype == "P") {
            $redir = "?m=bans&p=admin#^1";
        } else {
            $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Error')."', '".t('modules/bans/admin', 'Bad comment type')."', 'red');");
            return $objResponse;
        }

        $ctext = trim($ctext);

        $pre = $GLOBALS['db']->Prepare("INSERT INTO " . DB_PREFIX . "_comments(bid,type,aid,commenttxt,added) VALUES (?,?,?,?,UNIX_TIMESTAMP())");
        $GLOBALS['db']->Execute($pre, array($bid,
            $ctype,
            $userbank->GetAid(),
            $ctext));

        $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Comment Added')."', '".t('modules/bans/admin', 'The comment has been successfully published')."', 'green', 'index.php$redir');");
        $objResponse->addScript("TabToReload();");
        new CSystemLog(
            'm',
            t('log', 'Comment Added'),
            t('log', '[[username]] added a comment for ban #[[banid]]', array(
                '[[username]]' => $userbank->GetProperty("user"),
                '[[banid]]' => $bid
            ))
        );

        return $objResponse;
    }
    
    public function xajax_RemoveComment($cid, $ctype, $page) {
        $objResponse = new xajaxResponse();
        global $userbank;
        if (!$userbank->HasAccess(ADMIN_OWNER)) {
            $objResponse->redirect("index.php?p=login&n=no_access", 0);

            new CSystemLog(
                'w',
                t('log', 'Hacking Attempt'),
                t('log', '[[username]] tried to remove a comment, but doesnt have access.', array(
                    '[[username]]' => $userbank->GetProperty("user")
                ))
            );

            return $objResponse;
        }

        $cid = (int) $cid;
        $page = (int) $page;

        $pagelink = "";
        if ($page != -1) {
            $pagelink = "&page=" . $page;
        }

        $res = $GLOBALS['db']->Execute("DELETE FROM `" . DB_PREFIX . "_comments` WHERE `cid` = ?", array($cid));
        if ($ctype == "B") {
            $redir = "?m=bans" . $pagelink;
        } else {
            $redir = "?m=bans&p=admin";
        }
        if ($res) {
            $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Comment Deleted')."', '".t('modules/bans/admin', 'The selected comment has been deleted from the database')."', 'green', 'index.php{$redir}', true);");

            new CSystemLog(
                'm',
                t('log', 'Comment Deleted'),
                t('log', '[[username]] deleted comment #[[commentid]]', array(
                    '[[username]]' => $userbank->GetProperty("user"),
                    '[[commentid]]' => $cid
                ))
            );

        } else {
            $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Error')."', '".t('modules/bans/admin', 'There was a problem deleting the comment from the database. Check the logs for more info')."', 'red', 'index.php{$redir}', true);");
        }
        return $objResponse;
    }
    
    function xajax_EditComment($cid, $ctype, $ctext, $page) {
        $objResponse = new xajaxResponse();
        global $userbank;
        if (!$userbank->is_admin()) {
            $objResponse->redirect("index.php?p=login&n=no_access", 0);

            new CSystemLog(
                'w',
                t('log', 'Hacking Attempt'),
                t('log', '[[username]] tried to edit a comment, but doesnt have access.', array(
                    '[[username]]' => $userbank->GetProperty("user")
                ))
            );

            return $objResponse;
        }

        $cid = (int) $cid;
        $page = (int) $page;

        $pagelink = "";
        if ($page != -1) {
            $pagelink = "&page=" . $page;
        }

        if ($ctype == "B") {
            $redir = "?m=bans" . $pagelink;
        } elseif ($ctype == "S") {
            $redir = "?m=bans&p=admin#^2";
        } elseif ($ctype == "P") {
            $redir = "?m=bans&p=admin#^1";
        } else {
            $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Error')."', '".t('modules/bans/admin', 'Bad comment type.')."', 'red');");
            return $objResponse;
        }

        $ctext = trim($ctext);

        $pre = $GLOBALS['db']->Prepare("UPDATE " . DB_PREFIX . "_comments SET `commenttxt` = ?, `editaid` = ?, `edittime`= UNIX_TIMESTAMP() WHERE cid = ?");
        $GLOBALS['db']->Execute($pre, array($ctext,
            $userbank->GetAid(),
            $cid));
        $msg = t(
            'modules/bans/admin',
            'The comment #[[commentid]] has been successfully edited',
            array(
                '[[commentid]]' => $cid
            )
        );
        $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Comment Edited')."', '{$msg}', 'green', 'index.php$redir');");
        $objResponse->addScript("TabToReload();");

        new CSystemLog(
            'm',
            t('log', 'Comment Edited'),
            t('log', '[[username]] edited comment #[[commentid]]', array(
                '[[username]]' => $userbank->GetProperty("user"),
                '[[commentid]]' => $cid
            ))
        );

        return $objResponse;
    }
    
    public function xajax_RemoveSubmission($sid, $archiv) {
        $objResponse = new xajaxResponse();
        global $userbank;
        if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_BAN_SUBMISSIONS)) {
            $objResponse->redirect("index.php?p=login&n=no_access", 0);

            new CSystemLog(
                'w',
                t('log', 'Hacking Attempt'),
                t('log', '[[username]] tried to remove a submission, but doesnt have access.', array(
                    '[[username]]' => $userbank->GetProperty("user"),
                ))
            );

            return $objResponse;
        }
        $sid = (int) $sid;
        if ($archiv == "1") { // move submission to archiv
            $query1 = $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_submissions` SET archiv = '1', archivedby = '" . $userbank->GetAid() . "' WHERE subid = $sid");
            $query = $GLOBALS['db']->GetRow("SELECT count(subid) AS cnt FROM `" . DB_PREFIX . "_submissions` WHERE archiv = '0'");
            $objResponse->addScript("$('subcount').setHTML('" . $query['cnt'] . "');");

            $objResponse->addScript("SlideUp('sid_{$sid}');");
            $objResponse->addScript("SlideUp('sid_" . $sid . "a');");

            if ($query1) {
                $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Submission Archived')."', '".t('modules/bans/admin', 'The selected submission has been moved to the archive!')."', 'green', 'index.php?m=bans&p=admin', true);");
                new CSystemLog("m", "Submission Archived", "Submission (" . $sid . ") has been moved to the archive");

                new CSystemLog(
                    'm',
                    t('log', 'Submission Archived'),
                    t('log', 'Submission ([[submission]]) has been moved to the archive', array(
                        '[[submission]]' => $sid,
                    ))
                );

            } else {
                $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Error')."', '".t('modules/bans/admin', 'There was a problem moving the submission to the archive. Check the logs for more info')."', 'red', 'index.php?m=bans&p=admin', true);");
            }
        } else if ($archiv == "0") { // delete submission
            $query1 = $GLOBALS['db']->Execute("DELETE FROM `" . DB_PREFIX . "_submissions` WHERE subid = $sid");
            $GLOBALS['db']->Execute("DELETE FROM `" . DB_PREFIX . "_demos` WHERE demid = '" . $sid . "' AND demtype = 'S'");
            $query = $GLOBALS['db']->GetRow("SELECT count(subid) AS cnt FROM `" . DB_PREFIX . "_submissions` WHERE archiv = '1'");
            $objResponse->addScript("$('subcountarchiv').setHTML('" . $query['cnt'] . "');");

            $objResponse->addScript("SlideUp('asid_{$sid}');");
            $objResponse->addScript("SlideUp('asid_" . $sid . "a');");

            if ($query1) {
                $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Submission Deleted')."', '".t('modules/bans/admin', 'The selected submission has been deleted from the database')."', 'green', 'index.php?m=bans&p=admin', true);");

                new CSystemLog(
                    'm',
                    t('log', 'Submission Deleted'),
                    t('log', 'Submission ([[submission]]) has been deleted', array(
                        '[[submission]]' => $sid,
                    ))
                );

            } else {
                $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Error')."', '".t('modules/bans/admin', 'There was a problem deleting the submission from the database. Check the logs for more info')."', 'red', 'index.php?m=bans&p=admin', true);");
            }
        } else if ($archiv == "2") { // restore the submission
            $query1 = $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_submissions` SET archiv = '0', archivedby = NULL WHERE subid = $sid");
            $query = $GLOBALS['db']->GetRow("SELECT count(subid) AS cnt FROM `" . DB_PREFIX . "_submissions` WHERE archiv = '0'");
            $objResponse->addScript("$('subcountarchiv').setHTML('" . $query['cnt'] . "');");

            $objResponse->addScript("SlideUp('asid_{$sid}');");
            $objResponse->addScript("SlideUp('asid_" . $sid . "a');");

            if ($query1) {
                $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Submission Restored')."', '".t('modules/bans/admin', 'The selected submission has been restored from the archive!')."', 'green', 'index.php?m=bans&p=admin', true);");

                new CSystemLog(
                    'm',
                    t('log', 'Submission Restored'),
                    t('log', 'Submission ([[submission]]) has been restored from the archive', array(
                        '[[submission]]' => $sid,
                    ))
                );

            } else {
                $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Error')."', '".t('modules/bans/admin', 'There was a problem restoring the submission from the archive. Check the logs for more info')."', 'red', 'index.php?m=bans&p=admin', true);");
            }
        }
        return $objResponse;
    }
    
    function xajax_SetupBan($subid) {
        $objResponse = new xajaxResponse();
        $subid = (int) $subid;

        $ban = $GLOBALS['db']->GetRow("SELECT * FROM " . DB_PREFIX . "_submissions WHERE subid = $subid");
        $demo = $GLOBALS['db']->GetRow("SELECT * FROM " . DB_PREFIX . "_demos WHERE demid = $subid AND demtype = \"S\"");
        // clear any old stuff
        $objResponse->addScript("$('nickname').value = ''");
        $objResponse->addScript("$('fromsub').value = ''");
        $objResponse->addScript("$('steam').value = ''");
        $objResponse->addScript("$('ip').value = ''");
        $objResponse->addScript("$('txtReason').value = ''");
        $objResponse->addAssign("demo.msg", "innerHTML", "");
        // add new stuff
        $objResponse->addScript("$('nickname').value = '" . $ban['name'] . "'");
        $objResponse->addScript("$('steam').value = '" . $ban['SteamId'] . "'");
        $objResponse->addScript("$('ip').value = '" . $ban['sip'] . "'");
        if (trim($ban['SteamId']) == "") {
            $type = "1";
        } else {
            $type = "0";
        }
        $objResponse->addScriptCall("selectLengthTypeReason", "0", $type, addslashes($ban['reason']));

        $objResponse->addScript("$('fromsub').value = '$subid'");
        if ($demo) {
            $objResponse->addAssign("demo.msg", "innerHTML", $demo['origname']);
            $objResponse->addScript("demo('" . $demo['filename'] . "', '" . $demo['origname'] . "');");
        }
        $objResponse->addScript("SwapPane(0);");
        return $objResponse;
    }
    
    public function xajax_SendMail($subject, $message, $type, $id) {
        $objResponse = new xajaxResponse();
        global $userbank;

        $id = (int) $id;

        if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_BAN_PROTESTS | ADMIN_BAN_SUBMISSIONS)) {
            $objResponse->redirect("index.php?p=login&n=no_access", 0);

            new CSystemLog(
                'w',
                t('log', 'Hacking Attempt'),
                t('log', '[[username]] tried to send an email, but doesnt have access.', array(
                    '[[username]]' => $userbank->GetProperty("user"),
                ))
            );

            return $objResponse;
        }

        // Don't mind wrong types
        if ($type != 's' && $type != 'p') {
            return $objResponse;
        }

        // Submission
        $email = "";
        if ($type == 's') {
            $email = $GLOBALS['db']->GetOne('SELECT email FROM `' . DB_PREFIX . '_submissions` WHERE subid = ?', array($id));
        }
        // Protest
        else if ($type == 'p') {
            $email = $GLOBALS['db']->GetOne('SELECT email FROM `' . DB_PREFIX . '_protests` WHERE pid = ?', array($id));
        }

        if (empty($email)) {
            $objResponse->addScript("ShowBox('".t('email', 'Error')."', '".t('email', 'There is no email to send to supplied.')."', 'red', 'index.php?m=bans&p=admin');");
            return $objResponse;
        }

        $m = $userbank->sendMail($email, '[SourceBans] ' . $subject, $message);


        if ($m) {
            $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Email Sent')."', '".t('modules/bans/admin', 'The email has been sent to the user.')."', 'green', 'index.php?m=bans&p=admin');");

            new CSystemLog(
                'm',
                t('log', 'Email Sent'),
                t('log', '[[username]] send an email to [[email]].<br />Subject: [SourceBans] [[subject]]<br />Message: [[message]]', array(
                    '[[username]]' => $userbank->GetProperty("user"),
                    '[[email]]' => htmlspecialchars($email),
                    '[[subject]]' => htmlspecialchars($subject),
                    '[[message]]' => nl2br(htmlspecialchars($message))
                ))
            );

        } else {
            $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Error')."', '".t('modules/bans/admin', 'Failed to send the email to the user.')."', 'red', '');");
        }

        return $objResponse;
    }
    
    public function xajax_RemoveProtest($pid, $archiv) {
        $objResponse = new xajaxResponse();
        global $userbank;
        if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_BAN_PROTESTS)) {
            $objResponse->redirect("index.php?p=login&n=no_access", 0);

            new CSystemLog(
                'w',
                t('log', 'Hacking Attempt'),
                t('log', '[[username]] tried to remove a protest, but doesnt have access.', array(
                    '[[username]]' => $userbank->GetProperty("user"),
                ))
            );

            return $objResponse;
        }
        $pid = (int) $pid;
        if ($archiv == '0') { // delete protest
            $query1 = $GLOBALS['db']->Execute("DELETE FROM `" . DB_PREFIX . "_protests` WHERE pid = $pid");
            $GLOBALS['db']->Execute("DELETE FROM `" . DB_PREFIX . "_comments` WHERE type = 'P' AND bid = $pid;");
            $query = $GLOBALS['db']->GetRow("SELECT count(pid) AS cnt FROM `" . DB_PREFIX . "_protests` WHERE archiv = '1'");
            $objResponse->addScript("$('protcountarchiv').setHTML('" . $query['cnt'] . "');");
            $objResponse->addScript("SlideUp('apid_{$pid}');");
            $objResponse->addScript("SlideUp('apid_" . $pid . "a');");

            if ($query1) {
                $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Protest Deleted')."', '".t('modules/bans/admin', 'The selected protest has been deleted from the database')."', 'green', 'index.php?m=bans&p=admin', true);");

                new CSystemLog(
                    'm',
                    t('log', 'Protest Deleted'),
                    t('log', 'Protest ([[protest]]) has been deleted', array(
                        '[[protest]]' => $pid,
                    ))
                );

            } else {
                $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Error')."', '".t('modules/bans/admin', 'There was a problem deleting the protest from the database. Check the logs for more info')."', 'red', 'index.php?m=bans&p=admin', true);");
            }
        } else if ($archiv == '1') { // move protest to archiv
            $query1 = $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_protests` SET archiv = '1', archivedby = '" . $userbank->GetAid() . "' WHERE pid = $pid");
            $query = $GLOBALS['db']->GetRow("SELECT count(pid) AS cnt FROM `" . DB_PREFIX . "_protests` WHERE archiv = '0'");
            $objResponse->addScript("$('protcount').setHTML('" . $query['cnt'] . "');");
            $objResponse->addScript("SlideUp('pid_{$pid}');");
            $objResponse->addScript("SlideUp('pid_" . $pid . "a');");

            if ($query1) {
                $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Protest Archived')."', '".t('modules/bans/admin', 'The selected protest has been moved to the archive.')."', 'green', 'index.php?m=bans&p=admin', true);");

                new CSystemLog(
                    'm',
                    t('log', 'Protest Archived'),
                    t('log', 'Protest ([[protest]]) has been moved to the archive', array(
                        '[[protest]]' => $pid,
                    ))
                );

            } else {
                $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Error')."', '".t('modules/bans/admin', 'There was a problem moving the protest to the archive. Check the logs for more info')."', 'red', 'index.php?m=bans&p=admin', true);");
            }
        } else if ($archiv == '2') { // restore protest
            $query1 = $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_protests` SET archiv = '0', archivedby = NULL WHERE pid = $pid");
            $query = $GLOBALS['db']->GetRow("SELECT count(pid) AS cnt FROM `" . DB_PREFIX . "_protests` WHERE archiv = '1'");
            $objResponse->addScript("$('protcountarchiv').setHTML('" . $query['cnt'] . "');");
            $objResponse->addScript("SlideUp('apid_{$pid}');");
            $objResponse->addScript("SlideUp('apid_" . $pid . "a');");

            if ($query1) {
                $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Protest Restored')."', '".t('modules/bans/admin', 'The selected protest has been restored from the archive.')."', 'green', 'index.php?m=bans&p=admin', true);");

                new CSystemLog(
                    'm',
                    t('log', 'Protest Restored'),
                    t('log', 'Protest ([[protest]]) has been restored from the archive', array(
                        '[[protest]]' => $pid,
                    ))
                );

            } else {
                $objResponse->addScript("ShowBox('".t('modules/bans/admin', 'Error')."', '".t('modules/bans/admin', 'There was a problem restoring the protest from the archive. Check the logs for more info')."', 'red', 'index.php?m=bans&p=admin', true);");
            }
        }
        return $objResponse;
    }
}