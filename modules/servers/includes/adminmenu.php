<?php
global $userbank;
return array(
    array(
        'label' => t('modules/servers/main', 'Servers'),
        'url' => 'index.php?m=servers&p=admin',
        'visible' => $userbank->HasAccess(ADMIN_OWNER|ADMIN_LIST_SERVERS|ADMIN_ADD_SERVER|ADMIN_EDIT_SERVERS|ADMIN_DELETE_SERVERS)
    ),
    array(
        'label'     => t('adminmenu', 'Mods'),
        'url'       => 'index.php?m=servers&p=admin&c=mods',
        'visible'   => $userbank->HasAccess(ADMIN_OWNER|ADMIN_LIST_MODS|ADMIN_ADD_MODS|ADMIN_EDIT_MODS|ADMIN_DELETE_MODS)
    ),
);