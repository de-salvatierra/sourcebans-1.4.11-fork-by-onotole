<?php

class ServersModule extends CModule {
    
    /**
     * Возвращает список серверов для главной страницы
     * @return array
     */
    public static function forDashboard() {
        return self::getList(-1, true);
    }
    
    /**
     * Возвращает список серверов
     * @param integer $number ID сервера в массиве (? хз, что там разрабы с этим придумали. По-моему этот ИД прописывается как ID tablerow)
     * @param boolean $dashboard Выводить для главной или для страницы серверов
     * @return array
     */
    public static function getList($number, $dashboard = false) {
        $res = $GLOBALS['db']->Execute(
            "SELECT
                se.sid,
                se.ip,
                se.port,
                se.modid,
                se.rcon,
                md.icon
            FROM
                " . DB_PREFIX . "_servers se
            LEFT JOIN
                " . DB_PREFIX . "_mods md
            ON
                md.mid=se.modid
            WHERE
                se.sid > 0 AND se.enabled = 1
            ORDER BY
                se.modid, se.sid"
        );
        $servers = array();
        $i = 0;
        // TODO: избавиться от $GLOBALS
        $GLOBALS['server_qry'] = "";
        while (!$res->EOF) {
            if (isset($_SESSION['getInfo.' . $res->fields[1] . '.' . $res->fields[2]])) {
                $_SESSION['getInfo.' . $res->fields[1] . '.' . $res->fields[2]] = "";
            }
            $info = array();
            $info['sid'] = $res->fields[0];
            $info['ip'] = $res->fields[1];
            $info['port'] = $res->fields[2];
            $info['icon'] = $res->fields[5];
            $info['index'] = $i;
            if ($dashboard) {
                $info['evOnClick'] = "window.location = 'index.php?m=servers&s=" . $info['index'] . "';";
            }

            $GLOBALS['server_qry'] .= "xajax_ServerHostPlayers({$info['sid']}, 'servers', '', '" . $i . "', '" . $number . "', '" . $dashboard . "', 70);";
            array_push($servers, $info);
            $i++;
            $res->MoveNext();
        }
        return $servers;
    }
    
    public function xajax_AddMod($name, $folder, $icon, $steam_universe, $enabled) {
        $objResponse = new xajaxResponse();
        global $userbank;
        if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_ADD_MODS)) {
            $objResponse->redirect("index.php?p=login&n=no_access", 0);

            new CSystemLog(
                'w',
                t('log', 'Hacking Attempt'),
                t('log', '[[username]] tried to add a mod, but doesnt have access.', array(
                    '[[username]]' => $userbank->GetProperty("user"),
                ))
            );

            return $objResponse;
        }
        $name = htmlspecialchars(strip_tags($name)); //don't want to addslashes because execute will automatically do it
        $icon = htmlspecialchars(strip_tags($icon));
        $folder = htmlspecialchars(strip_tags($folder));
        $steam_universe = (int) $steam_universe;
        $enabled = (int) $enabled;

        // Already there?
        $check = $GLOBALS['db']->GetRow("SELECT * FROM `" . DB_PREFIX . "_mods` WHERE modfolder = ? OR name = ?;", array($folder, $name));
        if (!empty($check)) {
            $objResponse->addScript("ShowBox('".t('modules/servers/admin', 'Error')."', '".t('modules/servers/admin', 'A mod using that folder or name already exists.')."', 'red');");
            return $objResponse;
        }

        $pre = $GLOBALS['db']->Prepare("INSERT INTO " . DB_PREFIX . "_mods(name,icon,modfolder,steam_universe,enabled) VALUES (?,?,?,?,?)");
        $GLOBALS['db']->Execute($pre, array($name, $icon, $folder, $steam_universe, $enabled));

        $objResponse->addScript("ShowBox('".t('modules/servers/admin', 'Mod Added')."', '".t('modules/servers/admin', 'The game mod has been successfully added')."', 'green', 'index.php?m=servers&p=admin&c=mods');");
        $objResponse->addScript("TabToReload();");

        new CSystemLog(
            'm',
            t('log', 'Mod Added'),
            t('log', 'Mod ([[mod]]) has been added', array(
                '[[mod]]' => $name
            ))
        );

        return $objResponse;
    }
    
    public function xajax_RemoveMod($mid) {
        $objResponse = new xajaxResponse();
        global $userbank;
        if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_DELETE_MODS)) {
            $objResponse->redirect("index.php?p=login&n=no_access", 0);

            new CSystemLog(
                'w',
                t('log', 'Hacking Attempt'),
                t('log', '[[username]] tried to remove a mod, but doesnt have access.', array(
                    '[[username]]' => $userbank->GetProperty("user"),
                ))
            );

            return $objResponse;
        }
        $mid = (int) $mid;
        $objResponse->addScript("SlideUp('mid_{$mid}');");

        $modicon = $GLOBALS['db']->GetRow("SELECT icon, name FROM `" . DB_PREFIX . "_mods` WHERE mid = '" . $mid . "';");
        @unlink(SB_ICONS . $modicon['icon']);

        $query1 = $GLOBALS['db']->Execute("DELETE FROM `" . DB_PREFIX . "_mods` WHERE mid = '" . $mid . "'");

        if ($query1) {
            $objResponse->addScript("ShowBox('".t('modules/servers/admin', 'MOD Deleted')."', '".t('modules/servers/admin', 'The selected MOD has been deleted from the database')."', 'green', 'index.php?m=servers&p=admin&c=mods', true);");

            new CSystemLog(
                'm',
                t('log', 'MOD Deleted'),
                t('log', 'MOD ([[mod]]) has been deleted', array(
                    '[[mod]]' => $modicon['name'],
                ))
            );
        } else {
            $objResponse->addScript("ShowBox('".t('modules/servers/admin', 'Error')."', '".t('modules/servers/admin', 'There was a problem deleting the MOD from the database. Check the logs for more info')."', 'red', 'index.php?p=admin&c=mods', true);");
        }
        return $objResponse;
    }
    
    /**
     * AJAX метод добавления сервера
     * @global CUserManager $userbank
     * @param string $ip IP сервера
     * @param integer $port Порт сервера
     * @param string $rcon RCON пароль
     * @param string $rcon2 Подтверждение RCON пароля
     * @param integer $mod ID мода
     * @param string $enabled Активен ли сервер
     * @param string $group Айдишники групп серверов
     * @param string $group_name Имя группы
     * @return \xajaxResponse
     */
    public function xajax_AddServer($ip, $port, $rcon, $rcon2, $mod, $enabled, $group, $group_name) {
        $objResponse = new xajaxResponse();
        global $userbank;
        if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_ADD_SERVER)) {
            $objResponse->redirect("index.php?p=login&n=no_access", 0);

            new CSystemLog(
                'w',
                t('log', 'Hacking Attempt'),
                t('log', '[[username]] tried to add a server, but doesnt have access.', array(
                    '[[username]]' => $userbank->GetProperty("user"),
                ))
            );

            return $objResponse;
        }
        $ip = RemoveCode($ip);
        $group_name = RemoveCode($group_name);

        $error = 0;
        // ip
        if ((empty($ip))) {
            $error++;
            $objResponse->addAssign("address.msg", "innerHTML", t('modules/servers/admin', 'You must type the server address.'));
            $objResponse->addScript("$('address.msg').setStyle('display', 'block');");
        } else {
            $objResponse->addAssign("address.msg", "innerHTML", "");
            if (!validate_ip($ip) && !is_string($ip)) {
                $error++;
                $objResponse->addAssign("address.msg", "innerHTML", t('modules/servers/admin', 'You must type a valid IP.'));
                $objResponse->addScript("$('address.msg').setStyle('display', 'block');");
            } else {
                $objResponse->addAssign("address.msg", "innerHTML", "");
            }
        }
        // Port
        if ((empty($port))) {
            $error++;
            $objResponse->addAssign("port.msg", "innerHTML", t('modules/servers/admin', 'You must type the server port.'));
            $objResponse->addScript("$('port.msg').setStyle('display', 'block');");
        } else {
            $objResponse->addAssign("port.msg", "innerHTML", "");
            if (!is_numeric($port)) {
                $error++;
                $objResponse->addAssign("port.msg", "innerHTML", t('modules/servers/admin', 'You must type a valid port <b>number</b>.'));
                $objResponse->addScript("$('port.msg').setStyle('display', 'block');");
            } else {
                $objResponse->addScript("$('port.msg').setStyle('display', 'none');");
                $objResponse->addAssign("port.msg", "innerHTML", "");
            }
        }
        // rcon
        if (!empty($rcon) && $rcon != $rcon2) {
            $error++;
            $objResponse->addAssign("rcon2.msg", "innerHTML", t('modules/servers/admin', 'The passwords don\'t match.'));
            $objResponse->addScript("$('rcon2.msg').setStyle('display', 'block');");
        } else {
            $objResponse->addAssign("rcon2.msg", "innerHTML", "");
        }

        // Please Select
        if ($mod == -2) {
            $error++;
            $objResponse->addAssign("mod.msg", "innerHTML", t('modules/servers/admin', 'You must select the mod your server runs.'));
            $objResponse->addScript("$('mod.msg').setStyle('display', 'block');");
        } else {
            $objResponse->addAssign("mod.msg", "innerHTML", "");
        }

        if ($group == -2) {
            $error++;
            $objResponse->addAssign("group.msg", "innerHTML", t('modules/servers/admin', 'You must select an option.'));
            $objResponse->addScript("$('group.msg').setStyle('display', 'block');");
        } else {
            $objResponse->addAssign("group.msg", "innerHTML", "");
        }

        if ($error) {
            return $objResponse;
        }

        // Check for dublicates afterwards
        $chk = $GLOBALS['db']->GetRow('SELECT sid FROM `' . DB_PREFIX . '_servers` WHERE ip = ? AND port = ?;', array($ip, (int) $port));
        if ($chk) {
            $objResponse->addScript("ShowBox('".t('modules/servers/admin', 'Error')."', '".t('modules/servers/admin', 'There already is a server with that IP:Port combination.')."', 'red');");
            return $objResponse;
        }

        // ##############################################################
        // ##                     Start adding to DB                   ##
        // ##############################################################
        //they wanna make a new group
        $sid = nextSid();

        $enable = ($enabled == "true" ? 1 : 0);

        // Add the server
        $addserver = $GLOBALS['db']->Prepare("INSERT INTO " . DB_PREFIX . "_servers (`sid`, `ip`, `port`, `rcon`, `modid`, `enabled`)
                                              VALUES (?,?,?,?,?,?)");
        $GLOBALS['db']->Execute($addserver, array($sid, $ip, (int) $port, $rcon, $mod, $enable));

        // Add server to each group specified
        $groups = explode(",", $group);
        $addtogrp = $GLOBALS['db']->Prepare("INSERT INTO " . DB_PREFIX . "_servers_groups (`server_id`, `group_id`) VALUES (?,?)");
        foreach ($groups AS $g) {
            if ($g) {
                $GLOBALS['db']->Execute($addtogrp, array($sid, $g));
            }
        }


        $objResponse->addScript("ShowBox('".t('modules/servers/admin', 'Server Added')."', '".t('modules/servers/admin', 'Your server has been successfully created.')."', 'green', 'index.php?p=admin&c=servers');");
        $objResponse->addScript("TabToReload();");

        new CSystemLog(
            'm',
            t('log', 'Server Added'),
            t('log', 'Server ([[server]]) has been added', array(
                '[[server]]' => $ip . ":" . $port
            ))
        );

        return $objResponse;
    }
    
    /**
     * AJAX добавление сервера
     * @global CUserManager $userbank
     * @param integer $sid ID сервера
     * @return \xajaxResponse
     */
    public function xajax_RemoveServer($sid) {
        $objResponse = new xajaxResponse();
        global $userbank;
        if (!$userbank->HasAccess(ADMIN_OWNER | ADMIN_DELETE_SERVERS)) {
            $objResponse->redirect("index.php?p=login&n=no_access", 0);

            new CSystemLog(
                'w',
                t('log', 'Hacking Attempt'),
                t('log', '[[username]] tried to remove a server, but doesnt have access.', array(
                    '[[username]]' => $userbank->GetProperty("user"),
                ))
            );

            return $objResponse;
        }
        $sid = (int) $sid;
        $objResponse->addScript("SlideUp('sid_$sid');");
        $servinfo = $GLOBALS['db']->GetRow("SELECT ip, port FROM `" . DB_PREFIX . "_servers` WHERE sid = $sid");
        $query1 = $GLOBALS['db']->Execute("DELETE FROM `" . DB_PREFIX . "_servers` WHERE sid = $sid");
        $GLOBALS['db']->Execute("DELETE FROM `" . DB_PREFIX . "_servers_groups` WHERE server_id = $sid");
        $GLOBALS['db']->Execute("UPDATE `" . DB_PREFIX . "_admins_servers_groups` SET server_id = -1 WHERE server_id = $sid");

        $query = $GLOBALS['db']->GetRow("SELECT count(sid) AS cnt FROM `" . DB_PREFIX . "_servers`");
        $objResponse->addScript("$('srvcount').setHTML('" . $query['cnt'] . "');");


        if ($query1) {
            $objResponse->addScript("ShowBox('".t('modules/servers/admin', 'Server Deleted')."', '".t('modules/servers/admin', 'The selected server has been deleted from the database')."', 'green', 'index.php?m=servers&p=admin', true);");

            new CSystemLog(
                'm',
                t('log', 'Server Deleted'),
                t('log', 'Server ([[server]]) has been deleted', array(
                    '[[server]]' => $servinfo['ip'] . ":" . $servinfo['port'],
                ))
            );

        } else {
            $objResponse->addScript("ShowBox('".t('modules/servers/admin', 'Error')."', '".t('modules/servers/admin', 'There was a problem deleting the server from the database. Check the logs for more info')."', 'red', 'index.php?m=servers&p=admin', true);");
        }
        return $objResponse;
    }
    
    /**
     * AJAX отправка RCON команды на сервер
     * @global CUserManager $userbank
     * @param integer $sid ID сервера
     * @param string $command Команда
     * @param boolean $output Выводить ли ответ в консоль
     * @return \xajaxResponse
     */
    public function xajax_SendRcon($sid, $command, $output) {
        global $userbank;
        $objResponse = new xajaxResponse();
        if (!$userbank->HasAccess(SM_RCON . SM_ROOT)) {
            $objResponse->redirect("index.php?p=login&n=no_access", 0);

            new CSystemLog(
                'w',
                t('log', 'Hacking Attempt'),
                t('log', '[[username]] tried to send an rcon command, but doesnt have access.', array(
                    '[[username]]' => $userbank->GetProperty("user"),
                ))
            );

            return $objResponse;
        }
        if (empty($command)) {
            $objResponse->addScript("$('cmd').value=''; $('cmd').disabled='';$('rcon_btn').disabled=''");
            return $objResponse;
        }
        if ($command == "clr") {
            $objResponse->addAssign("rcon_con", "innerHTML", "");
            $objResponse->addScript("scroll.toBottom(); $('cmd').value=''; $('cmd').disabled='';$('rcon_btn').disabled=''");
            return $objResponse;
        }

        if (stripos($command, "rcon_password") !== false) {
            $objResponse->addAppend("rcon_con", "innerHTML", "> ".t('modules/servers/admin', "Error: You have to use this console. Don't try to cheat the rcon password!")."<br />");
            $objResponse->addScript("scroll.toBottom(); $('cmd').value=''; $('cmd').disabled='';$('rcon_btn').disabled=''");
            return $objResponse;
        }

        $sid = (int) $sid;

        $rcon = $GLOBALS['db']->GetRow("SELECT ip, port, rcon FROM `" . DB_PREFIX . "_servers` WHERE sid = " . $sid . " LIMIT 1");
        if (empty($rcon['rcon'])) {
            $objResponse->addAppend("rcon_con", "innerHTML", "> " . t('modules/servers/admin', 'Error: No RCON password!<br />You have to add the RCON password for this server in the "edit server" <br />page to use this console!<br />'));
            $objResponse->addScript("scroll.toBottom(); $('cmd').value='".t('modules/servers/admin', 'Add RCON password.')."'; $('cmd').disabled=true; $('rcon_btn').disabled=true");
            return $objResponse;
        }

        require_once INCLUDES_PATH . 'SourceQuery'.DIRECTORY_SEPARATOR.'SourceQuery.class.php';
        $query = new SourceQuery;
        try{
            $query->Connect($rcon['ip'], $rcon['port'], 1);
        } catch(Exception $e) {
            $objResponse->addAppend("rcon_con", "innerHTML", "> ".t('modules/servers/admin', 'Error: Can not connect to server!')."<br />");
            $objResponse->addScript("scroll.toBottom(); $('cmd').value=''; $('cmd').disabled='';$('rcon_btn').disabled=''");
            return $objResponse;
        }

        try {
            $query->SetRconPassword($rcon['rcon']);
        } catch(Exception $e) {
            $GLOBALS['db']->Execute("UPDATE " . DB_PREFIX . "_servers SET rcon = '' WHERE sid = '" . $sid . "';");
            $objResponse->addAppend(
                "rcon_con",
                "innerHTML",
                "> "
                .t(
                    'modules/servers/admin',
                    'Error: Wrong RCON password!<br />'
                    . 'You MUST change the RCON password for this server in the "edit server" <br />'
                    . 'page. If you continue to use this console with the wrong password, <br />'
                    . 'the server will block the connection!'
                    )
                ."<br />"
            );
            $objResponse->addScript("scroll.toBottom(); $('cmd').value='".t('modules/servers/admin', 'Change RCON password.')."'; $('cmd').disabled=true; $('rcon_btn').disabled=true");
            return $objResponse;
        }

        $ret = $query->Rcon($command);
        
        $ret = trim($ret);
        if (empty($ret) && $output) {
            $objResponse->addAppend("rcon_con", "innerHTML", "<br>-> {$command}<br />");
            $objResponse->addAppend("rcon_con", "innerHTML", "".t('modules/servers/admin', 'Command Executed.')."<br />");
        } elseif($output) {
            $objResponse->addAppend("rcon_con", "innerHTML", "<br>-> {$command}<br />");
            $objResponse->addAppend("rcon_con", "innerHTML", "{$ret}<br />");
        }
        $objResponse->addScript("scroll.toBottom(); $('cmd').value=''; $('cmd').disabled=''; $('rcon_btn').disabled=''");

        new CSystemLog(
            'm',
            t('log', 'RCON Sent'),
            t('log', 'RCON Command was sent to server ([[server]]): [[command]]', array(
                '[[server]]' => $rcon['ip'] . ":" . $rcon['port'],
                '[[command]]' => $command
            )),
            true,
            true
        );

        return $objResponse;
    }
}