<?php

/* @var $this ServersModule */

/**
 * =============================================================================
 * Page servers
 * 
 * @author SteamFriends Development Team
 * @version 1.0.0
 * @copyright SourceBans (C)2007 SteamFriends.com.  All rights reserved.
 * @package SourceBans
 * @link http://www.sourcebans.net
 * 
 * @version $Id: page.servers.php 269 2009-06-21 11:01:26Z peace-maker $
 * =============================================================================
 */
global $theme;
if (!defined("IN_SB")) {
    echo "You should not be here. Only follow links!";
    die();
}

if (isset($_GET['s'])) {
    $number = (int) $_GET['s'];
} else {
    $number = -1;
}
$sbViewParams['pageTitle'] = t('modules/servers/main', 'Servers');
$sbViewParams['title'] =  t('modules/servers/main', 'Servers list');
$sbViewParams['breadCrumbs'][] = t('modules/servers/main', 'Servers list');

$theme->assign('access_bans', ($userbank->HasAccess(ADMIN_OWNER | ADMIN_ADD_BAN) ? true : false));
$theme->assign('server_list', $this->getList($number));
$theme->assign('IN_SERVERS_PAGE', !defined('IN_HOME'));
$theme->assign('opened_server', $number);

if (!defined('IN_HOME')) {
    $theme->display('page_servers.tpl');
}
